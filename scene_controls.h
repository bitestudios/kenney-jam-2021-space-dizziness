#ifndef _SCENE_CONTROLS_H_
#define _SCENE_CONTROLS_H_

typedef enum {
	CONTROLS_MENU_OPTION_BACK,
	CONTROLS_MENU_OPTION_COUNT
} ControlsMenuOptions;

void
Init_scene_controls();

void
Scene_controls();

// This needs to be set
void
Set_controls_return_scene(u32 ret_scn);

u32 return_scene = 0;

#endif