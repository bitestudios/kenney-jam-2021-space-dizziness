#version 100

attribute vec2 vertex;
attribute vec4 vertex_color;
uniform mat4 vmat;
varying vec4 pixel_color;

void main()
{
    gl_Position = vec4(vertex, 0.0, 1.0) * vmat;
	pixel_color = vertex_color;
}


