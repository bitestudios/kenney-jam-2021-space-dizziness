

void
Scene_credits() {
	{
		f32 min_alpha = 0.5f;
		f32 max_alpha = 1.2f;
		f32 starfield_alpha = Lerp(min_alpha, max_alpha, (sinf(GLOBALS.current_time_f32*1.5f)+1.0f) * 0.5f);
		Draw_sprite_ex(
			0.0f, 0.0f,
			GAMEPLAY_WIDTH, GAMEPLAY_HEIGHT, 0.0f, 0.0f, 0.0f,
			&GLOBALS.starfield_bg, HMM_Vec4(1.0f, 1.0f, 1.0f, starfield_alpha));
	}

	{
		f32 title_height = (f32)GAMEPLAY_HEIGHT * 0.11f;
		f32 title_width = Get_text_width_with_height(&GLOBALS.font, "CREDITS", title_height);
		f32 title_x = ((f32)GAMEPLAY_WIDTH - title_width) * 0.5f;
		f32 title_y = (f32)GAMEPLAY_HEIGHT * 0.80;
		Draw_text_with_height(&GLOBALS.font, "CREDITS", title_height, title_x, title_y, 1, 1, 1, 1);
	}

	{
		f32 auth_height = (f32)GAMEPLAY_HEIGHT * 0.07f;
		f32 auth_width = Get_text_width_with_height(&GLOBALS.font, "Tano", auth_height);
		f32 auth_x = ((f32)GAMEPLAY_WIDTH - auth_width) * 0.5f;
		f32 auth_y = (f32)GAMEPLAY_HEIGHT * 0.65;
		Draw_text_with_height(&GLOBALS.font, "Tano", auth_height, auth_x, auth_y, 1, 1, 1, 1);
	}

	{
		f32 auth_height = (f32)GAMEPLAY_HEIGHT * 0.07f;
		f32 auth_width = Get_text_width_with_height(&GLOBALS.font, "Tonet", auth_height);
		f32 auth_x = ((f32)GAMEPLAY_WIDTH - auth_width) * 0.5f;
		f32 auth_y = (f32)GAMEPLAY_HEIGHT * 0.55;
		Draw_text_with_height(&GLOBALS.font, "Tonet", auth_height, auth_x, auth_y, 1, 1, 1, 1);
	}

	{
		f32 auth_height = (f32)GAMEPLAY_HEIGHT * 0.07f;
		f32 auth_width = Get_text_width_with_height(&GLOBALS.font, "Salva", auth_height);
		f32 auth_x = ((f32)GAMEPLAY_WIDTH - auth_width) * 0.5f;
		f32 auth_y = (f32)GAMEPLAY_HEIGHT * 0.45;
		Draw_text_with_height(&GLOBALS.font, "Salva", auth_height, auth_x, auth_y, 1, 1, 1, 1);
	}

	{
		f32 kenn_height = (f32)GAMEPLAY_HEIGHT * 0.06f;
		f32 kenn_width = Get_text_width_with_height(&GLOBALS.font, "Assets by Kenney", kenn_height);
		f32 kenn_x = ((f32)GAMEPLAY_WIDTH - kenn_width) * 0.5f;
		f32 kenn_y = (f32)GAMEPLAY_HEIGHT * 0.30;
		Draw_text_with_height(&GLOBALS.font, "Assets by Kenney", kenn_height, kenn_x, kenn_y, 1, 1, 1, 1);
	}


	if (Show_menu(
			GLOBALS.credits_menu_options,
			CREDITS_MENU_OPTION_COUNT,
			GLOBALS.credits_menu_selected_option,
			&GLOBALS.credits_menu_selected_option)) {

		GLOBALS.mouse_pointing = false;

		if (GLOBALS.credits_menu_selected_option == CREDITS_MENU_BACK) {
			GLOBALS.current_scene = SCENE_MENU;
		}
	}
}

