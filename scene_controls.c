#include "scene_controls.h"
#include "engine/sprite.h"


Texture space_texture, left_texture, right_texture;

void
Init_scene_controls() {
    if (Setup_texture("assets/img/arrowLeft.png", true, &left_texture) != 0) {
        Panic("Could not set texture\n");
    }
    if (Setup_texture("assets/img/arrowRight.png", true, &right_texture) != 0) {
        Panic("Could not set texture\n");
    }
    if (Setup_texture("assets/img/spaceButton.png", true, &space_texture) != 0) {
        Panic("Could not set texture\n");
    }
}

void
Set_controls_return_scene(u32 ret_scn) {
    return_scene = ret_scn;
}

#define ARROWS_SCALE 0.8
#define SPACE_SCALE 0.8

void
Scene_controls() {
	f32 min_alpha = 0.5f;
	f32 max_alpha = 1.2f;
	f32 starfield_alpha = Lerp(min_alpha, max_alpha, (sinf(GLOBALS.current_time_f32*1.5f)+1.0f) * 0.5f);

	Draw_sprite_ex(0.0f, 0.0f, GAMEPLAY_WIDTH, GAMEPLAY_HEIGHT, 0.0f, 0.0f, 0.0f, &GLOBALS.starfield_bg, HMM_Vec4(1.0f, 1.0f, 1.0f, starfield_alpha));
    f32 title_height = (f32)GAMEPLAY_HEIGHT * 0.11f;
	f32 title_width = Get_text_width_with_height(&GLOBALS.font, "CONTROLS", title_height);
	f32 title_x = ((f32)GAMEPLAY_WIDTH - title_width) * 0.5f;
	f32 title_y = (f32)GAMEPLAY_HEIGHT * 0.80;
	Draw_text_with_height(&GLOBALS.font, "CONTROLS", title_height, title_x, title_y, 1, 1, 1, 1);

    title_height = (f32)GAMEPLAY_HEIGHT * 0.08f;
	title_width = Get_text_width_with_height(&GLOBALS.font, "IMPULSE             ROTATE", title_height);
	title_x = ((f32)GAMEPLAY_WIDTH - title_width) * 0.5f;
	title_y = (f32)GAMEPLAY_HEIGHT * 0.60;
	Draw_text_with_height(&GLOBALS.font, "IMPULSE             ROTATE", title_height, title_x, title_y, 1, 1, 1, 1);

    title_y -= 50;
    title_x += 5;
    f32 s_width = space_texture.width*SPACE_SCALE;
    f32 s_height = space_texture.height*SPACE_SCALE*2/4;
    Draw_sprite(title_x, title_y, s_width, s_height, 0, &space_texture, space_texture.width, space_texture.height, 0, RGB_DEFAULT);

    title_height = 13;
    title_width = Get_text_width_with_height(&GLOBALS.font, "Space", title_height);
    f32 csp_x = title_x + s_width/2 - title_width/2 + 2;
    f32 csp_y = title_y + s_height/2 - title_height/2 + 2;
    Draw_text_with_height(&GLOBALS.font, "Space", title_height, csp_x, csp_y, 0, 0, 0, 1);
    
    title_x += 147;
    s_width = left_texture.width*ARROWS_SCALE;
    s_height = left_texture.height*ARROWS_SCALE;

    Draw_sprite(title_x, title_y, s_width, s_height, 0, &left_texture, left_texture.width, left_texture.height, 0, RGB_DEFAULT);
    title_x += 40;
    s_width = right_texture.width*ARROWS_SCALE;
    s_height = right_texture.height*ARROWS_SCALE;
    Draw_sprite(title_x, title_y, s_width, s_height, 0, &right_texture, left_texture.width, left_texture.height, 0, RGB_DEFAULT);

    // title_height = (f32)GAMEPLAY_HEIGHT * 0.08f;
	// title_width = Get_text_width_with_height(&GLOBALS.font, "IMPULSE --------- SPACE", title_height);
	// title_x = ((f32)GAMEPLAY_WIDTH - title_width) * 0.5f;
	// title_y = (f32)GAMEPLAY_HEIGHT * 0.35;
	// Draw_text_with_height(&GLOBALS.font, "IMPULSE --------- SPACE", title_height, title_x, title_y, 1, 1, 1, 1);

    if (Show_menu(GLOBALS.controls_menu_options, CONTROLS_MENU_OPTION_COUNT, 
        GLOBALS.controls_menu_selected_option, &GLOBALS.congratulations_menu_selected_option)) {
        GLOBALS.mouse_pointing = false;

        if (GLOBALS.controls_menu_selected_option == CONTROLS_MENU_OPTION_BACK) {
			GLOBALS.current_scene = return_scene;
		}
    }
}
