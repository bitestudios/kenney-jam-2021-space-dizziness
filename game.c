#include "game.h"

Globals GLOBALS = {0};

const BoundingMesh PLAYER_BOUNDIG_MESH = {
	.total_verts = 6,
	.vertices = {
		/* BEST FIT
		V2(-0.18f, 0.0f), V2(-0.18, -0.65f), V2( 0.7, 0.0f),
		V2(-0.18f, 0.0f), V2( 0.7,  0.0f), V2(-0.18, 0.65f),
		*/
		V2(-0.1f, 0.0f), V2(-0.1, -0.65f * 0.8f), V2( 0.7 * 0.8f, 0.0f),
		V2(-0.1f, 0.0f), V2( 0.7f * 0.8f, 0.0f),          V2(-0.1, 0.65f * 0.8f),
	},
};

const BoundingMesh ASTEROID0_BOUNDIG_MESH = {
	.total_verts = 30,
	.vertices = {
		V2( 0.0f,  0.0f), V2(-0.33f, -0.46f), V2(0.0, -0.49f),
		V2( 0.0f,  0.0f), V2(0.0, -0.49f),    V2( 0.3f, -0.4f),
		V2( 0.0f,  0.0f), V2( 0.3f, -0.4f),   V2( 0.47f, -0.17f),
		V2( 0.0f,  0.0f), V2( 0.47f, -0.17f), V2( 0.5f, 0.13f),
		V2( 0.0f,  0.0f), V2( 0.5f, 0.13f),   V2( 0.29f, 0.4f),
		V2( 0.0f,  0.0f), V2( 0.29f, 0.4f),   V2( 0.01f, 0.5f),
		V2( 0.0f,  0.0f), V2( 0.01f, 0.5f),   V2(-0.33f, 0.38f),
		V2( 0.0f,  0.0f), V2(-0.33f, 0.38f),  V2(-0.5f, 0.16f),
		V2( 0.0f,  0.0f), V2(-0.5f, 0.16f),   V2(-0.47f, -0.12f),
		V2( 0.0f,  0.0f), V2(-0.47f, -0.12f), V2(-0.33f, -0.46f),
	},
};

const BoundingMesh *ASTEROIDS_BOUNDING_MESHES[] = {
	&ASTEROID0_BOUNDIG_MESH
};

const Level LEVEL0 = {
	.player = {
		.x   = GAME_AREA_W * 0.5f,
		.y   = GAME_AREA_H * 0.5f,
		.dir = 90.0f,
		.impulses = 1,
		.energy = 50.0f
	},
	
	.checkpoint = {
		.x = GAME_AREA_W * 0.8f,
		.y = GAME_AREA_H * 0.2f
	},

	.total_asteroids = 1,

	.asteroids = {
		{
			.x = GAME_AREA_W * 0.8f,
			.y = GAME_AREA_H * 0.5f,
			.scale_x = 10.0f,
			.scale_y = 10.0f,
		},
	}
};

const Level LEVEL1 = {
	.player = {
		.x   = GAME_AREA_W * 0.5f,
		.y   = GAME_AREA_H * 0.5f,
		.dir = 45.0f,
		.impulses = 2,
		.energy = 50.0f
	},
	
	.checkpoint = {
		.x = GAME_AREA_W * 0.8f,
		.y = GAME_AREA_H * 0.2f
	},

	.total_asteroids = 1,

	.asteroids = {
		{
			.x = GAME_AREA_W * 0.8f,
			.y = GAME_AREA_H * 0.5f,
			.scale_x = 20.0f,
			.scale_y = 30.0f,
			.angular_speed = 180.0f,
		},
	}
};

int
Load_level(const char* filename, Level *level) {
    u32 objects_size;
    Object *obj = Load_objects_from_tiled_json(filename, GAMEPLAY_HEIGHT, &objects_size);
    if (obj == NULL) {
        Panic("Could not load objects from tiled!\n");
    }

    level->total_asteroids = 0;

    for (u32 i = 0; i < objects_size; i++) {
        if (strcmp(obj[i].name, "asteroid0") == 0) {
            level->asteroids[level->total_asteroids].x = obj[i].x + obj[i].width*0.5;
            level->asteroids[level->total_asteroids].y = obj[i].y + obj[i].height*0.5;
            level->asteroids[level->total_asteroids].rotation = obj[i].rotation;
            level->asteroids[level->total_asteroids].scale_x = obj[i].width;
            level->asteroids[level->total_asteroids].scale_y = obj[i].height;
            level->asteroids[level->total_asteroids].type = ASTEROID_TYPE_0;

            level->asteroids[level->total_asteroids].vx = level->asteroids[level->total_asteroids].vy = level->asteroids[level->total_asteroids].angular_speed = 0;

            for (u32 j = 0; j < obj[i].attributes_size; j++) {
                const char *name = obj[i].attributes[j].name;
                const char *value = obj[i].attributes[j].value;

                if (strcmp(name, "Vx") == 0) {
                    level->asteroids[level->total_asteroids].vx = atof(value);
                }
                else if (strcmp(name, "Vy") == 0) {
                    level->asteroids[level->total_asteroids].vy = atof(value);
                }
                else if (strcmp(name, "Vr") == 0) {
                    level->asteroids[level->total_asteroids].angular_speed = atof(value);
                }
            }

            level->total_asteroids++;
        }
        else if (strcmp(obj[i].name, "player") == 0) {
            level->player.x = obj[i].x;// + PLAYER_SCALE / 2;
            level->player.y = obj[i].y;// + GLOBALS.game.player.texture.height * PLAYER_SCALE / GLOBALS.game.player.texture.width;
            level->player.dir = obj[i].rotation;

            for (u32 j = 0; j < obj[i].attributes_size; j++) {
                const char *name = obj[i].attributes[j].name;
                const char *value = obj[i].attributes[j].value;

                if (strcmp(name, "Energy") == 0) {
                    level->player.energy = atof(value);
                }
                else if (strcmp(name, "Impulses") == 0) {
                    level->player.impulses = atoi(value);
                }
            }
        }
        else if (strcmp(obj[i].name, "checkpoint") == 0) {
            level->checkpoint.x = obj[i].x;// + obj[i].width * 0.5f;
            level->checkpoint.y = obj[i].y;// + obj[i].height * 0.5f;
        }
    }
    return 0;
}
