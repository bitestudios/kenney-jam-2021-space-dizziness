
#include "game.h"
#include "menu.h"
#include "engine/madmath.h"


MenuOption
Make_menu_option(const char *text, f32 y_percent) {
	MenuOption result;
	result.height = 0.08f*(f32)GAMEPLAY_HEIGHT;
	f32 msg_width = Get_text_width_with_height(&GLOBALS.font, text, result.height);
	result.text = text;
	result.x0   = ((f32)GAMEPLAY_WIDTH - msg_width) * 0.5;
	result.x1   = result.x0 + msg_width;
	result.y0   = (f32)GAMEPLAY_HEIGHT * y_percent;
	result.y1   = result.y0 + result.height;
	return result;
}

ImageOption
Make_menu_image_option(Texture *texture, f32 x, f32 y, f32 width, f32 height) {
	ImageOption result;
	result.x0   = x;
	result.x1   = x + width;
	result.y0   = y;
	result.y1   = y + height;
    result.width = width;
    result.height = height;
    result.texture = texture;
	return result;
}

bool
Mouse_on_image_option(ImageOption *option) {
    f32 mouse_x, mouse_y;
	Get_mouse_on_game_screen_coords(&mouse_x, &mouse_y);
	if (mouse_x >= option->x0 && mouse_x <= option->x1 && mouse_y >= option->y0 && mouse_y <= option->y1) {
		return true;
	}
	return false;
}


bool
Mouse_on_option(MenuOption *option) {
	f32 mouse_x, mouse_y;
	Get_mouse_on_game_screen_coords(&mouse_x, &mouse_y);
	if (mouse_x >= option->x0 && mouse_x <= option->x1 && mouse_y >= option->y0 && mouse_y <= option->y1) {
		return true;
	}
	return false;
}


void
Show_image_option(ImageOption *option, OptionState state) {
    const f32 focus_scale = 1.1f;
	
	if (state != OPTION_STATE_UNSELECTED) {
        Vec4 start_color = V4(1.0f, 1.0f, 0.0f, 1.0f);
		Vec4 end_color   = V4(0.0f, 1.0f, 1.0f, 1.0f);
        f32 lerp_factor = (sinf(GLOBALS.current_time_f32*1.2f) + 1.0f) * 0.5f;
		Assert(lerp_factor >= 0.0f && lerp_factor <= 1.0f, "LERP FACTOR %f", lerp_factor);
        Vec4 color = V4_lerp(end_color, start_color, lerp_factor);

        if (state == OPTION_STATE_HOLDED) {
            color = V4(0.0f, 0.9f, 0.9f, 1.0f);
        }
        hmm_vec4 m_color = HMM_Vec4(color.r, color.g, color.b, 1.0f);
        
        f32 width = option->width * focus_scale;
        f32 height = option->height * focus_scale;
        // f32 x0 = option->x0 - (option->x0 * (1-focus_scale));
        // f32 y0 = option->y0 - (option->y0 * (1-focus_scale));
        Draw_sprite(option->x0, option->y0, width, height, 0, option->texture, option->texture->width, option->texture->height, 0, m_color);
    }
    else {
        hmm_vec4 color = RGB_DEFAULT;
        Draw_sprite(option->x0, option->y0, option->width, option->height, 0.0f, option->texture, option->texture->width, option->texture->height, 0, color);
    }
}

void
Show_option(MenuOption *option, OptionState state) {
	const f32 focus_scale = 1.1f;
	
	if (state != OPTION_STATE_UNSELECTED) {

		f32 msg_height = option->height*focus_scale;
		f32 msg_width = Get_text_width_with_height(&GLOBALS.font, option->text, msg_height);
		f32 msg_x = ((f32)GAMEPLAY_WIDTH - msg_width) * 0.5;
		Vec4 start_color = V4(1.0f, 1.0f, 0.0f, 1.0f);
		Vec4 end_color   = V4(0.0f, 1.0f, 1.0f, 1.0f);
		f32 lerp_factor = (sinf(GLOBALS.current_time_f32*1.2f) + 1.0f) * 0.5f;
		Assert(lerp_factor >= 0.0f && lerp_factor <= 1.0f, "LERP FACTOR %f", lerp_factor);
		Vec4 color = V4_lerp(end_color, start_color, lerp_factor);
		if (state == OPTION_STATE_HOLDED) {
			color = V4(0.0f, 0.9f, 0.9f, 1.0f);
		}
		Draw_text_with_height(&GLOBALS.font, option->text, msg_height, msg_x, option->y0, color.r, color.g, color.b, color.a);
	}
	else {
		Vec4 color = V4(1.0f, 1.0f, 1.0f, 1.0f);
		Draw_text_with_height(&GLOBALS.font, option->text, option->height, option->x0, option->y0, color.r, color.g, color.b, color.a);
	}
}

bool
Check_image_option(ImageOption *option) {
    OptionState state = OPTION_STATE_UNSELECTED;
    bool clicked = false;
    if (Mouse_on_image_option(option)) {
        state = OPTION_STATE_SELECTED;
        if (app_data.mouse.buttons[PLATFORM_MOUSE_BUTTON_LEFT] == KEY_STATUS_DOWN ||
            app_data.mouse.buttons[PLATFORM_MOUSE_BUTTON_LEFT] == KEY_STATUS_HOLD) {
            state = OPTION_STATE_HOLDED;
        }
        if (app_data.mouse.buttons[PLATFORM_MOUSE_BUTTON_LEFT] == KEY_STATUS_UP) {
            Mixer_play_sound(GLOBALS.launch_option_sound, PLAY_SOUND_FLAG_NONE, MAX_VOLUME);
            clicked = true;
        }
    }
    Show_image_option(option, state);
    return clicked;
}


int
Show_menu(MenuOption *options, u32 options_count, i32 selected_option, i32 *selected_option_out) {
	bool triggered = false;

	if (app_data.keyboard[PLATFORM_KEYCODE_UP] == KEY_STATUS_DOWN) {
		--selected_option;
		if (selected_option < 0) {
			selected_option = 0;
		}
		Mixer_play_sound(GLOBALS.select_option_sound, PLAY_SOUND_FLAG_NONE, MAX_VOLUME);
	}

	if (app_data.keyboard[PLATFORM_KEYCODE_DOWN] == KEY_STATUS_DOWN) {
		++selected_option;
		if (selected_option >= options_count) {
			selected_option = options_count-1;
		}
		Mixer_play_sound(GLOBALS.select_option_sound, PLAY_SOUND_FLAG_NONE, MAX_VOLUME);
	}

	bool mouse_interact = false;
	if (GLOBALS.mouse_moved ||
		app_data.mouse.buttons[PLATFORM_MOUSE_BUTTON_LEFT] != KEY_STATUS_NONE) {
		mouse_interact = true;
	}
	bool mouse_selecting = false;

	for (u32 i = 0; i < options_count; ++i) {
		OptionState option_state = OPTION_STATE_UNSELECTED;
		if (i == selected_option) {
			option_state = OPTION_STATE_SELECTED;
		}
		if (mouse_interact && Mouse_on_option(&options[i])) {
			mouse_selecting = true;
			if (selected_option != i) {
				Mixer_play_sound(GLOBALS.select_option_sound, PLAY_SOUND_FLAG_NONE, MAX_VOLUME);
				selected_option = i;
			}

			// We set the option state
			if (app_data.mouse.buttons[PLATFORM_MOUSE_BUTTON_LEFT] == KEY_STATUS_DOWN ||
				app_data.mouse.buttons[PLATFORM_MOUSE_BUTTON_LEFT] == KEY_STATUS_HOLD) {
				option_state = OPTION_STATE_HOLDED;
			}
			else {
				option_state = OPTION_STATE_SELECTED;
			}

			// If the mouse button is up and is on a option we launch that option
			if (app_data.mouse.buttons[PLATFORM_MOUSE_BUTTON_LEFT] == KEY_STATUS_UP) {
				Mixer_play_sound(GLOBALS.launch_option_sound, PLAY_SOUND_FLAG_NONE, MAX_VOLUME);
				triggered = true;
			}
		}
		Show_option(&options[i], option_state);
	}

	if (mouse_interact) {
		GLOBALS.mouse_pointing = mouse_selecting;
	}

	if (app_data.keyboard[PLATFORM_KEYCODE_ENTER] == KEY_STATUS_DOWN) {
		Mixer_play_sound(GLOBALS.launch_option_sound, PLAY_SOUND_FLAG_NONE, MAX_VOLUME);
		triggered = true;
	}

	*selected_option_out = selected_option;
	return triggered;
}

