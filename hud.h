#ifndef _HUD_H_
#define _HUD_H_

#include "game.h"

#define HUD_MARGIN_LEFT 5
#define HUD_SPACING 5
#define HUD_IMPULSE_SIZE 15

#define HUD_BOLT_SCALE 10
#define HUD_ENERGY_WIDTH 4

#define HUD_BUTTON_SCALE 20
#define HUD_TEXT_HEIGHT 23

#define HUD_LEVEL_COUNTER_X 160
#define HUD_LEVEL_COUNTER_HEIGHT 15

void
Init_hud();

void
Draw_hud();

#endif
