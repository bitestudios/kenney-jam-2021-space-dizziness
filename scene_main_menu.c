
#include "menu.h"
#include "scene_game.h"

#define TOTAL_MENU_ASTEROIDS 10

void
Scene_main_menu() {


	static Asteroid asteroids[10] = {
		{
			.x = GAMEPLAY_WIDTH * 0.8,
			.y = GAMEPLAY_HEIGHT * 0.5,
			.scale_x = 60,
			.scale_y = 60,
			.angular_vel = 5.0f,
		},
		{
			.x = GAMEPLAY_WIDTH * 0.1,
			.y = GAMEPLAY_HEIGHT * 0.2,
			.scale_x = 20,
			.scale_y = 20,
			.angular_vel = -15.0f
		},
		{
			.x = GAMEPLAY_WIDTH * 0.15,
			.y = GAMEPLAY_HEIGHT * 0.1,
			.scale_x = 18,
			.scale_y = 18,
			.angular_vel = -15.0f
		},
		{
			.x = GAMEPLAY_WIDTH * 0.08,
			.y = GAMEPLAY_HEIGHT * 0.08,
			.scale_x = 15,
			.scale_y = 15,
			.angular_vel = 8.0f
		},
		{
			.x = GAMEPLAY_WIDTH * 0.15,
			.y = GAMEPLAY_HEIGHT * 0.65,
			.scale_x = 40,
			.scale_y = 40,
			.angular_vel = 7.0f,
		},
		{
			.x = GAMEPLAY_WIDTH * 0.85,
			.y = GAMEPLAY_HEIGHT * 0.15,
			.scale_x = 35,
			.scale_y = 35,
			.angular_vel = 10.0f,
		},
		{
			.x = GAMEPLAY_WIDTH * 0.15,
			.y = GAMEPLAY_HEIGHT * 0.4,
			.scale_x = 25,
			.scale_y = 25,
			.angular_vel = -6.0f
		},
		{
			.x = GAMEPLAY_WIDTH * 0.9,
			.y = GAMEPLAY_HEIGHT * 0.3,
			.scale_x = 17,
			.scale_y = 17,
			.angular_vel = -15.0f
		},
		{
			.x = GAMEPLAY_WIDTH * 0.85,
			.y = GAMEPLAY_HEIGHT * 0.7,
			.scale_x = 15,
			.scale_y = 15,
			.angular_vel = 10.0f
		},
		{
			.x = GAMEPLAY_WIDTH * 0.1,
			.y = GAMEPLAY_HEIGHT * 0.2,
			.scale_x = 20,
			.scale_y = 20,
			.angular_vel = -15.0f
		},
	};
    
	f32 min_alpha = 0.5f;
	f32 max_alpha = 1.2f;
	f32 starfield_alpha = Lerp(min_alpha, max_alpha, (sinf(GLOBALS.current_time_f32*1.5f)+1.0f) * 0.5f);
	Draw_sprite_ex(0.0f, 0.0f, GAMEPLAY_WIDTH, GAMEPLAY_HEIGHT, 0.0f, 0.0f, 0.0f, &GLOBALS.starfield_bg, HMM_Vec4(1.0f, 1.0f, 1.0f, starfield_alpha));
	
	for (u32 i = 0; i < TOTAL_MENU_ASTEROIDS; ++i) {
		Draw_dummy_asteroid(&asteroids[i]);
	}

	f32 title_height = (f32)GAMEPLAY_HEIGHT * 0.11f;
	f32 title_width = Get_text_width_with_height(&GLOBALS.font, "SPACE DIZZINESS", title_height);
	f32 title_x = ((f32)GAMEPLAY_WIDTH - title_width) * 0.5f;
	f32 title_y = (f32)GAMEPLAY_HEIGHT * 0.80;
	Draw_text_with_height(&GLOBALS.font, "SPACE DIZZINESS", title_height, title_x, title_y, 1, 1, 1, 1);
	
	if (Show_menu(
			GLOBALS.main_menu_options,
			MAIN_MENU_OPTION_COUNT,
			GLOBALS.main_menu_selected_option,
			&GLOBALS.main_menu_selected_option)) {

		GLOBALS.mouse_pointing = false;

		if (GLOBALS.main_menu_selected_option == MAIN_MENU_OPTION_PLAY) {
			GLOBALS.current_scene = SCENE_GAME;
			GLOBALS.game.current_level = 0;
			Scene_game_prepare_level();
			Mixer_clear_tracks();
			Mixer_play_sound(GLOBALS.launch_option_sound, PLAY_SOUND_FLAG_NONE, MAX_VOLUME);
			Mixer_play_sound(GLOBALS.main_theme_music, PLAY_SOUND_FLAG_LOOP, MAX_VOLUME);
		}
		else if (GLOBALS.main_menu_selected_option == MAIN_MENU_OPTION_CREDITS) {
			GLOBALS.current_scene = SCENE_CREDITS;
		}
        else if (GLOBALS.main_menu_selected_option == MAIN_MENU_OPTION_CONTROLS) {
            Set_controls_return_scene(SCENE_MENU);
            GLOBALS.current_scene = SCENE_CONTROLS;
        }
		else if (GLOBALS.main_menu_selected_option == MAIN_MENU_OPTION_EXIT) {
#if !defined(PLATFORM_WASM)
			GLOBALS.quit = true;
#endif
		}
	}

	{
		Texture * tex = &GLOBALS.kenney_jam_logo;
		f32 w = 100.0f;
		f32 h = (f32)tex->height / (f32)tex->width * w;
		Draw_sprite_ex(GAMEPLAY_WIDTH-w-5.0f, 5.0f, w, h, 0.0f, 0.0f, 0.0f, tex, HMM_Vec4(1.0f, 1.0f, 1.0f, 0.6f));
	}

}



