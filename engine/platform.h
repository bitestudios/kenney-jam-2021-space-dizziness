//
//
//
//

#ifndef _PLATFORM_H_
#define _PLATFORM_H_

#include "base.h"
#include <string.h>


#if defined(WIN32)
    /* Windows */
	#define PLATFORM_WINDOWS (1)
	#include <windows.h>

#elif defined(WASM)
	/* Wasm */
	#define PLATFORM_WASM (1)
	#define BUNDLED_TREE (1)  // Wasm always need the bundled tree
	#define GL_ES2_COMPAT (1)

#elif defined(__linux__) || defined(__unix__)
    /* Linux */
	#define PLATFORM_LINUX (1)
	#define GL_ES2_COMPAT (1)

#endif


///////////////////////////////////////////////////////////////////////////
//
//
//
// Basic GL application API
//
//
//
///////////////////////////////////////////////////////////////////////////



typedef enum {
	KEY_STATUS_NONE,
	KEY_STATUS_DOWN,
	KEY_STATUS_HOLD,
	KEY_STATUS_UP,
} KeyStatus;

/* Based on GLFW */
typedef enum {
    PLATFORM_KEYCODE_INVALID          = 0,
    PLATFORM_KEYCODE_SPACE            = 32,
    PLATFORM_KEYCODE_APOSTROPHE       = 39,  /* ' */
    PLATFORM_KEYCODE_COMMA            = 44,  /* , */
    PLATFORM_KEYCODE_MINUS            = 45,  /* - */
    PLATFORM_KEYCODE_PERIOD           = 46,  /* . */
    PLATFORM_KEYCODE_SLASH            = 47,  /* / */
    PLATFORM_KEYCODE_0                = 48,
    PLATFORM_KEYCODE_1                = 49,
    PLATFORM_KEYCODE_2                = 50,
    PLATFORM_KEYCODE_3                = 51,
    PLATFORM_KEYCODE_4                = 52,
    PLATFORM_KEYCODE_5                = 53,
    PLATFORM_KEYCODE_6                = 54,
    PLATFORM_KEYCODE_7                = 55,
    PLATFORM_KEYCODE_8                = 56,
    PLATFORM_KEYCODE_9                = 57,
    PLATFORM_KEYCODE_SEMICOLON        = 59,  /* ; */
    PLATFORM_KEYCODE_EQUAL            = 61,  /* = */
    PLATFORM_KEYCODE_A                = 65,
    PLATFORM_KEYCODE_B                = 66,
    PLATFORM_KEYCODE_C                = 67,
    PLATFORM_KEYCODE_D                = 68,
    PLATFORM_KEYCODE_E                = 69,
    PLATFORM_KEYCODE_F                = 70,
    PLATFORM_KEYCODE_G                = 71,
    PLATFORM_KEYCODE_H                = 72,
    PLATFORM_KEYCODE_I                = 73,
    PLATFORM_KEYCODE_J                = 74,
    PLATFORM_KEYCODE_K                = 75,
    PLATFORM_KEYCODE_L                = 76,
    PLATFORM_KEYCODE_M                = 77,
    PLATFORM_KEYCODE_N                = 78,
    PLATFORM_KEYCODE_O                = 79,
    PLATFORM_KEYCODE_P                = 80,
    PLATFORM_KEYCODE_Q                = 81,
    PLATFORM_KEYCODE_R                = 82,
    PLATFORM_KEYCODE_S                = 83,
    PLATFORM_KEYCODE_T                = 84,
    PLATFORM_KEYCODE_U                = 85,
    PLATFORM_KEYCODE_V                = 86,
    PLATFORM_KEYCODE_W                = 87,
    PLATFORM_KEYCODE_X                = 88,
    PLATFORM_KEYCODE_Y                = 89,
    PLATFORM_KEYCODE_Z                = 90,
    PLATFORM_KEYCODE_LEFT_BRACKET     = 91,  /* [ */
    PLATFORM_KEYCODE_BACKSLASH        = 92,  /* \ */
    PLATFORM_KEYCODE_RIGHT_BRACKET    = 93,  /* ] */
    PLATFORM_KEYCODE_GRAVE_ACCENT     = 96,  /* ` */
    PLATFORM_KEYCODE_WORLD_1          = 161, /* non-US #1 */
    PLATFORM_KEYCODE_WORLD_2          = 162, /* non-US #2 */
    PLATFORM_KEYCODE_ESCAPE           = 256,
    PLATFORM_KEYCODE_ENTER            = 257,
    PLATFORM_KEYCODE_TAB              = 258,
    PLATFORM_KEYCODE_BACKSPACE        = 259,
    PLATFORM_KEYCODE_INSERT           = 260,
    PLATFORM_KEYCODE_DELETE           = 261,
    PLATFORM_KEYCODE_RIGHT            = 262,
    PLATFORM_KEYCODE_LEFT             = 263,
    PLATFORM_KEYCODE_DOWN             = 264,
    PLATFORM_KEYCODE_UP               = 265,
    PLATFORM_KEYCODE_PAGE_UP          = 266,
    PLATFORM_KEYCODE_PAGE_DOWN        = 267,
    PLATFORM_KEYCODE_HOME             = 268,
    PLATFORM_KEYCODE_END              = 269,
    PLATFORM_KEYCODE_CAPS_LOCK        = 280,
    PLATFORM_KEYCODE_SCROLL_LOCK      = 281,
    PLATFORM_KEYCODE_NUM_LOCK         = 282,
    PLATFORM_KEYCODE_PRINT_SCREEN     = 283,
    PLATFORM_KEYCODE_PAUSE            = 284,
    PLATFORM_KEYCODE_F1               = 290,
    PLATFORM_KEYCODE_F2               = 291,
    PLATFORM_KEYCODE_F3               = 292,
    PLATFORM_KEYCODE_F4               = 293,
    PLATFORM_KEYCODE_F5               = 294,
    PLATFORM_KEYCODE_F6               = 295,
    PLATFORM_KEYCODE_F7               = 296,
    PLATFORM_KEYCODE_F8               = 297,
    PLATFORM_KEYCODE_F9               = 298,
    PLATFORM_KEYCODE_F10              = 299,
    PLATFORM_KEYCODE_F11              = 300,
    PLATFORM_KEYCODE_F12              = 301,
    PLATFORM_KEYCODE_F13              = 302,
    PLATFORM_KEYCODE_F14              = 303,
    PLATFORM_KEYCODE_F15              = 304,
    PLATFORM_KEYCODE_F16              = 305,
    PLATFORM_KEYCODE_F17              = 306,
    PLATFORM_KEYCODE_F18              = 307,
    PLATFORM_KEYCODE_F19              = 308,
    PLATFORM_KEYCODE_F20              = 309,
    PLATFORM_KEYCODE_F21              = 310,
    PLATFORM_KEYCODE_F22              = 311,
    PLATFORM_KEYCODE_F23              = 312,
    PLATFORM_KEYCODE_F24              = 313,
    PLATFORM_KEYCODE_F25              = 314,
    PLATFORM_KEYCODE_KP_0             = 320,
    PLATFORM_KEYCODE_KP_1             = 321,
    PLATFORM_KEYCODE_KP_2             = 322,
    PLATFORM_KEYCODE_KP_3             = 323,
    PLATFORM_KEYCODE_KP_4             = 324,
    PLATFORM_KEYCODE_KP_5             = 325,
    PLATFORM_KEYCODE_KP_6             = 326,
    PLATFORM_KEYCODE_KP_7             = 327,
    PLATFORM_KEYCODE_KP_8             = 328,
    PLATFORM_KEYCODE_KP_9             = 329,
    PLATFORM_KEYCODE_KP_DECIMAL       = 330,
    PLATFORM_KEYCODE_KP_DIVIDE        = 331,
    PLATFORM_KEYCODE_KP_MULTIPLY      = 332,
    PLATFORM_KEYCODE_KP_SUBTRACT      = 333,
    PLATFORM_KEYCODE_KP_ADD           = 334,
    PLATFORM_KEYCODE_KP_ENTER         = 335,
    PLATFORM_KEYCODE_KP_EQUAL         = 336,
    PLATFORM_KEYCODE_LEFT_SHIFT       = 340,
    PLATFORM_KEYCODE_LEFT_CONTROL     = 341,
    PLATFORM_KEYCODE_LEFT_ALT         = 342,
    PLATFORM_KEYCODE_LEFT_SUPER       = 343,
    PLATFORM_KEYCODE_RIGHT_SHIFT      = 344,
    PLATFORM_KEYCODE_RIGHT_CONTROL    = 345,
    PLATFORM_KEYCODE_RIGHT_ALT        = 346,
    PLATFORM_KEYCODE_RIGHT_SUPER      = 347,
    PLATFORM_KEYCODE_MENU             = 348,
	PLATFORM_MAX_KEYCODES,
} PlatformKeyCode;


typedef enum {
	PLATFORM_MOUSE_BUTTON_LEFT   = 0,
	PLATFORM_MOUSE_BUTTON_RIGHT  = 1,
	PLATFORM_MOUSE_BUTTON_MIDDLE = 2,
	PLATFORM_MOUSE_BUTTON_INVALID = 3,
} PlatformMouseButton;

//
// Global struct that will contain all the crossplatform application data
//
static struct AppData {
	u32  w_width;
	u32  w_height;
	u32  default_width;
	u32  default_height;
	bool window_resized;
	bool fullscreen_changed;
    u8   keyboard[PLATFORM_MAX_KEYCODES];
	struct {
		f32 x;
		f32 y;
		u8  buttons[3];
	} mouse;
	u8   quit_requested;
} app_data = {0};



//
// Structure that describes the window properties to create one
//
typedef struct {
	const char *title;
	u32 width;
	u32 height;
} WindowDesc;


/*
* Create_window
* =============================================================
*
* DESCRIPTIION
* --------------------------------------------------------------
* Creates a windows based on the description passed.
* This must be called before Setup_GL and any window operation.
* To close the window properly, Destroy_window must be called
* at the end.
*
* PARAMS
* --------------------------------------------------------------
*  window_desc: description of the window.
*
* RETURN
* --------------------------------------------------------------
* 0 on success, -1 on failure.
*
*/
int
Create_window(WindowDesc *window_desc);




/*
* Destroy_window
* =============================================================
*
* DESCRIPTIION
* --------------------------------------------------------------
* Destroys the window
*
*/
void
Destroy_window(void);

/*
* Set_fullscreen
* =============================================================
*
* DESCRIPTIION
* --------------------------------------------------------------
* Changes to fullscreen or windowed depending if is true (fullscreen) or
* false (windowed).
*
* The resize event will be triggered when this is called.
*
* PARAMS
* --------------------------------------------------------------
*  enable: set to fullscreen or not
*
*/
void
Set_fullscreen(bool enable);


/*
* Show_mouse
* =============================================================
*
* DESCRIPTIION
* --------------------------------------------------------------
* Shows or hides the mouse depending on the parameter.
* true = shows
* false = hides
*
* PARAMS
* --------------------------------------------------------------
*  show: if shows or not
*
*/
void
Show_mouse(bool show);


/*
* Set_window_title
* =============================================================
*
* DESCRIPTIION
* --------------------------------------------------------------
* Changes the window title.
*
* PARAMS
* --------------------------------------------------------------
*  title: Title to change
*
*/
void
Set_window_title(const char *title);



/*
* Setup_GL
* =============================================================
*
* DESCRIPTIION
* --------------------------------------------------------------
* Setups a GL context using the window created previously with 
* Create_window.
* To finalize all the GL stuff, Cleanup_GL must be called.
*
* RETURN
* --------------------------------------------------------------
* 0 on success, -1 on failure.
*
*/
int
Setup_GL(void);



/*
* Cleanup_GL
* =============================================================
*
* DESCRIPTIION
* --------------------------------------------------------------
* Cleans al the GL stuff.
*
*/
void
Cleanup_GL();



/*
* Set_swap_interval
* =============================================================
*
* DESCRIPTIION
* --------------------------------------------------------------
* Changes the swap interval between the backbuffer and the front buffer
* This must be called after Setup_GL
*
* 0 disables the swap interval so unlimits the fps rate.
*
* NOTE: This doesn't have any effect on WASM.
*
* PARAMS
* --------------------------------------------------------------
* interval
*
*/
void
Set_swap_interval(u8 interval);


/*
* Clear_events
* =============================================================
*
* DESCRIPTIION
* --------------------------------------------------------------
* Clears the events that are volatile between frames, such as window resizing
* and keys up.
* This is a internal function shouldn't be used from outside.
*
*/
void
Clear_events();


/*
* Clear_events
* =============================================================
*
* DESCRIPTIION
* --------------------------------------------------------------
* Put al keys thar are down to up. This is used when the window
* loses the focus.
*/
void
Put_keys_up();


/*
* Run_application_loop
* =============================================================
*
* DESCRIPTIION
* --------------------------------------------------------------
* Runs the application loop, recives a application frame handler
* which will execute every frame.
* If the handler returns 0 the application should continue normally
* If the handler returns 1 the application should finish.
* If the handler returns <0 the application will have an abnormal finish.
*
* Take into account that this function don't does any resource cleanup
* when the handler decides tu finish, all resource cleanup should be
* done inside the frame handler.
*
* This function varies in implementaton between wasm and other platforms
* because in javascript we need to use the frame handler as a event listener.
* Look at platform_wasm.h, WAFN_application_frame().
* 
* The way to call this function from main should be:
*
* int
* main(...) {
*	....
*   return Run_application_loop(My_frame_handler);
* }
*
*
* VARIABLES
* -------------------------------------------------------------------
*   application_frame: The frame handler
*/
int
Run_application_loop(int(*application_frame)(void));











/////////////////////////////////////////////
// 
//
//
// Audio API (Mostly based on sokol_audio) 
//
//
//
////////////////////////////////////////////

typedef int (*SoundPlayerCallback) (void *user_data, float* buffer, int n_frames, int n_channels);


//
// Global variable to access the current sound player information
// This will only have valid values after call successfully Launch_sound_player
//
static struct SoundPlayerInfo {
	int sample_rate;
	int num_channels;
	int buffer_frames;
	void *user_data;
	SoundPlayerCallback stream_cb;
} sound_player_info = {0};


//
// Description with the desired when the player is launched
//
typedef struct {
	int sample_rate;        /* requested sample rate */
	int num_channels;       /* number of channels, default: 1 (mono) */
	int buffer_frames;      /* number of frames in streaming buffer */
    void *user_data;        /* optional user data argument for stream_userdata_cb */
	SoundPlayerCallback stream_cb; /* Streaming callback */
} SoundPlayerDesc;


/*
* Launch_sound_player
* =============================================================
*
* DESCRIPTIION
* --------------------------------------------------------------
* Launchs a thread (On linux and windows) or a async rutine (wasm)
* Which will play the audio.
*
* When it needs a new chunk of samples will call the callback
*
* VARIABLES
* -------------------------------------------------------------------
*   desc: Description
*
*/
int
Launch_sound_player(SoundPlayerDesc *desc);


/*
* Terminate_sound_player
* =============================================================
*
* DESCRIPTIION
* --------------------------------------------------------------
* Terminates the player routine.
*
*/
void
Terminate_sound_player(void);








////////////////////////////////////////////////////////////////////////////
//
//
//
// FILESYSTEM API
//
//
//
////////////////////////////////////////////////////////////////////////////



/*
* Get_file_contentsz
* =============================================================
*
* DESCRIPTIION
* --------------------------------------------------------------
* Returns a NULL terminated array with de contents of the file,
* the parameter size is setted with de file size (without counting
* the appended 0).
* If the file can't be found, will return a NULL
*
* If we are on BUNDLED_TREE mode the data returned will be a pointer
* to the data embeded into de binary.
*
* If not, the data will be heap allocated.
*
* To free this data on a safe way we have to call to Free_file_contents()
* this function will take into account the mode.
*
* VARIABLES
* -------------------------------------------------------------------
*   filename: Name of the file
*   size: Pointer where store the result size, if is NULL will be ignored
*
* RETURNS
* -------------------------------------------------------------------
*   u8* : Pointer to the data.
*/
u8 *
Get_file_contentsz(const char *filename, u32 *size);




/*
* Free_file_contents
* =============================================================
*
* DESCRIPTIION
* --------------------------------------------------------------
* Frees the data getted by Get_file_contentsz
* if the value is NULL nothing happens
*
* VARIABLES
* -------------------------------------------------------------------
*   data: Pointer to the data to free
*/
void
Free_file_contents(u8 *data);




/*
* Free_file_contents
* =============================================================
*
* DESCRIPTIION
* --------------------------------------------------------------
* Gets the file modify time. This will always return 0 on bundled tree mode
*
* VARIABLES
* -------------------------------------------------------------------
*   filename: The name of the file
*
* RETURN
* ------------------------------------------------------------------
*  The modify time
*/
u64
Get_file_modify_time(const char *filename);





////////////////////////////////////////////////////////////////////////////
//
//
//
// TIME API
//
//
//
////////////////////////////////////////////////////////////////////////////

// 
// The basic time unit used here is the nanosecond (10^9 seconds)
//



// Macros to express seconds and miliseconds in nanoseconds
#define T_SECOND   (1000000000)
#define T_MSECOND  (1000000)



/*
* To_sec_f32
* =============================================================
*
* DESCRIPTIION
* --------------------------------------------------------------
* Converts signed 64 bits nanoseconds to float32 seconds
*
* PARAMS
* --------------------------------------------------------------
*  nseconds
*
* RETURN
* --------------------------------------------------------------
*  Seconds in f32
*
*/
static inline f32
To_sec_f32(i64 nseconds) { return ((f32)nseconds) / (f32)(1*T_SECOND); }



/*
* To_sec_f64
* =============================================================
*
* DESCRIPTIION
* --------------------------------------------------------------
* Converts signed 64 bits nanoseconds to float64 seconds
*
* PARAMS
* --------------------------------------------------------------
*  nseconds
*
* RETURN
* --------------------------------------------------------------
*  Seconds in f64
*
*/
static inline f64
To_sec_f64(i64 nseconds) { return ((f64)nseconds) / (f64)(1*T_SECOND); }


/*
* Init_app_time
* =============================================================
*
* DESCRIPTIION
* --------------------------------------------------------------
* Inits the app starting time.
* This function should be called at the start of the application
*
*/
void
Init_app_time();


/*
* App_time
* =============================================================
*
* DESCRIPTIION
* --------------------------------------------------------------
* Returns the nanoseconds passed from the last call to Init_app_time.
*
* RETURN
* --------------------------------------------------------------
*  The nanoseconds.
*
*/
i64
App_time();






////////////////////////////////////////////////////////////////////////////
//
//
//
// THREAD API
//
//
//
////////////////////////////////////////////////////////////////////////////


#if defined(PLATFORM_LINUX)

#include <pthread.h>
typedef pthread_mutex_t Mutex;

#elif defined(PLATFORM_WINDOWS)

typedef HANDLE Mutex;

#elif defined(PLATFORM_WASM)

typedef u8 Mutex;

#endif

void
Mutex_init(Mutex *mutex);


void
Mutex_deinit(Mutex *mutex);


void
Mutex_lock(Mutex *mutex);


void
Mutex_unlock(Mutex *mutex);


///////////////////////////////////////////////////////////////////
//
//
// GL declarations
//
//
//////////////////////////////////////////////////////////////////

#include <stddef.h>

#define __gl_h_ 1
#define __gl32_h_ 1
#define __gl31_h_ 1
#define __GL_H__ 1
#define __glext_h_ 1
#define __GLEXT_H_ 1
#define __gltypes_h_ 1
#define __glcorearb_h_ 1
#define __gl_glcorearb_h_ 1
#define GL_APIENTRY APIENTRY

typedef unsigned int  GLenum;
typedef unsigned int  GLuint;
typedef int  GLsizei;
typedef char  GLchar;
typedef ptrdiff_t  GLintptr;
typedef ptrdiff_t  GLsizeiptr;
typedef double  GLclampd;
typedef unsigned short  GLushort;
typedef unsigned char  GLubyte;
typedef unsigned char  GLboolean;
typedef uint64_t  GLuint64;
typedef double  GLdouble;
typedef unsigned short  GLhalf;
typedef float  GLclampf;
typedef unsigned int  GLbitfield;
typedef signed char  GLbyte;
typedef short  GLshort;
typedef void  GLvoid;
typedef int64_t  GLint64;
typedef float  GLfloat;
typedef struct __GLsync * GLsync;
typedef int  GLint;
#define GL_INT_2_10_10_10_REV 0x8D9F
#define GL_R32F 0x822E
#define GL_PROGRAM_POINT_SIZE 0x8642
#define GL_STENCIL_ATTACHMENT 0x8D20
#define GL_DEPTH_ATTACHMENT 0x8D00
#define GL_COLOR_ATTACHMENT2 0x8CE2
#define GL_COLOR_ATTACHMENT0 0x8CE0
#define GL_R16F 0x822D
#define GL_COLOR_ATTACHMENT22 0x8CF6
#define GL_DRAW_FRAMEBUFFER 0x8CA9
#define GL_FRAMEBUFFER_COMPLETE 0x8CD5
#define GL_NUM_EXTENSIONS 0x821D
#define GL_INFO_LOG_LENGTH 0x8B84
#define GL_VERTEX_SHADER 0x8B31
#define GL_INCR 0x1E02
#define GL_DYNAMIC_DRAW 0x88E8
#define GL_STATIC_DRAW 0x88E4
#define GL_TEXTURE_CUBE_MAP_POSITIVE_Z 0x8519
#define GL_TEXTURE_CUBE_MAP 0x8513
#define GL_FUNC_SUBTRACT 0x800A
#define GL_FUNC_REVERSE_SUBTRACT 0x800B
#define GL_CONSTANT_COLOR 0x8001
#define GL_DECR_WRAP 0x8508
#define GL_LINEAR_MIPMAP_LINEAR 0x2703
#define GL_ELEMENT_ARRAY_BUFFER 0x8893
#define GL_SHORT 0x1402
#define GL_DEPTH_TEST 0x0B71
#define GL_TEXTURE_CUBE_MAP_NEGATIVE_Y 0x8518
#define GL_LINK_STATUS 0x8B82
#define GL_TEXTURE_CUBE_MAP_POSITIVE_Y 0x8517
#define GL_SAMPLE_ALPHA_TO_COVERAGE 0x809E
#define GL_RGBA16F 0x881A
#define GL_CONSTANT_ALPHA 0x8003
#define GL_READ_FRAMEBUFFER 0x8CA8
#define GL_TEXTURE0 0x84C0
#define GL_TEXTURE_MIN_LOD 0x813A
#define GL_CLAMP_TO_EDGE 0x812F
#define GL_UNSIGNED_SHORT_5_6_5 0x8363
#define GL_TEXTURE_WRAP_R 0x8072
#define GL_UNSIGNED_SHORT_5_5_5_1 0x8034
#define GL_NEAREST_MIPMAP_NEAREST 0x2700
#define GL_UNSIGNED_SHORT_4_4_4_4 0x8033
#define GL_SRC_ALPHA_SATURATE 0x0308
#define GL_STREAM_DRAW 0x88E0
#define GL_ONE 1
#define GL_NEAREST_MIPMAP_LINEAR 0x2702
#define GL_RGB10_A2 0x8059
#define GL_RGBA8 0x8058
#define GL_COLOR_ATTACHMENT1 0x8CE1
#define GL_RGBA4 0x8056
#define GL_RGB8 0x8051
#define GL_ARRAY_BUFFER 0x8892
#define GL_STENCIL 0x1802
#define GL_TEXTURE_2D 0x0DE1
#define GL_DEPTH 0x1801
#define GL_FRONT 0x0404
#define GL_STENCIL_BUFFER_BIT 0x00000400
#define GL_REPEAT 0x2901
#define GL_RGBA 0x1908
#define GL_TEXTURE_CUBE_MAP_POSITIVE_X 0x8515
#define GL_DECR 0x1E03
#define GL_FRAGMENT_SHADER 0x8B30
#define GL_FLOAT 0x1406
#define GL_TEXTURE_MAX_LOD 0x813B
#define GL_DEPTH_COMPONENT 0x1902
#define GL_ONE_MINUS_DST_ALPHA 0x0305
#define GL_COLOR 0x1800
#define GL_TEXTURE_2D_ARRAY 0x8C1A
#define GL_TRIANGLES 0x0004
#define GL_UNSIGNED_BYTE 0x1401
#define GL_TEXTURE_MAG_FILTER 0x2800
#define GL_ONE_MINUS_CONSTANT_ALPHA 0x8004
#define GL_NONE 0
#define GL_SRC_COLOR 0x0300
#define GL_BYTE 0x1400
#define GL_TEXTURE_CUBE_MAP_NEGATIVE_Z 0x851A
#define GL_LINE_STRIP 0x0003
#define GL_TEXTURE_3D 0x806F
#define GL_CW 0x0900
#define GL_LINEAR 0x2601
#define GL_RENDERBUFFER 0x8D41
#define GL_GEQUAL 0x0206
#define GL_COLOR_BUFFER_BIT 0x00004000
#define GL_RGBA32F 0x8814
#define GL_BLEND 0x0BE2
#define GL_ONE_MINUS_SRC_ALPHA 0x0303
#define GL_ONE_MINUS_CONSTANT_COLOR 0x8002
#define GL_TEXTURE_WRAP_T 0x2803
#define GL_TEXTURE_WRAP_S 0x2802
#define GL_TEXTURE_MIN_FILTER 0x2801
#define GL_LINEAR_MIPMAP_NEAREST 0x2701
#define GL_EXTENSIONS 0x1F03
#define GL_NO_ERROR 0
#define GL_REPLACE 0x1E01
#define GL_KEEP 0x1E00
#define GL_CCW 0x0901
#define GL_TEXTURE_CUBE_MAP_NEGATIVE_X 0x8516
#define GL_RGB 0x1907
#define GL_TRIANGLE_STRIP 0x0005
#define GL_TRIANGLE_FAN 0x0006
#define GL_FALSE 0
#define GL_ZERO 0
#define GL_CULL_FACE 0x0B44
#define GL_INVERT 0x150A
#define GL_INT 0x1404
#define GL_UNSIGNED_INT 0x1405
#define GL_UNSIGNED_SHORT 0x1403
#define GL_NEAREST 0x2600
#define GL_SCISSOR_TEST 0x0C11
#define GL_LEQUAL 0x0203
#define GL_STENCIL_TEST 0x0B90
#define GL_DITHER 0x0BD0
#define GL_DEPTH_COMPONENT16 0x81A5
#define GL_EQUAL 0x0202
#define GL_FRAMEBUFFER 0x8D40
#define GL_RGB5 0x8050
#define GL_LINES 0x0001
#define GL_DEPTH_BUFFER_BIT 0x00000100
#define GL_SRC_ALPHA 0x0302
#define GL_INCR_WRAP 0x8507
#define GL_LESS 0x0201
#define GL_MULTISAMPLE 0x809D
#define GL_FRAMEBUFFER_BINDING 0x8CA6
#define GL_BACK 0x0405
#define GL_ALWAYS 0x0207
#define GL_FUNC_ADD 0x8006
#define GL_ONE_MINUS_DST_COLOR 0x0307
#define GL_NOTEQUAL 0x0205
#define GL_DST_COLOR 0x0306
#define GL_COMPILE_STATUS 0x8B81
#if defined (GL_ES2_COMPAT)
	#define GL_RED 0x1909 // GL_LUMINANCE
#else
	#define GL_RED 0x1903
#endif
#define GL_COLOR_ATTACHMENT3 0x8CE3
#define GL_DST_ALPHA 0x0304
#define GL_RGB5_A1 0x8057
#define GL_GREATER 0x0204
#define GL_POLYGON_OFFSET_FILL 0x8037
#define GL_TRUE 1
#define GL_NEVER 0x0200
#define GL_POINTS 0x0000
#define GL_ONE_MINUS_SRC_COLOR 0x0301
#define GL_MIRRORED_REPEAT 0x8370
#define GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS 0x8B4D
#define GL_R11F_G11F_B10F 0x8C3A
#define GL_UNSIGNED_INT_10F_11F_11F_REV 0x8C3B
#define GL_RGBA32UI 0x8D70
#define GL_RGB32UI 0x8D71
#define GL_RGBA16UI 0x8D76
#define GL_RGB16UI 0x8D77
#define GL_RGBA8UI 0x8D7C
#define GL_RGB8UI 0x8D7D
#define GL_RGBA32I 0x8D82
#define GL_RGB32I 0x8D83
#define GL_RGBA16I 0x8D88
#define GL_RGB16I 0x8D89
#define GL_RGBA8I 0x8D8E
#define GL_RGB8I 0x8D8F
#define GL_RED_INTEGER 0x8D94
#define GL_RG 0x8227
#define GL_RG_INTEGER 0x8228
#if defined (GL_ES2_COMPAT)
	#define GL_R8 0x1909 // GL_LUMINANCE
#else
	#define GL_R8 0x8229 
#endif
#define GL_R16 0x822A
#define GL_RG8 0x822B
#define GL_RG16 0x822C
#define GL_R16F 0x822D
#define GL_R32F 0x822E
#define GL_RG16F 0x822F
#define GL_RG32F 0x8230
#define GL_R8I 0x8231
#define GL_R8UI 0x8232
#define GL_R16I 0x8233
#define GL_R16UI 0x8234
#define GL_R32I 0x8235
#define GL_R32UI 0x8236
#define GL_RG8I 0x8237
#define GL_RG8UI 0x8238
#define GL_RG16I 0x8239
#define GL_RG16UI 0x823A
#define GL_RG32I 0x823B
#define GL_RG32UI 0x823C
#define GL_RGBA_INTEGER 0x8D99
#define GL_R8_SNORM 0x8F94
#define GL_RG8_SNORM 0x8F95
#define GL_RGB8_SNORM 0x8F96
#define GL_RGBA8_SNORM 0x8F97
#define GL_R16_SNORM 0x8F98
#define GL_RG16_SNORM 0x8F99
#define GL_RGB16_SNORM 0x8F9A
#define GL_RGBA16_SNORM 0x8F9B
#define GL_RGBA16 0x805B
#define GL_MAX_TEXTURE_SIZE 0x0D33
#define GL_MAX_CUBE_MAP_TEXTURE_SIZE 0x851C
#define GL_MAX_3D_TEXTURE_SIZE 0x8073
#define GL_MAX_ARRAY_TEXTURE_LAYERS 0x88FF
#define GL_MAX_VERTEX_ATTRIBS 0x8869
#define GL_CLAMP_TO_BORDER 0x812D
#define GL_TEXTURE_BORDER_COLOR 0x1004
#define GL_CURRENT_PROGRAM 0x8B8D
#define GL_MAX_VERTEX_UNIFORM_VECTORS 0x8DFB
#define GL_PACK_ALIGNMENT 0x0D05


#if !defined(PLATFORM_WINDOWS) // On windows we use function pointers

extern void glBindVertexArray(GLuint array);
extern void glFramebufferTextureLayer(GLenum target, GLenum attachment, GLuint texture, GLint level, GLint layer);
extern void glGenFramebuffers(GLsizei n, GLuint * framebuffers);
extern void glBindFramebuffer(GLenum target, GLuint framebuffer);
extern void glBindRenderbuffer(GLenum target, GLuint renderbuffer);
extern const GLubyte *glGetStringi(GLenum name, GLuint index);
extern void glClearBufferfi(GLenum buffer, GLint drawbuffer, GLfloat depth, GLint stencil);
extern void glClearBufferfv(GLenum buffer, GLint drawbuffer, const GLfloat * value);
extern void glClearBufferuiv(GLenum buffer, GLint drawbuffer, const GLuint * value);
extern void glClearBufferiv(GLenum buffer, GLint drawbuffer, const GLint * value);
extern void glDeleteRenderbuffers(GLsizei n, const GLuint * renderbuffers);
extern void glUniform1f(GLint location, GLfloat v0);
extern void glUniform2f(GLint location, GLfloat v0, GLfloat v1);
extern void glUniform3f(GLint location, GLfloat v0, GLfloat v1, GLfloat v2);
extern void glUniform4f(GLint location, const GLfloat v0, const GLfloat v1, const GLfloat v2, const GLfloat v3);
extern void glUniform4fv(GLint location, GLsizei count, const GLfloat * value);
extern void glUniform2fv(GLint location, GLsizei count, const GLfloat * value);
extern void glUseProgram(GLuint program);
extern void glShaderSource(GLuint shader, GLsizei count, const GLchar *const* string, const GLint * length);
extern void glLinkProgram(GLuint program);
extern GLint glGetUniformLocation(GLuint program, const GLchar * name);
extern void glGetShaderiv(GLuint shader, GLenum pname, GLint * params);
extern void glGetProgramInfoLog(GLuint program, GLsizei bufSize, GLsizei * length, GLchar * infoLog);
extern GLint glGetAttribLocation(GLuint program, const GLchar * name);
extern void glDisableVertexAttribArray(GLuint index);
extern void glDeleteShader(GLuint shader);
extern void glDeleteProgram(GLuint program);
extern void glCompileShader(GLuint shader);
extern void glStencilFuncSeparate(GLenum face, GLenum func, GLint ref, GLuint mask);
extern void glStencilOpSeparate(GLenum face, GLenum sfail, GLenum dpfail, GLenum dppass);
extern void glRenderbufferStorageMultisample(GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height);
extern void glDrawBuffers(GLsizei n, const GLenum * bufs);
extern void glVertexAttribDivisor(GLuint index, GLuint divisor);
extern void glBufferSubData(GLenum target, GLintptr offset, GLsizeiptr size, const void * data);
extern void glGenBuffers(GLsizei n, GLuint *buffers);
extern GLenum glCheckFramebufferStatus(GLenum target);
extern void glFramebufferRenderbuffer(GLenum target, GLenum attachment, GLenum renderbuffertarget, GLuint renderbuffer);
extern void glCompressedTexImage2D(GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLint border, GLsizei imageSize, const void * data);
extern void glCompressedTexImage3D(GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLsizei imageSize, const void * data);
extern void glActiveTexture(GLenum texture);
extern void glTexSubImage3D(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum type, const void * pixels);
extern void glUniformMatrix4fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat * value);
extern void glRenderbufferStorage(GLenum target, GLenum internalformat, GLsizei width, GLsizei height);
extern void glGenTextures(GLsizei n, GLuint * textures);
extern void glPolygonOffset(GLfloat factor, GLfloat units);
extern void glDrawElements(GLenum mode, GLsizei count, GLenum type, const void * indices);
extern void glDeleteFramebuffers(GLsizei n, const GLuint * framebuffers);
extern void glBlendEquationSeparate(GLenum modeRGB, GLenum modeAlpha);
extern void glDeleteTextures(GLsizei n, const GLuint * textures);
extern void glGetProgramiv(GLuint program, GLenum pname, GLint * params);
extern void glBindTexture(GLenum target, GLuint texture);
extern void glTexImage3D(GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLenum format, GLenum type, const void * pixels);
extern GLuint glCreateShader(GLenum type);
extern void glTexSubImage2D(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLenum type, const void * pixels);
extern void glClearDepth(GLdouble depth);
extern void glFramebufferTexture2D(GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level);
extern GLuint glCreateProgram(void);
extern void glViewport(GLint x, GLint y, GLsizei width, GLsizei height);
extern void glDeleteBuffers(GLsizei n, const GLuint * buffers);
extern void glDrawArrays(GLenum mode, GLint first, GLsizei count);
extern void glDrawElementsInstanced(GLenum mode, GLsizei count, GLenum type, const void * indices, GLsizei instancecount);
extern void glVertexAttribPointer(GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const void * pointer);
extern void glUniform1i(GLint location, GLint v0);
extern void glUniform2i(GLint location, GLint v0, GLint v1);
extern void glDisable(GLenum cap);
extern void glColorMask(GLboolean red, GLboolean green, GLboolean blue, GLboolean alpha);
extern void glColorMaski(GLuint buf, GLboolean red, GLboolean green, GLboolean blue, GLboolean alpha);
extern void glBindBuffer(GLenum target, GLuint buffer);
extern void glDeleteVertexArrays(GLsizei n, const GLuint * arrays);
extern void glDepthMask(GLboolean flag);
extern void glDrawArraysInstanced(GLenum mode, GLint first, GLsizei count, GLsizei instancecount);
extern void glClearStencil(GLint s);
extern void glScissor(GLint x, GLint y, GLsizei width, GLsizei height);
extern void glUniform3fv(GLint location, GLsizei count, const GLfloat * value);

extern void glGenRenderbuffers(GLsizei n, GLuint * renderbuffers);
extern void glBufferData(GLenum target, GLsizeiptr size, const void * data, GLenum usage);
extern void glBlendFuncSeparate(GLenum sfactorRGB, GLenum dfactorRGB, GLenum sfactorAlpha, GLenum dfactorAlpha);
extern void glTexParameteri(GLenum target, GLenum pname, GLint param);
extern void glGetIntegerv(GLenum pname, GLint * data);
extern void glEnable(GLenum cap);
extern void glBlitFramebuffer(GLint srcX0, GLint srcY0, GLint srcX1, GLint srcY1, GLint dstX0, GLint dstY0, GLint dstX1, GLint dstY1, GLbitfield mask, GLenum filter);
extern void glStencilMask(GLuint mask);
extern void glAttachShader(GLuint program, GLuint shader);
extern void glDetachShader(GLuint program, GLuint shader);
extern GLenum glGetError(void);
extern void glClearColor(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha);
extern void glBlendColor(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha);
extern void glTexParameterf(GLenum target, GLenum pname, GLfloat param);
extern void glTexParameterfv(GLenum target, GLenum pname, GLfloat* params);
extern void glGetShaderInfoLog(GLuint shader, GLsizei bufSize, GLsizei * length, GLchar * infoLog);
extern void glDepthFunc(GLenum func);
extern void glStencilOp(GLenum fail, GLenum zfail, GLenum zpass);
extern void glStencilFunc(GLenum func, GLint ref, GLuint mask);
extern void glEnableVertexAttribArray(GLuint index);
extern void glBlendFunc(GLenum sfactor, GLenum dfactor);
extern void glUniform1fv(GLint location, GLsizei count, const GLfloat * value);
extern void glReadBuffer(GLenum src);
extern void glClear(GLbitfield mask);
extern void glTexImage2D(GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLint border, GLenum format, GLenum type, const void * pixels);
extern void glGenVertexArrays(GLsizei n, GLuint * arrays);
extern void glFrontFace(GLenum mode);
extern void glCullFace(GLenum mode);
extern void glFinish(void);
extern void glPixelStorei(GLenum pname, GLint param);

#endif // !defined (PLATFORM_WINDOWS)




/////////////////////////////////////////////////////////////////////////////////
//
//
//
//    IMPLEMENTATION STARTS HERE
//
// | | | | | | | | | | | | | | | | | |
// V V V V V V V V V V V V V V V V V V
/////////////////////////////////////////////////////////////////////////////////






/////////////////////////////////////////////////////////////////////////////////
//
//
//
//                     WINDOWS IMPLEMENTATION
//
//
//
////////////////////////////////////////////////////////////////////////////////
#if defined(PLATFORM_WINDOWS)

// https://www.opengl.org/archives/resources/code/samples/win32_tutorial/
// https://github.com/floooh/sokol/blob/master/sokol_app.h

#include <windowsx.h> // GET_X_LPARAM ...


typedef HGLRC (WINAPI * PFN_wglCreateContext)(HDC);
typedef BOOL (WINAPI * PFN_wglDeleteContext)(HGLRC);
typedef PROC (WINAPI * PFN_wglGetProcAddress)(LPCSTR);
typedef HDC (WINAPI * PFN_wglGetCurrentDC)(void);
typedef BOOL (WINAPI * PFN_wglMakeCurrent)(HDC,HGLRC);
typedef BOOL (WINAPI * PFNWGLSWAPINTERVALEXTPROC)(int);
#define WGL_NUMBER_PIXEL_FORMATS_ARB 0x2000
#define WGL_SUPPORT_OPENGL_ARB 0x2010
#define WGL_DRAW_TO_WINDOW_ARB 0x2001
#define WGL_PIXEL_TYPE_ARB 0x2013
#define WGL_TYPE_RGBA_ARB 0x202b
#define WGL_ACCELERATION_ARB 0x2003
#define WGL_NO_ACCELERATION_ARB 0x2025
#define WGL_RED_BITS_ARB 0x2015
#define WGL_GREEN_BITS_ARB 0x2017
#define WGL_BLUE_BITS_ARB 0x2019
#define WGL_ALPHA_BITS_ARB 0x201b
#define WGL_DEPTH_BITS_ARB 0x2022
#define WGL_STENCIL_BITS_ARB 0x2023
#define WGL_DOUBLE_BUFFER_ARB 0x2011
#define WGL_SAMPLES_ARB 0x2042
#define WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB 0x00000002
#define WGL_CONTEXT_PROFILE_MASK_ARB 0x9126
#define WGL_CONTEXT_CORE_PROFILE_BIT_ARB 0x00000001
#define WGL_CONTEXT_MAJOR_VERSION_ARB 0x2091
#define WGL_CONTEXT_MINOR_VERSION_ARB 0x2092
#define WGL_CONTEXT_FLAGS_ARB 0x2094
#define ERROR_INVALID_VERSION_ARB 0x2095
#define ERROR_INVALID_PROFILE_ARB 0x2096
#define ERROR_INCOMPATIBLE_DEVICE_CONTEXTS_ARB 0x2054


#define GL_FUNCS \
    GL_XMACRO(glBindVertexArray,                 void, (GLuint array)) \
    GL_XMACRO(glFramebufferTextureLayer,         void, (GLenum target, GLenum attachment, GLuint texture, GLint level, GLint layer)) \
    GL_XMACRO(glGenFramebuffers,                 void, (GLsizei n, GLuint * framebuffers)) \
    GL_XMACRO(glBindFramebuffer,                 void, (GLenum target, GLuint framebuffer)) \
    GL_XMACRO(glBindRenderbuffer,                void, (GLenum target, GLuint renderbuffer)) \
    GL_XMACRO(glGetStringi,                      const GLubyte *, (GLenum name, GLuint index)) \
    GL_XMACRO(glClearBufferfi,                   void, (GLenum buffer, GLint drawbuffer, GLfloat depth, GLint stencil)) \
    GL_XMACRO(glClearBufferfv,                   void, (GLenum buffer, GLint drawbuffer, const GLfloat * value)) \
    GL_XMACRO(glClearBufferuiv,                  void, (GLenum buffer, GLint drawbuffer, const GLuint * value)) \
    GL_XMACRO(glClearBufferiv,                   void, (GLenum buffer, GLint drawbuffer, const GLint * value)) \
    GL_XMACRO(glDeleteRenderbuffers,             void, (GLsizei n, const GLuint * renderbuffers)) \
    GL_XMACRO(glUniform1f,                       void, (GLint location, GLfloat v0)) \
    GL_XMACRO(glUniform2f,                       void, (GLint location, GLfloat v0, GLfloat v1)) \
    GL_XMACRO(glUniform3f,                       void, (GLint location, GLfloat v0, GLfloat v1, GLfloat v2)) \
    GL_XMACRO(glUniform4f,                       void, (GLint location, const GLfloat v0, const GLfloat v1, const GLfloat v2, const GLfloat v3)) \
    GL_XMACRO(glUniform4fv,                      void, (GLint location, GLsizei count, const GLfloat * value)) \
    GL_XMACRO(glUniform2fv,                      void, (GLint location, GLsizei count, const GLfloat * value)) \
    GL_XMACRO(glUseProgram,                      void, (GLuint program)) \
    GL_XMACRO(glShaderSource,                    void, (GLuint shader, GLsizei count, const GLchar *const* string, const GLint * length)) \
    GL_XMACRO(glLinkProgram,                     void, (GLuint program)) \
    GL_XMACRO(glGetUniformLocation,              GLint, (GLuint program, const GLchar * name)) \
    GL_XMACRO(glGetShaderiv,                     void, (GLuint shader, GLenum pname, GLint * params)) \
    GL_XMACRO(glGetProgramInfoLog,               void, (GLuint program, GLsizei bufSize, GLsizei * length, GLchar * infoLog)) \
    GL_XMACRO(glGetAttribLocation,               GLint, (GLuint program, const GLchar * name)) \
    GL_XMACRO(glDisableVertexAttribArray,        void, (GLuint index)) \
    GL_XMACRO(glDeleteShader,                    void, (GLuint shader)) \
    GL_XMACRO(glDeleteProgram,                   void, (GLuint program)) \
    GL_XMACRO(glCompileShader,                   void, (GLuint shader)) \
    GL_XMACRO(glStencilFuncSeparate,             void, (GLenum face, GLenum func, GLint ref, GLuint mask)) \
    GL_XMACRO(glStencilOpSeparate,               void, (GLenum face, GLenum sfail, GLenum dpfail, GLenum dppass)) \
    GL_XMACRO(glRenderbufferStorageMultisample,  void, (GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height)) \
    GL_XMACRO(glDrawBuffers,                     void, (GLsizei n, const GLenum * bufs)) \
    GL_XMACRO(glVertexAttribDivisor,             void, (GLuint index, GLuint divisor)) \
    GL_XMACRO(glBufferSubData,                   void, (GLenum target, GLintptr offset, GLsizeiptr size, const void * data)) \
    GL_XMACRO(glGenBuffers,                      void, (GLsizei n, GLuint * buffers)) \
    GL_XMACRO(glCheckFramebufferStatus,          GLenum, (GLenum target)) \
    GL_XMACRO(glFramebufferRenderbuffer,         void, (GLenum target, GLenum attachment, GLenum renderbuffertarget, GLuint renderbuffer)) \
    GL_XMACRO(glCompressedTexImage2D,            void, (GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLint border, GLsizei imageSize, const void * data)) \
    GL_XMACRO(glCompressedTexImage3D,            void, (GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLsizei imageSize, const void * data)) \
    GL_XMACRO(glActiveTexture,                   void, (GLenum texture)) \
    GL_XMACRO(glTexSubImage3D,                   void, (GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum type, const void * pixels)) \
    GL_XMACRO(glUniformMatrix4fv,                void, (GLint location, GLsizei count, GLboolean transpose, const GLfloat * value)) \
    GL_XMACRO(glRenderbufferStorage,             void, (GLenum target, GLenum internalformat, GLsizei width, GLsizei height)) \
    GL_XMACRO(glGenTextures,                     void, (GLsizei n, GLuint * textures)) \
    GL_XMACRO(glPolygonOffset,                   void, (GLfloat factor, GLfloat units)) \
    GL_XMACRO(glDrawElements,                    void, (GLenum mode, GLsizei count, GLenum type, const void * indices)) \
    GL_XMACRO(glDeleteFramebuffers,              void, (GLsizei n, const GLuint * framebuffers)) \
    GL_XMACRO(glBlendEquationSeparate,           void, (GLenum modeRGB, GLenum modeAlpha)) \
    GL_XMACRO(glDeleteTextures,                  void, (GLsizei n, const GLuint * textures)) \
    GL_XMACRO(glGetProgramiv,                    void, (GLuint program, GLenum pname, GLint * params)) \
    GL_XMACRO(glBindTexture,                     void, (GLenum target, GLuint texture)) \
    GL_XMACRO(glTexImage3D,                      void, (GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLenum format, GLenum type, const void * pixels)) \
    GL_XMACRO(glCreateShader,                    GLuint, (GLenum type)) \
    GL_XMACRO(glTexSubImage2D,                   void, (GLenum target, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLenum type, const void * pixels)) \
    GL_XMACRO(glClearDepth,                      void, (GLdouble depth)) \
    GL_XMACRO(glFramebufferTexture2D,            void, (GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level)) \
    GL_XMACRO(glCreateProgram,                   GLuint, (void)) \
    GL_XMACRO(glViewport,                        void, (GLint x, GLint y, GLsizei width, GLsizei height)) \
    GL_XMACRO(glDeleteBuffers,                   void, (GLsizei n, const GLuint * buffers)) \
    GL_XMACRO(glDrawArrays,                      void, (GLenum mode, GLint first, GLsizei count)) \
    GL_XMACRO(glDrawElementsInstanced,           void, (GLenum mode, GLsizei count, GLenum type, const void * indices, GLsizei instancecount)) \
    GL_XMACRO(glVertexAttribPointer,             void, (GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const void * pointer)) \
    GL_XMACRO(glUniform1i,                       void, (GLint location, GLint v0)) \
    GL_XMACRO(glUniform2i,                       void, (GLint location, GLint v0, GLint v1)) \
    GL_XMACRO(glDisable,                         void, (GLenum cap)) \
    GL_XMACRO(glColorMask,                       void, (GLboolean red, GLboolean green, GLboolean blue, GLboolean alpha)) \
    GL_XMACRO(glColorMaski,                      void, (GLuint buf, GLboolean red, GLboolean green, GLboolean blue, GLboolean alpha)) \
    GL_XMACRO(glBindBuffer,                      void, (GLenum target, GLuint buffer)) \
    GL_XMACRO(glDeleteVertexArrays,              void, (GLsizei n, const GLuint * arrays)) \
    GL_XMACRO(glDepthMask,                       void, (GLboolean flag)) \
    GL_XMACRO(glDrawArraysInstanced,             void, (GLenum mode, GLint first, GLsizei count, GLsizei instancecount)) \
    GL_XMACRO(glClearStencil,                    void, (GLint s)) \
    GL_XMACRO(glScissor,                         void, (GLint x, GLint y, GLsizei width, GLsizei height)) \
    GL_XMACRO(glUniform3fv,                      void, (GLint location, GLsizei count, const GLfloat * value)) \
    GL_XMACRO(glGenRenderbuffers,                void, (GLsizei n, GLuint * renderbuffers)) \
    GL_XMACRO(glBufferData,                      void, (GLenum target, GLsizeiptr size, const void * data, GLenum usage)) \
    GL_XMACRO(glBlendFuncSeparate,               void, (GLenum sfactorRGB, GLenum dfactorRGB, GLenum sfactorAlpha, GLenum dfactorAlpha)) \
    GL_XMACRO(glTexParameteri,                   void, (GLenum target, GLenum pname, GLint param)) \
    GL_XMACRO(glGetIntegerv,                     void, (GLenum pname, GLint * data)) \
    GL_XMACRO(glEnable,                          void, (GLenum cap)) \
    GL_XMACRO(glBlitFramebuffer,                 void, (GLint srcX0, GLint srcY0, GLint srcX1, GLint srcY1, GLint dstX0, GLint dstY0, GLint dstX1, GLint dstY1, GLbitfield mask, GLenum filter)) \
    GL_XMACRO(glStencilMask,                     void, (GLuint mask)) \
    GL_XMACRO(glAttachShader,                    void, (GLuint program, GLuint shader)) \
    GL_XMACRO(glDetachShader,                    void, (GLuint program, GLuint shader)) \
    GL_XMACRO(glGetError,                        GLenum, (void)) \
    GL_XMACRO(glClearColor,                      void, (GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha)) \
    GL_XMACRO(glBlendColor,                      void, (GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha)) \
    GL_XMACRO(glTexParameterf,                   void, (GLenum target, GLenum pname, GLfloat param)) \
    GL_XMACRO(glTexParameterfv,                  void, (GLenum target, GLenum pname, GLfloat* params)) \
    GL_XMACRO(glGetShaderInfoLog,                void, (GLuint shader, GLsizei bufSize, GLsizei * length, GLchar * infoLog)) \
    GL_XMACRO(glDepthFunc,                       void, (GLenum func)) \
    GL_XMACRO(glStencilOp ,                      void, (GLenum fail, GLenum zfail, GLenum zpass)) \
    GL_XMACRO(glStencilFunc,                     void, (GLenum func, GLint ref, GLuint mask)) \
    GL_XMACRO(glEnableVertexAttribArray,         void, (GLuint index)) \
    GL_XMACRO(glBlendFunc,                       void, (GLenum sfactor, GLenum dfactor)) \
    GL_XMACRO(glUniform1fv,                      void, (GLint location, GLsizei count, const GLfloat * value)) \
    GL_XMACRO(glReadBuffer,                      void, (GLenum src)) \
    GL_XMACRO(glClear,                           void, (GLbitfield mask)) \
    GL_XMACRO(glTexImage2D,                      void, (GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLint border, GLenum format, GLenum type, const void * pixels)) \
    GL_XMACRO(glGenVertexArrays,                 void, (GLsizei n, GLuint * arrays)) \
    GL_XMACRO(glFrontFace,                       void, (GLenum mode)) \
    GL_XMACRO(glCullFace,                        void, (GLenum mode)) \
    GL_XMACRO(glFinish,                          void, (void))

// generate GL function pointer typedefs
#define GL_XMACRO(name, ret, args) typedef ret (GL_APIENTRY* PFN_ ## name) args;
GL_FUNCS
#undef GL_XMACRO

// generate GL function pointers
#define GL_XMACRO(name, ret, args) static PFN_ ## name name;
GL_FUNCS
#undef GL_XMACRO

static struct Win32Data {
	HWND hwnd;
	HDC  hdc;
	HWND msg_hwnd;
	HDC  msg_hdc;
	wchar_t window_title_wide[128];

	HGLRC gl_ctx;
	HINSTANCE opengl32;
	PFN_wglCreateContext wglCreateContext;
	PFN_wglDeleteContext wglDeleteContext;
	PFN_wglGetProcAddress wglGetProcAddress;
	PFN_wglGetCurrentDC wglGetCurrentDC;
	PFN_wglMakeCurrent wglMakeCurrent;
    PFNWGLSWAPINTERVALEXTPROC wglSwapIntervalEXT;
} win32_data = {0};


#define MAX_KEYMAPPING 512

PlatformKeyCode key_mapping[MAX_KEYMAPPING] = {
    /* same as GLFW */
    [0x00B] = PLATFORM_KEYCODE_0,
    [0x002] = PLATFORM_KEYCODE_1,
    [0x003] = PLATFORM_KEYCODE_2,
    [0x004] = PLATFORM_KEYCODE_3,
    [0x005] = PLATFORM_KEYCODE_4,
    [0x006] = PLATFORM_KEYCODE_5,
    [0x007] = PLATFORM_KEYCODE_6,
    [0x008] = PLATFORM_KEYCODE_7,
    [0x009] = PLATFORM_KEYCODE_8,
    [0x00A] = PLATFORM_KEYCODE_9,
    [0x01E] = PLATFORM_KEYCODE_A,
    [0x030] = PLATFORM_KEYCODE_B,
    [0x02E] = PLATFORM_KEYCODE_C,
    [0x020] = PLATFORM_KEYCODE_D,
    [0x012] = PLATFORM_KEYCODE_E,
    [0x021] = PLATFORM_KEYCODE_F,
    [0x022] = PLATFORM_KEYCODE_G,
    [0x023] = PLATFORM_KEYCODE_H,
    [0x017] = PLATFORM_KEYCODE_I,
    [0x024] = PLATFORM_KEYCODE_J,
    [0x025] = PLATFORM_KEYCODE_K,
    [0x026] = PLATFORM_KEYCODE_L,
    [0x032] = PLATFORM_KEYCODE_M,
    [0x031] = PLATFORM_KEYCODE_N,
    [0x018] = PLATFORM_KEYCODE_O,
    [0x019] = PLATFORM_KEYCODE_P,
    [0x010] = PLATFORM_KEYCODE_Q,
    [0x013] = PLATFORM_KEYCODE_R,
    [0x01F] = PLATFORM_KEYCODE_S,
    [0x014] = PLATFORM_KEYCODE_T,
    [0x016] = PLATFORM_KEYCODE_U,
    [0x02F] = PLATFORM_KEYCODE_V,
    [0x011] = PLATFORM_KEYCODE_W,
    [0x02D] = PLATFORM_KEYCODE_X,
    [0x015] = PLATFORM_KEYCODE_Y,
    [0x02C] = PLATFORM_KEYCODE_Z,
    [0x028] = PLATFORM_KEYCODE_APOSTROPHE,
    [0x02B] = PLATFORM_KEYCODE_BACKSLASH,
    [0x033] = PLATFORM_KEYCODE_COMMA,
    [0x00D] = PLATFORM_KEYCODE_EQUAL,
    [0x029] = PLATFORM_KEYCODE_GRAVE_ACCENT,
    [0x01A] = PLATFORM_KEYCODE_LEFT_BRACKET,
    [0x00C] = PLATFORM_KEYCODE_MINUS,
    [0x034] = PLATFORM_KEYCODE_PERIOD,
    [0x01B] = PLATFORM_KEYCODE_RIGHT_BRACKET,
    [0x027] = PLATFORM_KEYCODE_SEMICOLON,
    [0x035] = PLATFORM_KEYCODE_SLASH,
    [0x056] = PLATFORM_KEYCODE_WORLD_2,
    [0x00E] = PLATFORM_KEYCODE_BACKSPACE,
    [0x153] = PLATFORM_KEYCODE_DELETE,
    [0x14F] = PLATFORM_KEYCODE_END,
    [0x01C] = PLATFORM_KEYCODE_ENTER,
    [0x001] = PLATFORM_KEYCODE_ESCAPE,
    [0x147] = PLATFORM_KEYCODE_HOME,
    [0x152] = PLATFORM_KEYCODE_INSERT,
    [0x15D] = PLATFORM_KEYCODE_MENU,
    [0x151] = PLATFORM_KEYCODE_PAGE_DOWN,
    [0x149] = PLATFORM_KEYCODE_PAGE_UP,
    [0x045] = PLATFORM_KEYCODE_PAUSE,
    [0x146] = PLATFORM_KEYCODE_PAUSE,
    [0x039] = PLATFORM_KEYCODE_SPACE,
    [0x00F] = PLATFORM_KEYCODE_TAB,
    [0x03A] = PLATFORM_KEYCODE_CAPS_LOCK,
    [0x145] = PLATFORM_KEYCODE_NUM_LOCK,
    [0x046] = PLATFORM_KEYCODE_SCROLL_LOCK,
    [0x03B] = PLATFORM_KEYCODE_F1,
    [0x03C] = PLATFORM_KEYCODE_F2,
    [0x03D] = PLATFORM_KEYCODE_F3,
    [0x03E] = PLATFORM_KEYCODE_F4,
    [0x03F] = PLATFORM_KEYCODE_F5,
    [0x040] = PLATFORM_KEYCODE_F6,
    [0x041] = PLATFORM_KEYCODE_F7,
    [0x042] = PLATFORM_KEYCODE_F8,
    [0x043] = PLATFORM_KEYCODE_F9,
    [0x044] = PLATFORM_KEYCODE_F10,
    [0x057] = PLATFORM_KEYCODE_F11,
    [0x058] = PLATFORM_KEYCODE_F12,
    [0x064] = PLATFORM_KEYCODE_F13,
    [0x065] = PLATFORM_KEYCODE_F14,
    [0x066] = PLATFORM_KEYCODE_F15,
    [0x067] = PLATFORM_KEYCODE_F16,
    [0x068] = PLATFORM_KEYCODE_F17,
    [0x069] = PLATFORM_KEYCODE_F18,
    [0x06A] = PLATFORM_KEYCODE_F19,
    [0x06B] = PLATFORM_KEYCODE_F20,
    [0x06C] = PLATFORM_KEYCODE_F21,
    [0x06D] = PLATFORM_KEYCODE_F22,
    [0x06E] = PLATFORM_KEYCODE_F23,
    [0x076] = PLATFORM_KEYCODE_F24,
    [0x038] = PLATFORM_KEYCODE_LEFT_ALT,
    [0x01D] = PLATFORM_KEYCODE_LEFT_CONTROL,
    [0x02A] = PLATFORM_KEYCODE_LEFT_SHIFT,
    [0x15B] = PLATFORM_KEYCODE_LEFT_SUPER,
    [0x137] = PLATFORM_KEYCODE_PRINT_SCREEN,
    [0x138] = PLATFORM_KEYCODE_RIGHT_ALT,
    [0x11D] = PLATFORM_KEYCODE_RIGHT_CONTROL,
    [0x036] = PLATFORM_KEYCODE_RIGHT_SHIFT,
    [0x15C] = PLATFORM_KEYCODE_RIGHT_SUPER,
    [0x150] = PLATFORM_KEYCODE_DOWN,
    [0x14B] = PLATFORM_KEYCODE_LEFT,
    [0x14D] = PLATFORM_KEYCODE_RIGHT,
    [0x148] = PLATFORM_KEYCODE_UP,
    [0x052] = PLATFORM_KEYCODE_KP_0,
    [0x04F] = PLATFORM_KEYCODE_KP_1,
    [0x050] = PLATFORM_KEYCODE_KP_2,
    [0x051] = PLATFORM_KEYCODE_KP_3,
    [0x04B] = PLATFORM_KEYCODE_KP_4,
    [0x04C] = PLATFORM_KEYCODE_KP_5,
    [0x04D] = PLATFORM_KEYCODE_KP_6,
    [0x047] = PLATFORM_KEYCODE_KP_7,
    [0x048] = PLATFORM_KEYCODE_KP_8,
    [0x049] = PLATFORM_KEYCODE_KP_9,
    [0x04E] = PLATFORM_KEYCODE_KP_ADD,
    [0x053] = PLATFORM_KEYCODE_KP_DECIMAL,
    [0x135] = PLATFORM_KEYCODE_KP_DIVIDE,
    [0x11C] = PLATFORM_KEYCODE_KP_ENTER,
    [0x037] = PLATFORM_KEYCODE_KP_MULTIPLY,
    [0x04A] = PLATFORM_KEYCODE_KP_SUBTRACT,
};


LONG WINAPI
Window_proc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	static bool mouse_tracked = false;

	switch (uMsg) {
	    case WM_CLOSE:
			app_data.quit_requested = true;
	        break;
	    case WM_SYSCOMMAND:
	        break;
	    case WM_ERASEBKGND:
	        return 1;
	    case WM_SIZE: {
			app_data.w_width  = LOWORD(lParam);
			app_data.w_height = HIWORD(lParam);
			app_data.window_resized = true;
	        break;
		}
	    case WM_SETCURSOR:
	        break;
	    case WM_LBUTTONDOWN: {
			if (app_data.mouse.buttons[PLATFORM_MOUSE_BUTTON_LEFT] == KEY_STATUS_DOWN ||
				app_data.mouse.buttons[PLATFORM_MOUSE_BUTTON_LEFT] == KEY_STATUS_HOLD) {

				app_data.mouse.buttons[PLATFORM_MOUSE_BUTTON_LEFT] = KEY_STATUS_HOLD;
			}
			else {
				app_data.mouse.buttons[PLATFORM_MOUSE_BUTTON_LEFT] = KEY_STATUS_DOWN;
			}
	        break;
		}
	    case WM_RBUTTONDOWN: {
			if (app_data.mouse.buttons[PLATFORM_MOUSE_BUTTON_RIGHT] == KEY_STATUS_DOWN ||
				app_data.mouse.buttons[PLATFORM_MOUSE_BUTTON_RIGHT] == KEY_STATUS_HOLD) {

				app_data.mouse.buttons[PLATFORM_MOUSE_BUTTON_RIGHT] = KEY_STATUS_HOLD;
			}
			else {
				app_data.mouse.buttons[PLATFORM_MOUSE_BUTTON_RIGHT] = KEY_STATUS_DOWN;
			}
	        break;
		}
	    case WM_MBUTTONDOWN: {
			if (app_data.mouse.buttons[PLATFORM_MOUSE_BUTTON_MIDDLE] == KEY_STATUS_DOWN ||
				app_data.mouse.buttons[PLATFORM_MOUSE_BUTTON_MIDDLE] == KEY_STATUS_HOLD) {

				app_data.mouse.buttons[PLATFORM_MOUSE_BUTTON_MIDDLE] = KEY_STATUS_HOLD;
			}
			else {
				app_data.mouse.buttons[PLATFORM_MOUSE_BUTTON_MIDDLE] = KEY_STATUS_DOWN;
			}
	        break;
		}
	    case WM_LBUTTONUP: {
			app_data.mouse.buttons[PLATFORM_MOUSE_BUTTON_LEFT] = KEY_STATUS_UP;
	        break;
		}
	    case WM_RBUTTONUP: {
			app_data.mouse.buttons[PLATFORM_MOUSE_BUTTON_RIGHT] = KEY_STATUS_UP;
	        break;
		}
	    case WM_MBUTTONUP: {
			app_data.mouse.buttons[PLATFORM_MOUSE_BUTTON_MIDDLE] = KEY_STATUS_UP;
	        break;
		}
		case  WM_MOUSEMOVE: {
			app_data.mouse.x = (float)GET_X_LPARAM(lParam);
			app_data.mouse.y = (float)GET_Y_LPARAM(lParam);
			if (!mouse_tracked) {
				TRACKMOUSEEVENT tme;
				memset(&tme, 0, sizeof(tme));
				tme.cbSize = sizeof(tme);
				tme.dwFlags = TME_LEAVE;
				tme.hwndTrack = win32_data.hwnd;
				TrackMouseEvent(&tme);
			}
			break;
		}
	    case WM_INPUT:
	        break;
	    case WM_MOUSELEAVE: {
			mouse_tracked = false;
			// Puts all the keys down to up because will never notify the up event
			Put_keys_up();
	        break;
		}
	    case WM_MOUSEWHEEL:
	        break;
	    case WM_MOUSEHWHEEL:
	        break;
	    case WM_CHAR:
	        break;
        case WM_KEYDOWN:
        case WM_SYSKEYDOWN: {
			int key = (int)(HIWORD(lParam)&0x1FF);
			if (key >= 0 && key < MAX_KEYMAPPING) {
				u32 key_id = key_mapping[key];
				if (app_data.keyboard[key_id] == KEY_STATUS_DOWN ||
					app_data.keyboard[key_id] == KEY_STATUS_HOLD) {

					app_data.keyboard[key_id] = KEY_STATUS_HOLD;
				}
				else {
					app_data.keyboard[key_id] = KEY_STATUS_DOWN;
				}
			}
            break;
		}
        case WM_KEYUP:
        case WM_SYSKEYUP: {
			int key = (int)(HIWORD(lParam)&0x1FF);
			if (key >= 0 && key < MAX_KEYMAPPING) {
				app_data.keyboard[key_mapping[key]] = KEY_STATUS_UP;
			}
			break;
		}
		case WM_KILLFOCUS: {
			// Puts all the keys down to up because will never notify the up event
			Put_keys_up();
			break;
		}
	    case WM_ENTERSIZEMOVE:
	        break;
	    case WM_EXITSIZEMOVE:
	        break;
	    case WM_TIMER:
	        //    SwapBuffers(hdc);
	        break;
	    case WM_DROPFILES:
	        break;
	    default:
	        break;
	}
	
    return DefWindowProcW(hWnd, uMsg, wParam, lParam);
} 


/*
 * Extracted from sokol_app.h _sapp_win32_uwp_utf8_to_wide
 *
 */
static bool
String_to_wide(const char* src, wchar_t* dst, int dst_num_bytes) {
    Assert(src && dst && (dst_num_bytes > 1), "Invalid params.");
    memset(dst, 0, (size_t)dst_num_bytes);
    const int dst_chars = dst_num_bytes / (int)sizeof(wchar_t);
    const int dst_needed = MultiByteToWideChar(CP_UTF8, 0, src, -1, 0, 0);
    if ((dst_needed > 0) && (dst_needed < dst_chars)) {
        MultiByteToWideChar(CP_UTF8, 0, src, -1, dst, dst_chars);
        return true;
    }
    else {
        /* input string doesn't fit into destination buffer */
        return false;
    }
}


int
Create_window(WindowDesc *window_desc) {

	app_data.default_width  = window_desc->width;
	app_data.default_height = window_desc->height;
	app_data.w_width  = window_desc->width;
	app_data.w_height = window_desc->height;
	
	String_to_wide(
			window_desc->title,
			win32_data.window_title_wide,
			sizeof(win32_data.window_title_wide));

    WNDCLASSW wndclassw;
    memset(&wndclassw, 0, sizeof(wndclassw));
    wndclassw.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
    wndclassw.lpfnWndProc = (WNDPROC) Window_proc;
    wndclassw.hInstance = GetModuleHandleW(NULL);
    wndclassw.hCursor = LoadCursor(NULL, IDC_ARROW);
    wndclassw.hIcon = LoadIcon(NULL, IDI_WINLOGO);
    wndclassw.lpszClassName = L"APP";
    RegisterClassW(&wndclassw);

    DWORD win_style;
    const DWORD win_ex_style = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
    RECT rect = { 0, 0, 0, 0 };
    win_style = WS_CLIPSIBLINGS |
				WS_CLIPCHILDREN |
				WS_CAPTION |
				WS_SYSMENU |
				WS_MINIMIZEBOX |
				WS_MAXIMIZEBOX |
				WS_SIZEBOX;

    rect.right  = window_desc->width;
    rect.bottom = window_desc->height;

    AdjustWindowRectEx(&rect, win_style, FALSE, win_ex_style);
    const int win_width = rect.right - rect.left;
    const int win_height = rect.bottom - rect.top;
    win32_data.hwnd = CreateWindowExW(
        win_ex_style,                 /* dwExStyle */
        L"APP",                       /* lpClassName */
        win32_data.window_title_wide, /* lpWindowName */
        win_style,                    /* dwStyle */
        CW_USEDEFAULT,                /* X */
        CW_USEDEFAULT,                /* Y */
        win_width,                    /* nWidth */
        win_height,                   /* nHeight */
        NULL,                         /* hWndParent */
        NULL,                         /* hMenu */
        GetModuleHandle(NULL),        /* hInstance */
        NULL);                        /* lParam */
    ShowWindow(win32_data.hwnd, SW_SHOW);
    win32_data.hdc = GetDC(win32_data.hwnd);
	if (!win32_data.hdc) {
		return -1;
	}
	return 0;
}

void
Set_fullscreen(bool enable) {
    HMONITOR monitor = MonitorFromWindow(win32_data.hwnd, MONITOR_DEFAULTTONEAREST);
    MONITORINFO minfo;
    memset(&minfo, 0, sizeof(minfo));
    minfo.cbSize = sizeof(MONITORINFO);
    GetMonitorInfo(monitor, &minfo);
    const RECT mr = minfo.rcMonitor;
    const int monitor_w = mr.right - mr.left;
    const int monitor_h = mr.bottom - mr.top;

    const DWORD win_ex_style = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
    DWORD win_style;
    RECT rect = { 0, 0, 0, 0 };

    if (enable) {
        win_style = WS_POPUP | WS_SYSMENU | WS_VISIBLE;
        rect.right = monitor_w;
        rect.bottom = monitor_h;
    }
    else {
        win_style = WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_SIZEBOX;
        rect.right  = app_data.default_width;
        rect.bottom = app_data.default_height;
    }

    AdjustWindowRectEx(&rect, win_style, FALSE, win_ex_style);

    int win_width = rect.right - rect.left;
    int win_height = rect.bottom - rect.top;

    if (!enable) {
        rect.left = (monitor_w - win_width) / 2;
        rect.top = (monitor_h - win_height) / 2;
    }

    SetWindowLongPtr(win32_data.hwnd, GWL_STYLE, win_style);
    SetWindowPos(win32_data.hwnd, HWND_TOP, mr.left + rect.left, mr.top + rect.top, win_width, win_height, SWP_SHOWWINDOW | SWP_FRAMECHANGED);
	
	// Seems that when we call SetWindowPos it calls internally to our Window_proc with the WM_SIZE
	// event, but Set_fullscreen is called from the frame, and Clear_events clears all events after
	// the frame callback, so the application wouldn't notice the resize.
	//
	// To fix this we set fullscreen_changed to true and clear events will handle this case
	// correctly.
	//
	// Tano 15-08-2021
	app_data.fullscreen_changed = true;
}

void
Show_mouse(bool show) {
    ShowCursor((BOOL)show);
}

void
Set_window_title(const char *title) {
	String_to_wide(
			title,
			win32_data.window_title_wide,
			sizeof(win32_data.window_title_wide)
	);
    SetWindowTextW(win32_data.hwnd, win32_data.window_title_wide);
}


static void *
Load_gl_function(const char *func_name) {
    void* proc_addr = (void*)win32_data.wglGetProcAddress(func_name);
	if (proc_addr == NULL) {
		proc_addr = (void*) GetProcAddress(win32_data.opengl32, func_name);
	}
    if (proc_addr == NULL) {
		printf("CANNOT LOAD %s\n", func_name);
	}
    return proc_addr;
}

int
Setup_GL() {
	win32_data.opengl32 = LoadLibraryA("opengl32.dll");
	if (!win32_data.opengl32) {
	    fprintf(stderr, "Failed to load opengl32.dll\n");
		return -1;
	}

	win32_data.wglCreateContext = (PFN_wglCreateContext)(void*)GetProcAddress(win32_data.opengl32, "wglCreateContext");
	if (!win32_data.wglCreateContext) {
	    fprintf(stderr, "Failed to get proc address wglCreateContext\n");
		return -1;
	}

	win32_data.wglDeleteContext  = (PFN_wglDeleteContext)(void*)GetProcAddress(win32_data.opengl32, "wglDeleteContext");
	if (!win32_data.wglDeleteContext) {
	    fprintf(stderr, "Failed to get proc address wglDeleteContext\n");
		return -1;
	}

	win32_data.wglGetProcAddress = (PFN_wglGetProcAddress)(void*)GetProcAddress(win32_data.opengl32, "wglGetProcAddress");
	if (!win32_data.wglGetProcAddress) {
	    fprintf(stderr, "Failed to get proc address wglGetProcAddress\n");
		return -1;
	}

	win32_data.wglGetCurrentDC = (PFN_wglGetCurrentDC)(void*)GetProcAddress(win32_data.opengl32, "wglGetCurrentDC");
	if (!win32_data.wglGetCurrentDC) {
	    fprintf(stderr, "Failed to get proc address wglGetCurrentDC\n");
		return -1;
	}

	win32_data.wglMakeCurrent = (PFN_wglMakeCurrent)(void*)GetProcAddress(win32_data.opengl32, "wglMakeCurrent");
	if (!win32_data.wglMakeCurrent) {
	    fprintf(stderr, "Failed to get proc address wglMakeCurrent\n");
		return -1;
	}
    

	PIXELFORMATDESCRIPTOR pfd;
    /* there is no guarantee that the contents of the stack that become
       the pfd are zeroed, therefore _make sure_ to clear these bits. */
    memset(&pfd, 0, sizeof(pfd));
    pfd.nSize        = sizeof(pfd);
    pfd.nVersion     = 1;
    pfd.dwFlags      = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pfd.iPixelType   = PFD_TYPE_RGBA;
    pfd.cColorBits   = 24;

    int pf = ChoosePixelFormat(win32_data.hdc, &pfd);
    if (pf == 0) {
		fprintf(stderr, "ChoosePixelFormat() failed: Cannot find a suitable pixel format."); 
		return -1;
    } 
 
    if (!SetPixelFormat(win32_data.hdc, pf, &pfd)) {
		fprintf(stderr, "SetPixelFormat() failed: Cannot set format specified.");
		return -1;
    } 

    DescribePixelFormat(win32_data.hdc, pf, sizeof(PIXELFORMATDESCRIPTOR), &pfd);

	win32_data.gl_ctx = win32_data.wglCreateContext(win32_data.hdc);
	if (!win32_data.gl_ctx) {
		const DWORD err = GetLastError();
		if (err == (0xc0070000 | ERROR_INVALID_VERSION_ARB)) {
			fprintf(stderr, "WGL: Driver does not support OpenGL version 3.3\n");
			return -1;
		}
		else if (err == (0xc0070000 | ERROR_INVALID_PROFILE_ARB)) {
			fprintf(stderr, "WGL: Driver does not support the requested OpenGL profile");
			return -1;
		}
		else if (err == (0xc0070000 | ERROR_INCOMPATIBLE_DEVICE_CONTEXTS_ARB)) {
			fprintf(stderr, "WGL: The share context is not compatible with the requested context");
			return -1;
		}
		else {
			fprintf(stderr, "WGL: Failed to create OpenGL context");
			return -1;
		}
	}
	win32_data.wglMakeCurrent(win32_data.hdc, win32_data.gl_ctx);
	

	// What the fuck is this (Tano)
	//if (_sapp.wgl.ext_swap_control) {
	//	/* FIXME: DwmIsCompositionEnabled() (see GLFW) */
	//	_sapp.wgl.SwapIntervalEXT(_sapp.swap_interval);
	//}
	
	win32_data.wglSwapIntervalEXT = (PFNWGLSWAPINTERVALEXTPROC)(void*) win32_data.wglGetProcAddress("wglSwapIntervalEXT");
	if (!win32_data.wglSwapIntervalEXT) {
	    fprintf(stderr, "Failed to get proc address wglSwapIntervalEXT\n");
	}

	// BIND ALL GL FUNCTIONS
	#define GL_XMACRO(name, ret, args) name = (PFN_ ## name) Load_gl_function(#name);
		GL_FUNCS
    #undef GL_XMACRO

	return 0;
}

void
Cleanup_GL() {
    win32_data.wglDeleteContext(win32_data.gl_ctx);
    win32_data.gl_ctx = 0;
    FreeLibrary(win32_data.opengl32); win32_data.opengl32 = 0;
}

void
Destroy_window() {
    DestroyWindow(win32_data.hwnd);
}

static void
Process_events() {
	MSG msg;
	while (PeekMessageW(&msg, NULL, 0, 0, PM_REMOVE)) {
		if (WM_QUIT == msg.message) {
			app_data.quit_requested = true;
			continue;
		}
		else {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
}

static void
Swap_buffers() {
	/* get rendered buffer to the screen */
	SwapBuffers(win32_data.hdc);
}

void
Set_swap_interval(u8 interval) {
	if (win32_data.wglSwapIntervalEXT) {
		win32_data.wglSwapIntervalEXT(interval);
	}
}




////////////////////////////////////////////////////////////////////////////////
//
//
//
// SOUND API
//
//
//
////////////////////////////////////////////////////////////////////////////////

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#ifndef NOMINMAX
#define NOMINMAX
#endif
#include <synchapi.h>
#ifndef CINTERFACE
#define CINTERFACE
#endif
#ifndef COBJMACROS
#define COBJMACROS
#endif
#ifndef CONST_VTABLE
#define CONST_VTABLE
#endif
#include <mmdeviceapi.h>
#include <audioclient.h>
static const IID splayer_IID_IAudioClient = { 0x1cb9ad4c, 0xdbfa, 0x4c32, { 0xb1, 0x78, 0xc2, 0xf5, 0x68, 0xa7, 0x03, 0xb2 } };
static const IID splayer_IID_IMMDeviceEnumerator = { 0xa95664d2, 0x9614, 0x4f35, { 0xa7, 0x46, 0xde, 0x8d, 0xb6, 0x36, 0x17, 0xe6 } };
static const CLSID splayer_CLSID_IMMDeviceEnumerator = { 0xbcde0395, 0xe52f, 0x467c, { 0x8e, 0x3d, 0xc4, 0x57, 0x92, 0x91, 0x69, 0x2e } };
static const IID splayer_IID_IAudioRenderClient = { 0xf294acfc, 0x3146, 0x4483,{ 0xa7, 0xbf, 0xad, 0xdc, 0xa7, 0xc2, 0x60, 0xe2 } };
static const IID splayer_IID_Devinterface_Audio_Render = { 0xe6327cad, 0xdcec, 0x4949, {0xae, 0x8a, 0x99, 0x1e, 0x97, 0x6a, 0x79, 0xd2 } };
static const IID splayer_IID_IActivateAudioInterface_Completion_Handler = { 0x94ea2b94, 0xe9cc, 0x49e0, {0xc0, 0xff, 0xee, 0x64, 0xca, 0x8f, 0x5b, 0x90} };
#define AUDIO_WIN32COM_ID(x) (&x)
/* fix for Visual Studio 2015 SDKs */

typedef struct {
    HANDLE thread_handle;
    HANDLE buffer_end_event;
    volatile bool stop;
    UINT32 dst_buffer_frames;
    int src_buffer_frames;
    int src_buffer_byte_size;
    int src_buffer_pos;
    float* src_buffer;
} SoundPlayerThreadData;

static struct SoundPlayerBackend {
	IMMDeviceEnumerator* device_enumerator;
	IMMDevice* device;
	IAudioClient* audio_client;
	IAudioRenderClient* render_client;
	int si16_bytes_per_frame;
	SoundPlayerThreadData thread;
} sound_player_backend = {0};



static void
Cleanup_sound_player(void) {
    if (sound_player_backend.thread.src_buffer) {
        free(sound_player_backend.thread.src_buffer);
        sound_player_backend.thread.src_buffer = NULL;
    }
    if (sound_player_backend.render_client) {
        IAudioRenderClient_Release(sound_player_backend.render_client);
        sound_player_backend.render_client = 0;
    }
    if (sound_player_backend.audio_client) {
        IAudioClient_Release(sound_player_backend.audio_client);
        sound_player_backend.audio_client = 0;
    }
	if (sound_player_backend.device) {
	    IMMDevice_Release(sound_player_backend.device);
	    sound_player_backend.device = 0;
	}
	if (sound_player_backend.device_enumerator) {
	    IMMDeviceEnumerator_Release(sound_player_backend.device_enumerator);
	    sound_player_backend.device_enumerator = 0;
	}
	if (0 != sound_player_backend.thread.buffer_end_event) {
	    CloseHandle(sound_player_backend.thread.buffer_end_event);
	    sound_player_backend.thread.buffer_end_event = 0;
	}
}





static void
Win32_splayer_submit_buffer(int num_frames) {
    BYTE* wasapi_buffer = 0;
    if (FAILED(IAudioRenderClient_GetBuffer(sound_player_backend.render_client, num_frames, &wasapi_buffer))) {
        return;
    }

    /* convert float samples to int16_t, refill float buffer if needed */
    const int num_samples = num_frames * sound_player_info.num_channels;
    int16_t* dst = (int16_t*) wasapi_buffer;
    int buffer_pos = sound_player_backend.thread.src_buffer_pos;
    const int buffer_float_size = sound_player_backend.thread.src_buffer_byte_size / (int)sizeof(float);
    float* src = sound_player_backend.thread.src_buffer;
    for (int i = 0; i < num_samples; i++) {
        if (0 == buffer_pos) {
			sound_player_info.stream_cb(
					sound_player_info.user_data,
					sound_player_backend.thread.src_buffer,
					sound_player_info.buffer_frames,
					sound_player_info.num_channels
			);
        }
		int value = (src[buffer_pos] * 0x7FFF);
		value = value < -32768 ? -32768 : (value > 32767 ? 32767 : value); // Clamp the value to prevent overflow
        dst[i] = (int16_t) (value);
        buffer_pos += 1;
        if (buffer_pos == buffer_float_size) {
            buffer_pos = 0;
        }
    }
    sound_player_backend.thread.src_buffer_pos = buffer_pos;

    IAudioRenderClient_ReleaseBuffer(sound_player_backend.render_client, num_frames, 0);
}

static DWORD WINAPI
Sound_player_thread(LPVOID param) {
    (void)param;
    Win32_splayer_submit_buffer(sound_player_backend.thread.src_buffer_frames);
    IAudioClient_Start(sound_player_backend.audio_client);
    while (!sound_player_backend.thread.stop) {
        WaitForSingleObject(sound_player_backend.thread.buffer_end_event, INFINITE);
        UINT32 padding = 0;
        if (FAILED(IAudioClient_GetCurrentPadding(sound_player_backend.audio_client, &padding))) {
            continue;
        }
        if (sound_player_backend.thread.dst_buffer_frames < padding) {
			fprintf(stderr, "Error, on sound player thread, buffer frames less than padding");
			return 1;
		}
        int num_frames = (int)sound_player_backend.thread.dst_buffer_frames - (int)padding;
        if (num_frames > 0) {
            Win32_splayer_submit_buffer(num_frames);
        }
    }
    return 0;
}


int
Launch_sound_player(SoundPlayerDesc *desc) {
	REFERENCE_TIME dur;
	/* CoInitializeEx could have been called elsewhere already, in which
	    case the function returns with S_FALSE (thus it does not make much
	    sense to check the result)
	*/
	CoInitializeEx(0, COINIT_MULTITHREADED);
	sound_player_backend.thread.buffer_end_event = CreateEvent(0, FALSE, FALSE, 0);
	if (0 == sound_player_backend.thread.buffer_end_event) {
	    fprintf(stderr, "failed to create buffer_end_event.\n");
		Cleanup_sound_player();
		return -1;
	}
	if (FAILED(CoCreateInstance(
					&splayer_CLSID_IMMDeviceEnumerator,
					0,
					CLSCTX_ALL,
					&splayer_IID_IMMDeviceEnumerator,
					(void**)&sound_player_backend.device_enumerator))
	) {
	    fprintf(stderr, "failed to create device enumerator.\n");
		Cleanup_sound_player();
		return -1;
	}

	if (FAILED(IMMDeviceEnumerator_GetDefaultAudioEndpoint(sound_player_backend.device_enumerator,
			eRender, eConsole,
			&sound_player_backend.device))) {
	    fprintf(stderr, "GetDefaultAudioEndPoint failed.\n");
		Cleanup_sound_player();
		return -1;
	}

	if (FAILED(IMMDevice_Activate(sound_player_backend.device, &splayer_IID_IAudioClient, CLSCTX_ALL, 0,
	    (void**)&sound_player_backend.audio_client)))
	{
	    fprintf(stderr, "device activate failed\n");
		Cleanup_sound_player();
		return -1;
	}

	WAVEFORMATEX fmt;
	memset(&fmt, 0, sizeof(fmt));
	fmt.nChannels = (WORD)desc->num_channels;
	fmt.nSamplesPerSec = (DWORD)desc->sample_rate;
	fmt.wFormatTag = WAVE_FORMAT_PCM;
	fmt.wBitsPerSample = 16;
	fmt.nBlockAlign = (fmt.nChannels * fmt.wBitsPerSample) / 8;
	fmt.nAvgBytesPerSec = fmt.nSamplesPerSec * fmt.nBlockAlign;
	dur = (REFERENCE_TIME)
	    (((double)desc->buffer_frames) / (((double)desc->sample_rate) * (1.0/10000000.0)));
	if (FAILED(IAudioClient_Initialize(sound_player_backend.audio_client,
	    AUDCLNT_SHAREMODE_SHARED,
	    AUDCLNT_STREAMFLAGS_EVENTCALLBACK|AUDCLNT_STREAMFLAGS_AUTOCONVERTPCM|AUDCLNT_STREAMFLAGS_SRC_DEFAULT_QUALITY,
	    dur, 0, &fmt, 0)))
	{
	    fprintf(stderr, "audio client initialize failed.\n");
		Cleanup_sound_player();
		return -1;
	}
	if (FAILED(IAudioClient_GetBufferSize(sound_player_backend.audio_client, &sound_player_backend.thread.dst_buffer_frames))) {
	    fprintf(stderr, "audio client get buffer size failed.\n");
		Cleanup_sound_player();
		return -1;
	}
	if (FAILED(IAudioClient_GetService(sound_player_backend.audio_client, &splayer_IID_IAudioRenderClient,
			(void**)&sound_player_backend.render_client))) {
	    fprintf(stderr, "audio client GetService failed.\n");
		Cleanup_sound_player();
		return -1;
	}
	if (FAILED(IAudioClient_SetEventHandle(sound_player_backend.audio_client,
			sound_player_backend.thread.buffer_end_event))) {
	    fprintf(stderr, "audio client SetEventHandle failed.\n");
		Cleanup_sound_player();
		return -1;
	}
	sound_player_backend.si16_bytes_per_frame = desc->num_channels * (int)sizeof(int16_t);
	size_t bytes_per_frame = desc->num_channels * (int)sizeof(float);
	sound_player_backend.thread.src_buffer_frames = desc->buffer_frames;
	sound_player_backend.thread.src_buffer_byte_size = sound_player_backend.thread.src_buffer_frames * bytes_per_frame;
	
	/* allocate an intermediate buffer for sample format conversion */
	sound_player_backend.thread.src_buffer = Alloc(f32, desc->num_channels*desc->buffer_frames);
	
	/* create streaming thread */
	sound_player_backend.thread.thread_handle = CreateThread(NULL, 0, Sound_player_thread, 0, 0, 0);
	if (0 == sound_player_backend.thread.thread_handle) {
	    fprintf(stderr, "audio wasapi: CreateThread failed\n");
		Cleanup_sound_player();
		return -1;
	}

	sound_player_info.sample_rate   = desc->sample_rate;
	sound_player_info.buffer_frames = desc->buffer_frames;
	sound_player_info.user_data     = desc->user_data;
	sound_player_info.stream_cb     = desc->stream_cb;
	sound_player_info.num_channels  = desc->num_channels;

	return 0;
}





void
Terminate_sound_player(void) {
	if (sound_player_backend.thread.thread_handle) {
	    sound_player_backend.thread.stop = true;
	    SetEvent(sound_player_backend.thread.buffer_end_event);
	    WaitForSingleObject(sound_player_backend.thread.thread_handle, INFINITE);
	    CloseHandle(sound_player_backend.thread.thread_handle);
	    sound_player_backend.thread.thread_handle = 0;
	}
	if (sound_player_backend.audio_client) {
	    IAudioClient_Stop(sound_player_backend.audio_client);
	}
	Cleanup_sound_player();
	CoUninitialize();
}











////////////////////////////////////////////////////////////////////////////////
//
//
//
// TIME API
//
//
//
////////////////////////////////////////////////////////////////////////////////









LARGE_INTEGER app_initial_time = {0};
LARGE_INTEGER win32_time_freq  = {0};
 

void
Init_app_time() {
	QueryPerformanceFrequency(&win32_time_freq);
	QueryPerformanceCounter(&app_initial_time);
}


i64
App_time() {
	LARGE_INTEGER qpc_t;
	QueryPerformanceCounter(&qpc_t);

	i64 counter_diff = qpc_t.QuadPart - app_initial_time.QuadPart;
	i64 freq         = win32_time_freq.QuadPart;
	
	// This is to preserve the precission (from sokol time)
    i64 q = counter_diff / freq;
    i64 r = counter_diff % freq;
    return q * 1*T_SECOND + (r * 1*T_SECOND) / freq;
}








////////////////////////////////////////////////////////////////////////////////
//
//
//
// THREAD API
//
//
//
////////////////////////////////////////////////////////////////////////////////


void
Mutex_init(Mutex *mutex) {
	*mutex = CreateMutex( 
			NULL,              // default security attributes.
			FALSE,             // initially not owned
			NULL);             // unnamed mutex

	if (*mutex == NULL) {
		Panic("Failed to init mutex.");
	}
}

void
Mutex_deinit(Mutex *mutex) {
	CloseHandle(*mutex);
}

void
Mutex_lock(Mutex *mutex) {
	DWORD dwWaitResult = dwWaitResult = WaitForSingleObject( 
			*mutex,     // handle to mutex
			INFINITE);  // no time-out interval

	if (WAIT_OBJECT_0 != dwWaitResult) {
		Panic("Failed to lock mutex.");
	}
}

void
Mutex_unlock(Mutex *mutex) {
	if (!ReleaseMutex(*mutex)) {
		Panic("Failed to unlock mutex.");
	}
}


////////////////////////////////////////////////////////////////////////////////
//
//
//
// FILESYSTEM API
//
//
//
////////////////////////////////////////////////////////////////////////////////


// Documented above
#if !defined(BUNDLED_TREE)
u64
Get_file_modify_time(const char *filename) {
	HANDLE file_handle = CreateFileA(filename, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (file_handle == INVALID_HANDLE_VALUE) {
		return 0;
	}
	FILETIME time;
	u64 result = 0;
	if (GetFileTime(file_handle, NULL, NULL, &time)) {
		result = (u64)time.dwLowDateTime;
		result |= (u64)time.dwHighDateTime << 32;
	}
	CloseHandle(file_handle);
	return result;
}
#endif










/////////////////////////////////////////////////////////////////////////////////
//
//
//
//                     LINUX IMPLEMENTATION ||||
//                                          VVVV
//
//
////////////////////////////////////////////////////////////////////////////////

#elif defined(PLATFORM_LINUX)

#include <EGL/egl.h>
#include <EGL/eglplatform.h>
#include <time.h>
#include <X11/Xcursor/Xcursor.h>

static struct X11Data {
	Window root;
	Window  win;
	Display *xdisplay;
	XSetWindowAttributes swa;
    Atom WM_PROTOCOLS;
    Atom WM_DELETE_WINDOW;
	Atom NET_WM_STATE;
    Atom NET_WM_STATE_FULLSCREEN;
	
	Cursor hidden_cursor;

	// EGL-related objects
	EGLDisplay egl_display;
	EGLConfig  egl_conf;
	EGLContext egl_context;
	EGLSurface egl_surface;
} x11_data = {0};



int
Create_window(WindowDesc *window_desc) {

	app_data.default_width  = window_desc->width;
	app_data.default_height = window_desc->height;
	app_data.w_width  = window_desc->width;
	app_data.w_height = window_desc->height;

	/* open standard display (primary screen) */
	x11_data.xdisplay = XOpenDisplay(NULL);   
	if (x11_data.xdisplay == NULL) {
	    printf("Error opening X display\n");
	    return -1;
	}
	// get the root window (usually the whole screen)
	x11_data.root = DefaultRootWindow(x11_data.xdisplay);
	
	// list all events this window accepts
	x11_data.swa.event_mask =
		StructureNotifyMask |
		ExposureMask        |
		PointerMotionMask   |
		KeyPressMask        |
		KeyReleaseMask      |
		ButtonPressMask     |
		FocusChangeMask     |
		ButtonReleaseMask;
	
	// Xlib's window creation
	x11_data.win = XCreateWindow (
			x11_data.xdisplay, x11_data.root, 0, 0, window_desc->width, window_desc->height, 0,
			CopyFromParent, InputOutput, CopyFromParent, CWEventMask,
			&x11_data.swa);
	
	XMapWindow(x11_data.xdisplay, x11_data.win);         // make window visible
	XStoreName(x11_data.xdisplay, x11_data.win, window_desc->title);
    
	// To handle windows close
	x11_data.WM_PROTOCOLS     = XInternAtom(x11_data.xdisplay, "WM_PROTOCOLS", False);
    x11_data.WM_DELETE_WINDOW = XInternAtom(x11_data.xdisplay, "WM_DELETE_WINDOW", False);
	x11_data.NET_WM_STATE     = XInternAtom(x11_data.xdisplay, "_NET_WM_STATE", False);
	x11_data.NET_WM_STATE_FULLSCREEN = XInternAtom(x11_data.xdisplay, "_NET_WM_STATE_FULLSCREEN", False);
    Atom protocols[] = {
        x11_data.WM_DELETE_WINDOW
    };
    XSetWMProtocols(x11_data.xdisplay, x11_data.win, protocols, 1);

	{ // Create a invisible cursor
		const int w = 16;
    	const int h = 16;
    	XcursorImage* img = XcursorImageCreate(w, h);
		if (img == NULL) {
			Destroy_window();
			return -1;
		}
    	img->xhot = 0;
    	img->yhot = 0;
    	const size_t num_bytes = (size_t)(w * h) * sizeof(XcursorPixel);
    	memset(img->pixels, 0, num_bytes);
    	x11_data.hidden_cursor = XcursorImageLoadCursor(x11_data.xdisplay, img);
    	XcursorImageDestroy(img);
	}

	return 0;
}

static void
X11_send_event(Atom type, int a, int b, int c, int d, int e) {
    XEvent event;
    memset(&event, 0, sizeof(event));

    event.type = ClientMessage;
    event.xclient.window = x11_data.win;
    event.xclient.format = 32;
    event.xclient.message_type = type;
    event.xclient.data.l[0] = a;
    event.xclient.data.l[1] = b;
    event.xclient.data.l[2] = c;
    event.xclient.data.l[3] = d;
    event.xclient.data.l[4] = e;

    XSendEvent(x11_data.xdisplay, x11_data.root,
               False,
               SubstructureNotifyMask | SubstructureRedirectMask,
               &event);
}

void
Set_fullscreen(bool enable) {
	/* NOTE: this function must be called after XMapWindow (which happens in _sapp_x11_show_window()) */
    if (x11_data.NET_WM_STATE && x11_data.NET_WM_STATE_FULLSCREEN) {
        if (enable) {
            const int _NET_WM_STATE_ADD = 1;
            X11_send_event(x11_data.NET_WM_STATE,
                                _NET_WM_STATE_ADD,
                                x11_data.NET_WM_STATE_FULLSCREEN,
                                0, 1, 0);
        }
        else {
            const int _NET_WM_STATE_REMOVE = 0;
            X11_send_event(x11_data.NET_WM_STATE,
                                _NET_WM_STATE_REMOVE,
                                x11_data.NET_WM_STATE_FULLSCREEN,
                                0, 1, 0);
        }
    }
    XFlush(x11_data.xdisplay);
}


void
Show_mouse(bool show) {
    if (show) {
        XUndefineCursor(x11_data.xdisplay, x11_data.win);
    }
    else {
        XDefineCursor(x11_data.xdisplay, x11_data.win, x11_data.hidden_cursor);
    }	
}

void
Set_window_title(const char *title) {
	XStoreName(x11_data.xdisplay, x11_data.win, title);
	XFlush(x11_data.xdisplay);
}


void
Destroy_window(void) {
	XUnmapWindow(x11_data.xdisplay, x11_data.win);
	XDestroyWindow(x11_data.xdisplay, x11_data.win);
	XCloseDisplay(x11_data.xdisplay);
}


int
Setup_GL() {
    EGLint attr[] = {
        EGL_SURFACE_TYPE,    EGL_WINDOW_BIT,
        EGL_RED_SIZE,        8,
        EGL_GREEN_SIZE,      8,
        EGL_BLUE_SIZE,       8,
        EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT, /* If one needs GLES2 */
        EGL_NONE
    };

    EGLint num_config;
    EGLint major, minor;

    EGLint ctxattr[] = {
       EGL_CONTEXT_CLIENT_VERSION, 2,
       EGL_NONE
    };

    x11_data.egl_display = eglGetDisplay((EGLNativeDisplayType)x11_data.xdisplay);
    if (x11_data.egl_display == EGL_NO_DISPLAY) {
        fprintf(stderr, "Error getting EGL display\n");
        return -1;
    }

    if (eglInitialize(x11_data.egl_display, &major, &minor) != EGL_TRUE) {
        fprintf(stderr, "Error initializing EGL\n");
        return -1;
    }
    // printf("EGL major: %d, minor %d\n", major, minor);

    /* create EGL rendering context */
    if (!eglChooseConfig(x11_data.egl_display, attr, &x11_data.egl_conf, 1, &num_config )) {
        fprintf(stderr, "Failed to choose config (eglError: %x)\n", eglGetError());
        return -1;
    }

    if (num_config != 1) {
        return -1;
    }

    x11_data.egl_surface = eglCreateWindowSurface(
			x11_data.egl_display,
			x11_data.egl_conf,
			x11_data.win,
			NULL
	);
    if (x11_data.egl_surface == EGL_NO_SURFACE) {
        fprintf(stderr, "CreateWindowSurface, EGL eglError: %d\n", eglGetError());
        return -1;
    }

    x11_data.egl_context = eglCreateContext(
			x11_data.egl_display,
			x11_data.egl_conf,
			EGL_NO_CONTEXT,
			ctxattr
	);
    if (x11_data.egl_context == EGL_NO_CONTEXT) {
        fprintf(stderr, "CreateContext, EGL eglError: %d\n", eglGetError() );
        return -1;
    }
	
	//
	// WARNING LEAK!!!!
	// This generates a leak, no idea why....
	//
	if (eglMakeCurrent(
			x11_data.egl_display,
			x11_data.egl_surface,
			x11_data.egl_surface,
			x11_data.egl_context
	) != EGL_TRUE) {
        fprintf(stderr, "MakeCurrent, EGL eglError: %d\n", eglGetError() );
        return -1;
	}
	return 0;
}


void
Cleanup_GL() {
	// I readed somewhere that is important that all gl commands must finalize before clean
	// everything.
	glFinish();

	// This unbinds the context, must be done before destroy the context.
	if (eglMakeCurrent(x11_data.egl_display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT) != EGL_TRUE) {
   	 	fprintf(stderr, "Error, cannot unbind context and surfaces.\n");
	}
	
	// Destroys the surface and the context
   	if (eglDestroySurface(x11_data.egl_display, x11_data.egl_surface) != EGL_TRUE) {
   	 	fprintf(stderr, "Error, cannot destroy egl surface.\n");
   	}
	if (eglDestroyContext(x11_data.egl_display, x11_data.egl_context) != EGL_TRUE) {
   	 	fprintf(stderr, "Error, cannot destroy egl context.\n");
   	}

	// Destroys the egl_display
   	if (eglTerminate(x11_data.egl_display) != EGL_TRUE) {
   	 	fprintf(stderr, "Error, cannot terminate egl display connection.\n");
   	}
}

void
Set_swap_interval(u8 interval) {
	eglSwapInterval(x11_data.egl_display, interval);
}

static void
Swap_buffers() {
	/* get rendered buffer to the screen */
	eglSwapBuffers (x11_data.egl_display, x11_data.egl_surface);
}

// Based on sokol_app.h
static PlatformKeyCode
Translate_key(int scancode) {
    int dummy;
    KeySym* keysyms = XGetKeyboardMapping(x11_data.xdisplay, scancode, 1, &dummy);
    if (keysyms == NULL) {
		return PLATFORM_KEYCODE_INVALID;
	}

    KeySym keysym = keysyms[0];
    XFree(keysyms);
    switch (keysym) {
        case XK_Escape:         return PLATFORM_KEYCODE_ESCAPE;
        case XK_Tab:            return PLATFORM_KEYCODE_TAB;
        case XK_Shift_L:        return PLATFORM_KEYCODE_LEFT_SHIFT;
        case XK_Shift_R:        return PLATFORM_KEYCODE_RIGHT_SHIFT;
        case XK_Control_L:      return PLATFORM_KEYCODE_LEFT_CONTROL;
        case XK_Control_R:      return PLATFORM_KEYCODE_RIGHT_CONTROL;
        case XK_Meta_L:
        case XK_Alt_L:          return PLATFORM_KEYCODE_LEFT_ALT;
        case XK_Mode_switch:    /* Mapped to Alt_R on many keyboards */
        case XK_ISO_Level3_Shift: /* AltGr on at least some machines */
        case XK_Meta_R:
        case XK_Alt_R:          return PLATFORM_KEYCODE_RIGHT_ALT;
        case XK_Super_L:        return PLATFORM_KEYCODE_LEFT_SUPER;
        case XK_Super_R:        return PLATFORM_KEYCODE_RIGHT_SUPER;
        case XK_Menu:           return PLATFORM_KEYCODE_MENU;
        case XK_Num_Lock:       return PLATFORM_KEYCODE_NUM_LOCK;
        case XK_Caps_Lock:      return PLATFORM_KEYCODE_CAPS_LOCK;
        case XK_Print:          return PLATFORM_KEYCODE_PRINT_SCREEN;
        case XK_Scroll_Lock:    return PLATFORM_KEYCODE_SCROLL_LOCK;
        case XK_Pause:          return PLATFORM_KEYCODE_PAUSE;
        case XK_Delete:         return PLATFORM_KEYCODE_DELETE;
        case XK_BackSpace:      return PLATFORM_KEYCODE_BACKSPACE;
        case XK_Return:         return PLATFORM_KEYCODE_ENTER;
        case XK_Home:           return PLATFORM_KEYCODE_HOME;
        case XK_End:            return PLATFORM_KEYCODE_END;
        case XK_Page_Up:        return PLATFORM_KEYCODE_PAGE_UP;
        case XK_Page_Down:      return PLATFORM_KEYCODE_PAGE_DOWN;
        case XK_Insert:         return PLATFORM_KEYCODE_INSERT;
        case XK_Left:           return PLATFORM_KEYCODE_LEFT;
        case XK_Right:          return PLATFORM_KEYCODE_RIGHT;
        case XK_Down:           return PLATFORM_KEYCODE_DOWN;
        case XK_Up:             return PLATFORM_KEYCODE_UP;
        case XK_F1:             return PLATFORM_KEYCODE_F1;
        case XK_F2:             return PLATFORM_KEYCODE_F2;
        case XK_F3:             return PLATFORM_KEYCODE_F3;
        case XK_F4:             return PLATFORM_KEYCODE_F4;
        case XK_F5:             return PLATFORM_KEYCODE_F5;
        case XK_F6:             return PLATFORM_KEYCODE_F6;
        case XK_F7:             return PLATFORM_KEYCODE_F7;
        case XK_F8:             return PLATFORM_KEYCODE_F8;
        case XK_F9:             return PLATFORM_KEYCODE_F9;
        case XK_F10:            return PLATFORM_KEYCODE_F10;
        case XK_F11:            return PLATFORM_KEYCODE_F11;
        case XK_F12:            return PLATFORM_KEYCODE_F12;
        case XK_F13:            return PLATFORM_KEYCODE_F13;
        case XK_F14:            return PLATFORM_KEYCODE_F14;
        case XK_F15:            return PLATFORM_KEYCODE_F15;
        case XK_F16:            return PLATFORM_KEYCODE_F16;
        case XK_F17:            return PLATFORM_KEYCODE_F17;
        case XK_F18:            return PLATFORM_KEYCODE_F18;
        case XK_F19:            return PLATFORM_KEYCODE_F19;
        case XK_F20:            return PLATFORM_KEYCODE_F20;
        case XK_F21:            return PLATFORM_KEYCODE_F21;
        case XK_F22:            return PLATFORM_KEYCODE_F22;
        case XK_F23:            return PLATFORM_KEYCODE_F23;
        case XK_F24:            return PLATFORM_KEYCODE_F24;
        case XK_F25:            return PLATFORM_KEYCODE_F25;

        case XK_KP_Divide:      return PLATFORM_KEYCODE_KP_DIVIDE;
        case XK_KP_Multiply:    return PLATFORM_KEYCODE_KP_MULTIPLY;
        case XK_KP_Subtract:    return PLATFORM_KEYCODE_KP_SUBTRACT;
        case XK_KP_Add:         return PLATFORM_KEYCODE_KP_ADD;

        case XK_KP_Insert:      return PLATFORM_KEYCODE_KP_0;
        case XK_KP_End:         return PLATFORM_KEYCODE_KP_1;
        case XK_KP_Down:        return PLATFORM_KEYCODE_KP_2;
        case XK_KP_Page_Down:   return PLATFORM_KEYCODE_KP_3;
        case XK_KP_Left:        return PLATFORM_KEYCODE_KP_4;
        case XK_KP_Begin:       return PLATFORM_KEYCODE_KP_5;
        case XK_KP_Right:       return PLATFORM_KEYCODE_KP_6;
        case XK_KP_Home:        return PLATFORM_KEYCODE_KP_7;
        case XK_KP_Up:          return PLATFORM_KEYCODE_KP_8;
        case XK_KP_Page_Up:     return PLATFORM_KEYCODE_KP_9;
        case XK_KP_Delete:      return PLATFORM_KEYCODE_KP_DECIMAL;
        case XK_KP_Equal:       return PLATFORM_KEYCODE_KP_EQUAL;
        case XK_KP_Enter:       return PLATFORM_KEYCODE_KP_ENTER;

        case XK_a:              return PLATFORM_KEYCODE_A;
        case XK_b:              return PLATFORM_KEYCODE_B;
        case XK_c:              return PLATFORM_KEYCODE_C;
        case XK_d:              return PLATFORM_KEYCODE_D;
        case XK_e:              return PLATFORM_KEYCODE_E;
        case XK_f:              return PLATFORM_KEYCODE_F;
        case XK_g:              return PLATFORM_KEYCODE_G;
        case XK_h:              return PLATFORM_KEYCODE_H;
        case XK_i:              return PLATFORM_KEYCODE_I;
        case XK_j:              return PLATFORM_KEYCODE_J;
        case XK_k:              return PLATFORM_KEYCODE_K;
        case XK_l:              return PLATFORM_KEYCODE_L;
        case XK_m:              return PLATFORM_KEYCODE_M;
        case XK_n:              return PLATFORM_KEYCODE_N;
        case XK_o:              return PLATFORM_KEYCODE_O;
        case XK_p:              return PLATFORM_KEYCODE_P;
        case XK_q:              return PLATFORM_KEYCODE_Q;
        case XK_r:              return PLATFORM_KEYCODE_R;
        case XK_s:              return PLATFORM_KEYCODE_S;
        case XK_t:              return PLATFORM_KEYCODE_T;
        case XK_u:              return PLATFORM_KEYCODE_U;
        case XK_v:              return PLATFORM_KEYCODE_V;
        case XK_w:              return PLATFORM_KEYCODE_W;
        case XK_x:              return PLATFORM_KEYCODE_X;
        case XK_y:              return PLATFORM_KEYCODE_Y;
        case XK_z:              return PLATFORM_KEYCODE_Z;
        case XK_1:              return PLATFORM_KEYCODE_1;
        case XK_2:              return PLATFORM_KEYCODE_2;
        case XK_3:              return PLATFORM_KEYCODE_3;
        case XK_4:              return PLATFORM_KEYCODE_4;
        case XK_5:              return PLATFORM_KEYCODE_5;
        case XK_6:              return PLATFORM_KEYCODE_6;
        case XK_7:              return PLATFORM_KEYCODE_7;
        case XK_8:              return PLATFORM_KEYCODE_8;
        case XK_9:              return PLATFORM_KEYCODE_9;
        case XK_0:              return PLATFORM_KEYCODE_0;
        case XK_space:          return PLATFORM_KEYCODE_SPACE;
        case XK_minus:          return PLATFORM_KEYCODE_MINUS;
        case XK_equal:          return PLATFORM_KEYCODE_EQUAL;
        case XK_bracketleft:    return PLATFORM_KEYCODE_LEFT_BRACKET;
        case XK_bracketright:   return PLATFORM_KEYCODE_RIGHT_BRACKET;
        case XK_backslash:      return PLATFORM_KEYCODE_BACKSLASH;
        case XK_semicolon:      return PLATFORM_KEYCODE_SEMICOLON;
        case XK_apostrophe:     return PLATFORM_KEYCODE_APOSTROPHE;
        case XK_grave:          return PLATFORM_KEYCODE_GRAVE_ACCENT;
        case XK_comma:          return PLATFORM_KEYCODE_COMMA;
        case XK_period:         return PLATFORM_KEYCODE_PERIOD;
        case XK_slash:          return PLATFORM_KEYCODE_SLASH;
        case XK_less:           return PLATFORM_KEYCODE_WORLD_1; /* At least in some layouts... */
        default:                return PLATFORM_KEYCODE_INVALID;
    }
}


static PlatformMouseButton
Translate_mouse_button(unsigned int button) {
    switch (button) {
        case Button1: return PLATFORM_MOUSE_BUTTON_LEFT;
        case Button2: return PLATFORM_MOUSE_BUTTON_MIDDLE;
        case Button3: return PLATFORM_MOUSE_BUTTON_RIGHT;
        default:      return PLATFORM_MOUSE_BUTTON_INVALID;
    }
}

static void
Process_events() {
	XEvent event;
	for (int remaining_events = XPending(x11_data.xdisplay);
		remaining_events > 0;
		--remaining_events) {

        XNextEvent(x11_data.xdisplay, &event);
		switch (event.type) {
			case ConfigureNotify: {
        	    if ((event.xconfigure.width != app_data.w_width) ||
					(event.xconfigure.height != app_data.w_width)) {

					app_data.w_width  = event.xconfigure.width;
					app_data.w_height = event.xconfigure.height;
					app_data.window_resized = true;
        	    }
        	    break;
			}
			case FocusOut: {
				// Puts all the keys down to up because will never notify the up event
				Put_keys_up();
				break;
			}
			case KeyPress: {
                int keycode = (int)event.xkey.keycode;
                PlatformKeyCode key = Translate_key(keycode);
				if (app_data.keyboard[key] == KEY_STATUS_DOWN ||
					app_data.keyboard[key] == KEY_STATUS_HOLD) {

					app_data.keyboard[key] = KEY_STATUS_HOLD;
				}
				else {
					app_data.keyboard[key] = KEY_STATUS_DOWN;
				}
				break;
			}
			case KeyRelease: {
				// Checks if the key release is from an autorepeat
				// https://stackoverflow.com/questions/2100654/ignore-auto-repeat-in-x11-applications
				if (XEventsQueued(x11_data.xdisplay, QueuedAfterReading)) {
					XEvent next_event;
					XPeekEvent(x11_data.xdisplay, &next_event);
					if (next_event.type == KeyPress &&
						next_event.xkey.time == event.xkey.time &&
  					    next_event.xkey.keycode == event.xkey.keycode) {
					  break;
  					}
				}
                int keycode = (int)event.xkey.keycode;
                PlatformKeyCode key = Translate_key(keycode);
				app_data.keyboard[key] = KEY_STATUS_UP;
				break;
            }
			case ButtonPress: {
				PlatformMouseButton btn = Translate_mouse_button(event.xbutton.button);
				if (btn != PLATFORM_MOUSE_BUTTON_INVALID) {
					if (app_data.mouse.buttons[btn] == KEY_STATUS_DOWN ||
						app_data.mouse.buttons[btn] == KEY_STATUS_HOLD) {

						app_data.mouse.buttons[btn] = KEY_STATUS_HOLD;
					}
					else {
						app_data.mouse.buttons[btn] = KEY_STATUS_DOWN;
					}
				}
				else {
					/* might be a scroll event */
				}
				break;
			}
			case ButtonRelease:
			{
				PlatformMouseButton btn = Translate_mouse_button(event.xbutton.button);
				if (btn != PLATFORM_MOUSE_BUTTON_INVALID) {
					app_data.mouse.buttons[btn] = KEY_STATUS_UP;
				}
			    break;
			}
			case ClientMessage: {
				if (event.xclient.message_type == x11_data.WM_PROTOCOLS) {
            	    const Atom protocol = (Atom)event.xclient.data.l[0];
            	    if (protocol == x11_data.WM_DELETE_WINDOW) {
            	        app_data.quit_requested = true;
            	    }
            	}
				break;
			}
			case MotionNotify: {
				app_data.mouse.x = (f32) event.xmotion.x;
				app_data.mouse.y = (f32) event.xmotion.y;
				break;
			}
            break;// TODO(Tano) HANDLE EVERYTHING!!!!!

		}
	}

}








////////////////////////////////////////////////////////////////////////////////
//
//
//
// SOUND API
//
//
//
////////////////////////////////////////////////////////////////////////////////

#include <pthread.h>
#define ALSA_PCM_NEW_HW_PARAMS_API
#include <alsa/asoundlib.h>


static struct SoundPlayerBackend{
	snd_pcm_t* device;
	float* buffer;
	pthread_t thread;
	volatile bool thread_stop;
} sound_player_backend = {0};


static void
Cleanup_sound_player(void) {
	if (sound_player_backend.device) {
		snd_pcm_close(sound_player_backend.device);
		sound_player_backend.device = NULL;
		snd_config_update_free_global();
	}
}


/* the streaming callback runs in a separate thread */
static void *
Sound_player_thread(void *param) {
    snd_pcm_prepare(sound_player_backend.device);
    while (!sound_player_backend.thread_stop) {
        /* snd_pcm_writei() will be blocking until it needs data */
        int write_res = snd_pcm_writei(
				sound_player_backend.device,
				sound_player_backend.buffer,
				(snd_pcm_uframes_t)sound_player_info.buffer_frames
		);
        if (write_res < 0) {
            /* underrun occurred */
            snd_pcm_prepare(sound_player_backend.device);
        }
        else {
            /* fill the streaming buffer with new data */
			sound_player_info.stream_cb(
					sound_player_info.user_data,
					sound_player_backend.buffer,
					sound_player_info.buffer_frames,
					sound_player_info.num_channels
			);
        }
    }
    return 0;
}



int
Launch_sound_player(SoundPlayerDesc *desc) {
    int dir; uint32_t rate;
    int rc = snd_pcm_open(&sound_player_backend.device, "default", SND_PCM_STREAM_PLAYBACK,	0);
    if (rc < 0) {
        fprintf(stderr, "Cannot open the audio device.\n");
		Cleanup_sound_player();
        return -1;
    }

    /* configuration works by restricting the 'configuration space' step
       by step, we require all parameters except the sample rate and buffer size to
       match perfectly
    */
    snd_pcm_hw_params_t* params = 0;
    snd_pcm_hw_params_alloca(&params);
    int error_code = snd_pcm_hw_params_any(sound_player_backend.device, params);
    if (0 > error_code) {
        printf("Broken configuration for %s PCM: no configurations available\n", snd_strerror(error_code));
        return error_code;
    }
    error_code = snd_pcm_hw_params_set_access(sound_player_backend.device, params, SND_PCM_ACCESS_RW_INTERLEAVED);
    if (0 > error_code) {
        printf("snd_pcm_hw_params_set_access() failed. Code: %d. Error: %s\n", error_code, snd_strerror(error_code));
        return error_code;
    }
    if (0 > snd_pcm_hw_params_set_format(sound_player_backend.device, params, SND_PCM_FORMAT_FLOAT_LE)) {
        fprintf(stderr, "Cannot device doesn't support floats\n");
		Cleanup_sound_player();
        return -1;
    }

    /* let ALSA pick a nearby sampling rate */
    rate = (uint32_t) desc->sample_rate;
    dir = 0;
    error_code = snd_pcm_hw_params_set_rate_near(sound_player_backend.device, params, &rate, &dir);
    if (0 > error_code) {
        const char *error = snd_strerror(error_code);
        fprintf(stderr, "snd_pcm_hw_params_set_rate_near() failed. Code: %d, Error: %s\n", error_code, error);
		Cleanup_sound_player();
        return -1;
    }

	/* Pick a near buffer size */
	snd_pcm_uframes_t buffer_frames = desc->buffer_frames;
    if (0 > snd_pcm_hw_params_set_buffer_size_near(sound_player_backend.device, params, &buffer_frames)) {
        fprintf(stderr, "Requested buffer size not supported\n");
		Cleanup_sound_player();
        return -1;
    }

    if (0 > snd_pcm_hw_params_set_channels(sound_player_backend.device, params, (uint32_t)desc->num_channels)) {
        fprintf(stderr, "Requested channel count not supported\n");
		Cleanup_sound_player();
        return -1;
    }

    
    if (0 > snd_pcm_hw_params(sound_player_backend.device, params)) {
        fprintf(stderr, "snd_pcm_hw_params() failed\n");
		Cleanup_sound_player();
        return -1;
    }


    /* read back actual sample rate and channels */
    sound_player_info.sample_rate   = rate;
	sound_player_info.buffer_frames = buffer_frames;
	sound_player_info.user_data     = desc->user_data;
	sound_player_info.stream_cb     = desc->stream_cb;
	sound_player_info.num_channels  = desc->num_channels;

    /* allocate the streaming buffer */
	size_t buffer_nelems = sound_player_info.buffer_frames*sound_player_info.num_channels;
    sound_player_backend.buffer = Alloc(f32, buffer_nelems);
    memset(sound_player_backend.buffer, 0, (size_t)(buffer_nelems * sizeof(f32)));

    /* create the buffer-streaming start thread */
    if (0 != pthread_create(&sound_player_backend.thread, 0, Sound_player_thread, 0)) {
        fprintf(stderr, "pthread_create() failed\n");
		Cleanup_sound_player();
        return -1;
    }

    return 0;
}





void
Terminate_sound_player(void) {
	sound_player_backend.thread_stop = true;
	void *dummy;
	pthread_join(sound_player_backend.thread, &dummy);
	Cleanup_sound_player();
	if (sound_player_backend.buffer != NULL) {
		free(sound_player_backend.buffer);
		sound_player_backend.buffer = NULL;
	}
}




////////////////////////////////////////////////////////////////////////////////
//
//
//
// TIME API
//
//
//
////////////////////////////////////////////////////////////////////////////////


struct timespec app_initial_time = {0};

void
Init_app_time() {
	clock_gettime(CLOCK_MONOTONIC_RAW, &app_initial_time);
}


i64
App_time() {
	struct timespec current;
	clock_gettime(CLOCK_MONOTONIC_RAW, &current);
	i64 result = (current.tv_sec - app_initial_time.tv_sec) * (1 * T_SECOND);
	result += current.tv_nsec - app_initial_time.tv_nsec;
	return result;
}








////////////////////////////////////////////////////////////////////////////////
//
//
//
// THREAD API
//
//
//
////////////////////////////////////////////////////////////////////////////////

#include <pthread.h> 

void
Mutex_init(Mutex *mutex) {
	if (pthread_mutex_init(mutex, NULL) != 0) {
		Panic("Failed to init mutex.");
	}
}

void
Mutex_deinit(Mutex *mutex) {
}

void
Mutex_lock(Mutex *mutex) {
	if (pthread_mutex_lock(mutex) != 0) {
		Panic("Failed to lock mutex.");
	}
}

void
Mutex_unlock(Mutex *mutex) {
	if (pthread_mutex_unlock(mutex) != 0) {
		Panic("Failed to unlock mutex.");
	}
}




////////////////////////////////////////////////////////////////////////////////
//
//
//
// FILESYSTEM API
//
//
//
////////////////////////////////////////////////////////////////////////////////


#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

// Documented above
#if !defined(BUNDLED_TREE)
u64
Get_file_modify_time(const char *filename) {
    struct stat file_stat;
    int err = stat(filename, &file_stat);
    if (err != 0) {
		return 0;
	}
    return (u64)file_stat.st_mtime;
}
#endif













/////////////////////////////////////////////////////////////////////////////////
//
//
//
//                     WASM IMPLEMENTATION ||||
//                                         VVVV
//
//
////////////////////////////////////////////////////////////////////////////////

#elif defined(PLATFORM_WASM)


extern int  JS_setup_GL(void);
extern bool JS_window_has_focus(void);
extern void JS_set_window_title(const char *);
extern void JS_set_fullscreen(bool);
extern void JS_show_mouse(bool);
extern i32  JS_get_time(void); // FIXME(Tano) We should return i64 but chrome fails

static struct WasmData {
	int(*wasm_handle_frame)(void);
	bool focus;
} wasm_data = {0};



int
Create_window(WindowDesc *window_desc) {
	JS_set_window_title(window_desc->title);
	return 0;
}


void
Set_fullscreen(bool enable) {
	JS_set_fullscreen(enable);
}

void
Show_mouse(bool show) {
	JS_show_mouse(show);
}

void
Set_window_title(const char *title) {
	JS_set_window_title(title);
}

void
Destroy_window() {
	// We set the viewport black
	glClearColor(0.0, 0.0, 0.0, 1.0);// background color	
	glClear(GL_COLOR_BUFFER_BIT);
}

#define MAX_KEYMAPPING 512

PlatformKeyCode key_mapping[MAX_KEYMAPPING] = {
	[8]   = PLATFORM_KEYCODE_BACKSPACE,
	[9]   = PLATFORM_KEYCODE_TAB,
	[13]  = PLATFORM_KEYCODE_ENTER,
	[16]  = PLATFORM_KEYCODE_LEFT_SHIFT,
	[17]  = PLATFORM_KEYCODE_LEFT_CONTROL,
	[18]  = PLATFORM_KEYCODE_LEFT_ALT,
	[19]  = PLATFORM_KEYCODE_PAUSE,
	[27]  = PLATFORM_KEYCODE_ESCAPE,
	[32]  = PLATFORM_KEYCODE_SPACE,
	[33]  = PLATFORM_KEYCODE_PAGE_UP,
	[34]  = PLATFORM_KEYCODE_PAGE_DOWN,
	[35]  = PLATFORM_KEYCODE_END,
	[36]  = PLATFORM_KEYCODE_HOME,
	[37]  = PLATFORM_KEYCODE_LEFT,
	[38]  = PLATFORM_KEYCODE_UP,
	[39]  = PLATFORM_KEYCODE_RIGHT,
	[40]  = PLATFORM_KEYCODE_DOWN,
	[45]  = PLATFORM_KEYCODE_INSERT,
	[46]  = PLATFORM_KEYCODE_DELETE,
	[48]  = PLATFORM_KEYCODE_0,
	[49]  = PLATFORM_KEYCODE_1,
	[50]  = PLATFORM_KEYCODE_2,
	[51]  = PLATFORM_KEYCODE_3,
	[52]  = PLATFORM_KEYCODE_4,
	[53]  = PLATFORM_KEYCODE_5,
	[54]  = PLATFORM_KEYCODE_6,
	[55]  = PLATFORM_KEYCODE_7,
	[56]  = PLATFORM_KEYCODE_8,
	[57]  = PLATFORM_KEYCODE_9,
	[59]  = PLATFORM_KEYCODE_SEMICOLON,
	[64]  = PLATFORM_KEYCODE_EQUAL,
	[65]  = PLATFORM_KEYCODE_A,
	[66]  = PLATFORM_KEYCODE_B,
	[67]  = PLATFORM_KEYCODE_C,
	[68]  = PLATFORM_KEYCODE_D,
	[69]  = PLATFORM_KEYCODE_E,
	[70]  = PLATFORM_KEYCODE_F,
	[71]  = PLATFORM_KEYCODE_G,
	[72]  = PLATFORM_KEYCODE_H,
	[73]  = PLATFORM_KEYCODE_I,
	[74]  = PLATFORM_KEYCODE_J,
	[75]  = PLATFORM_KEYCODE_K,
	[76]  = PLATFORM_KEYCODE_L,
	[77]  = PLATFORM_KEYCODE_M,
	[78]  = PLATFORM_KEYCODE_N,
	[79]  = PLATFORM_KEYCODE_O,
	[80]  = PLATFORM_KEYCODE_P,
	[81]  = PLATFORM_KEYCODE_Q,
	[82]  = PLATFORM_KEYCODE_R,
	[83]  = PLATFORM_KEYCODE_S,
	[84]  = PLATFORM_KEYCODE_T,
	[85]  = PLATFORM_KEYCODE_U,
	[86]  = PLATFORM_KEYCODE_V,
	[87]  = PLATFORM_KEYCODE_W,
	[88]  = PLATFORM_KEYCODE_X,
	[89]  = PLATFORM_KEYCODE_Y,
	[90]  = PLATFORM_KEYCODE_Z,
	[91]  = PLATFORM_KEYCODE_LEFT_SUPER,
	[93]  = PLATFORM_KEYCODE_MENU,
	[96]  = PLATFORM_KEYCODE_KP_0,
	[97]  = PLATFORM_KEYCODE_KP_1,
	[98]  = PLATFORM_KEYCODE_KP_2,
	[99]  = PLATFORM_KEYCODE_KP_3,
	[100] = PLATFORM_KEYCODE_KP_4,
	[101] = PLATFORM_KEYCODE_KP_5,
	[102] = PLATFORM_KEYCODE_KP_6,
	[103] = PLATFORM_KEYCODE_KP_7,
	[104] = PLATFORM_KEYCODE_KP_8,
	[105] = PLATFORM_KEYCODE_KP_9,
	[106] = PLATFORM_KEYCODE_KP_MULTIPLY,
	[107] = PLATFORM_KEYCODE_KP_ADD,
	[109] = PLATFORM_KEYCODE_KP_SUBTRACT,
	[110] = PLATFORM_KEYCODE_KP_DECIMAL,
	[111] = PLATFORM_KEYCODE_KP_DIVIDE,
	[112] = PLATFORM_KEYCODE_F1,
	[113] = PLATFORM_KEYCODE_F2,
	[114] = PLATFORM_KEYCODE_F3,
	[115] = PLATFORM_KEYCODE_F4,
	[116] = PLATFORM_KEYCODE_F5,
	[117] = PLATFORM_KEYCODE_F6,
	[118] = PLATFORM_KEYCODE_F7,
	[119] = PLATFORM_KEYCODE_F8,
	[120] = PLATFORM_KEYCODE_F9,
	[121] = PLATFORM_KEYCODE_F10,
	[122] = PLATFORM_KEYCODE_F11,
	[123] = PLATFORM_KEYCODE_F12,
	[144] = PLATFORM_KEYCODE_NUM_LOCK,
	[145] = PLATFORM_KEYCODE_SCROLL_LOCK,
	[173] = PLATFORM_KEYCODE_MINUS,
	[186] = PLATFORM_KEYCODE_SEMICOLON,
	[187] = PLATFORM_KEYCODE_EQUAL,
	[188] = PLATFORM_KEYCODE_COMMA,
	[189] = PLATFORM_KEYCODE_MINUS,
	[190] = PLATFORM_KEYCODE_PERIOD,
	[191] = PLATFORM_KEYCODE_SLASH,
	[192] = PLATFORM_KEYCODE_GRAVE_ACCENT,
	[219] = PLATFORM_KEYCODE_LEFT_BRACKET,
	[220] = PLATFORM_KEYCODE_BACKSLASH,
	[221] = PLATFORM_KEYCODE_RIGHT_BRACKET,
	[222] = PLATFORM_KEYCODE_APOSTROPHE,
	[224] = PLATFORM_KEYCODE_LEFT_SUPER,
};


static void
Process_events() {
}

int
Setup_GL() {
	if (JS_setup_GL()) {
		return 0;
	}
	else {
		return -1;
	}
}

void
Cleanup_GL() {
	// Nothing to do on javascript
}

void
Set_swap_interval(u8 interval) {
}

i64 app_initial_time = 0;

void
Init_app_time(void) {
	app_initial_time = T_MSECOND*(i64)JS_get_time();
}


i64
App_time(void) {
	return (T_MSECOND*(i64)JS_get_time()) - app_initial_time;
}



/* WAFN_application_frame
 * ========================================
 *
 * DESCRIPTIION
 * ----------------------------------------
 * Binding to call from javascript on each frame
 *
 */
__attribute__((export_name("WAFN_application_frame")))int
WAFN_application_frame(void) {
	Assert(wasm_data.wasm_handle_frame, "The handler cannot be NULL!!!");
	Process_events();
	int result = wasm_data.wasm_handle_frame();
	Clear_events();

	// If the focus has changed to not focus
	// Puts all the keys down to up because will never notify the up event
	bool current_focus = JS_window_has_focus();
	if (!current_focus && wasm_data.focus) {
		Put_keys_up();
	}
	wasm_data.focus = current_focus;

	return result;
}


/* WAFN_report_resize
 * ========================================
 *
 * DESCRIPTIION
 * ----------------------------------------
 * Binding to call from javascript on each frame
 *
 */
__attribute__((export_name("WAFN_report_resize"))) void
WAFN_report_resize(u32 w, u32 h) {
	app_data.w_width  = w;
	app_data.w_height = h;
	app_data.window_resized = true;
}



/* WAFN_report_keydown
 * ========================================
 *
 * DESCRIPTIION
 * ----------------------------------------
 * Binding to call from javascript on each keydown
 *
 */
__attribute__((export_name("WAFN_report_keydown"))) void
WAFN_report_keydown(int keycode) {
	if (keycode >= 0 && keycode < MAX_KEYMAPPING) {
		u32 key_id = key_mapping[keycode];
		if (app_data.keyboard[key_id] == KEY_STATUS_DOWN ||
			app_data.keyboard[key_id] == KEY_STATUS_HOLD) {

			app_data.keyboard[key_id] = KEY_STATUS_HOLD;
		}
		else {
			app_data.keyboard[key_id] = KEY_STATUS_DOWN;
		}
	}
}


/* WAFN_report_keyup
 * ========================================
 *
 * DESCRIPTIION
 * ----------------------------------------
 * Binding to call from javascript on each keyup
 *
 */
__attribute__((export_name("WAFN_report_keyup"))) void
WAFN_report_keyup(int keycode) {
	if (keycode >= 0 && keycode < MAX_KEYMAPPING) {
		app_data.keyboard[key_mapping[keycode]] = KEY_STATUS_UP;
	}
}


/* WAFN_set_mouse_xy
 * ========================================
 *
 * DESCRIPTIION
 * ----------------------------------------
 * Binding to call from javascript when the mouse moves
 *
 */
__attribute__((export_name("WAFN_set_mouse_xy"))) void
WAFN_set_mouse_xy(f32 x, f32 y) {
	app_data.mouse.x = x;
	app_data.mouse.y = y;
}



/* WAFN_set_buttons_down
 * ========================================
 *
 * DESCRIPTIION
 * ----------------------------------------
 * Receives a button mask and sets the enabled buttons down
 *
 * PARAMS
 * ----------------------------------------------------------
 * buttons: the mask that says which buttons are down
 * 1 is left, 2 is right and 4 is middle
 */
__attribute__((export_name("WAFN_set_mouse_buttons_down"))) void
WAFN_set_mouse_buttons_down(u32 buttons) {
	if (buttons & 1) {
		if (app_data.mouse.buttons[PLATFORM_MOUSE_BUTTON_LEFT] == KEY_STATUS_DOWN ||
			app_data.mouse.buttons[PLATFORM_MOUSE_BUTTON_LEFT] == KEY_STATUS_HOLD) {

			app_data.mouse.buttons[PLATFORM_MOUSE_BUTTON_LEFT]  = KEY_STATUS_HOLD;
		}
		else {
			app_data.mouse.buttons[PLATFORM_MOUSE_BUTTON_LEFT]  = KEY_STATUS_DOWN;
		}
	}
	if (buttons & 2) {
		if (app_data.mouse.buttons[PLATFORM_MOUSE_BUTTON_RIGHT] == KEY_STATUS_DOWN ||
			app_data.mouse.buttons[PLATFORM_MOUSE_BUTTON_RIGHT] == KEY_STATUS_HOLD) {

			app_data.mouse.buttons[PLATFORM_MOUSE_BUTTON_RIGHT] = KEY_STATUS_HOLD;
		}
		else {
			app_data.mouse.buttons[PLATFORM_MOUSE_BUTTON_RIGHT] = KEY_STATUS_DOWN;
		}
	}
	if (buttons & 4) {
		if (app_data.mouse.buttons[PLATFORM_MOUSE_BUTTON_MIDDLE] == KEY_STATUS_DOWN ||
			app_data.mouse.buttons[PLATFORM_MOUSE_BUTTON_MIDDLE] == KEY_STATUS_HOLD) {

			app_data.mouse.buttons[PLATFORM_MOUSE_BUTTON_MIDDLE] = KEY_STATUS_HOLD;
		}
		else {
			app_data.mouse.buttons[PLATFORM_MOUSE_BUTTON_MIDDLE] = KEY_STATUS_DOWN;
		}
	}
}


/* WAFN_set_button_up
 * ========================================
 *
 * DESCRIPTIION
 * ----------------------------------------
 * Receives a button to set it down
 *
 * PARAMS
 * ----------------------------------------------------------
 * button: 0 is left, 1 is middle, 2 is right.
 *
 */
__attribute__((export_name("WAFN_set_mouse_button_up"))) void
WAFN_set_mouse_button_up(u32 button) {
	if (button == 0) {
		app_data.mouse.buttons[PLATFORM_MOUSE_BUTTON_LEFT] = KEY_STATUS_UP;
	}
	else if (button == 1) {
		app_data.mouse.buttons[PLATFORM_MOUSE_BUTTON_MIDDLE] = KEY_STATUS_UP;
	}
	else if (button == 2) {
		app_data.mouse.buttons[PLATFORM_MOUSE_BUTTON_RIGHT] = KEY_STATUS_UP;
	}
}




////////////////////////////////////////////////////////////////////////////////
//
//
//
// SOUND API
//
//
//
////////////////////////////////////////////////////////////////////////////////

static struct SoundPlayerBackend {
	f32 *buffer;
} sound_player_backend = {0};

extern void JS_start_audio(u32 sample_rate, u32 n_channels, u32 n_frames, f32 *buffer);
extern void JS_terminate_audio();


__attribute__((export_name("WAFN_audio_cb"))) bool
WAFN_audio_cb(u32 n_frames, u32 n_channels, f32 *buffer) {
	int result = sound_player_info.stream_cb(
			sound_player_info.user_data,
			buffer,
			n_frames,
			n_channels
	);

	return result == 0;
}


int
Launch_sound_player(SoundPlayerDesc *desc) {
	sound_player_backend.buffer = Alloc(f32, desc->num_channels * desc->buffer_frames);
	JS_start_audio(
			desc->sample_rate,
			desc->num_channels,
			desc->buffer_frames,
			sound_player_backend.buffer
	);

	sound_player_info.sample_rate   = desc->sample_rate;
	sound_player_info.buffer_frames = desc->buffer_frames;
	sound_player_info.user_data     = desc->user_data;
	sound_player_info.stream_cb     = desc->stream_cb;
	sound_player_info.num_channels  = desc->num_channels;
	return 0;
}





void
Terminate_sound_player(void) {
	if (sound_player_backend.buffer != NULL) {
		free(sound_player_backend.buffer);
		sound_player_backend.buffer = NULL;
	}
	JS_terminate_audio();
}








////////////////////////////////////////////////////////////////////////////////
//
//
//
// THREAD API
//
//
//
////////////////////////////////////////////////////////////////////////////////

//
// We don't use threads on wasm, so this does nothing.
//

void
Mutex_init(Mutex *mutex) {
}

void
Mutex_deinit(Mutex *mutex) {
}

void
Mutex_lock(Mutex *mutex) {
}

void
Mutex_unlock(Mutex *mutex) {
}

#endif // defined (PLATFORM_WASM)



#if defined(BUNDLED_TREE)
	extern const unsigned int BUNDLE_FILES_COUNT;
	extern const char * const bundle_filenames[];
	extern char * const bundle_files_index[];
	extern const unsigned int bundle_file_sizes[];
#endif


// Documented on the above, on the head section
void
Clear_events() {
	// We have to reset the window resize event every frame.
	app_data.window_resized = false;

	// We must trigger the windows resize event if we went to fullscreen, the right dimensions
	// are informed when the fullscreen has changed.
	// On x11 and wasm the resize event is triggered anyway, so this doesn't matter.
	if (app_data.fullscreen_changed == true) {
		app_data.fullscreen_changed = false;
		app_data.window_resized = true;
	}
	
	// The keys that are UP must be NONE the next frame
	for (u32 i = 0; i < PLATFORM_MAX_KEYCODES; ++i) {
		if (app_data.keyboard[i] == KEY_STATUS_DOWN) {
			app_data.keyboard[i] = KEY_STATUS_HOLD;
		}
		if (app_data.keyboard[i] == KEY_STATUS_UP) {
			app_data.keyboard[i] = KEY_STATUS_NONE;
		}
	}

	// The same with mouse buttons
	for (u32 i = 0; i < 3; ++i) {
		if (app_data.mouse.buttons[i] == KEY_STATUS_DOWN) {
			app_data.mouse.buttons[i] = KEY_STATUS_HOLD;
		}
		if (app_data.mouse.buttons[i] == KEY_STATUS_UP) {
			app_data.mouse.buttons[i] = KEY_STATUS_NONE;
		}
	}
}


void
Put_keys_up() {
	// The keys that are DOWN go UP
	for (u32 i = 0; i < PLATFORM_MAX_KEYCODES; ++i) {
		if (app_data.keyboard[i] == KEY_STATUS_DOWN || app_data.keyboard[i] == KEY_STATUS_HOLD) {
			app_data.keyboard[i] = KEY_STATUS_UP;
		}
	}
	// The same with mouse buttons
	for (u32 i = 0; i < 3; ++i) {
		if (app_data.mouse.buttons[i] == KEY_STATUS_DOWN || app_data.keyboard[i] == KEY_STATUS_HOLD) {
			app_data.mouse.buttons[i] = KEY_STATUS_UP;
		}
	}
}


// Documented on the above, on the head section
#if defined(PLATFORM_WASM)

int
Run_application_loop(int(*application_frame)(void)) {
	wasm_data.wasm_handle_frame = application_frame;
	return 0;
}

#else

int
Run_application_loop(int(*application_frame)(void)) {
	Assert(application_frame, "application_frame cannot be NULL!!!");
	int loop_result = 0;
	for (;;) {
		Process_events();
		loop_result = application_frame();
		if (loop_result != 0) {break;}
		Clear_events();
		Swap_buffers();
	}
#if defined(PLATFORM_WINDOWS)
	// I'm not sure if this can be done here or must be always inside the windowProc :/ Tano.
	PostQuitMessage(0);
#endif
	if (loop_result == 1) {
		return 0;
	}
	return loop_result;
}

#endif







// Documented on the above, on the head section
u8 *
Get_file_contentsz(const char *filename, u32 *size) {
#if defined(BUNDLED_TREE)
	// This is a linear search, we may use a hashmap :/
	for (u32 i = 0; i < BUNDLE_FILES_COUNT; ++i) {
		if (strcmp(filename, bundle_filenames[i]) == 0) {
			if (size) { *size = bundle_file_sizes[i]; }
			return (u8 *)bundle_files_index[i];
		}
	}
	return NULL;
#else
	FILE *f = fopen(filename, "rb");
	if (f == NULL) {
		return NULL;
	}
	fseek(f, 0L, SEEK_END);	
	u32 file_size = ftell(f);
	fseek(f, 0L, SEEK_SET);	
	u8 *data = Alloc(u8, file_size + 1);
	fread(data, 1, file_size, f);
	fclose(f);

	data[file_size] = '\0';
	if (size) {*size = file_size;}
	return data;
#endif
}



// Documented on the above, on the head section
void
Free_file_contents(u8 *data) {
	if (data == NULL) {return;}
#if !defined(BUNDLED_TREE)
	free(data);
#endif
}


// Documented above
#if defined(BUNDLED_TREE)
u64
Get_file_modify_time(const char *filename) {
	return 0;
}
#endif


#endif // _PLATFORM_H_


