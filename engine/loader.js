var BINARY_PATH;

var WASM;
var WASM_MEMORY;
var HEAP8, HEAPU8, HEAP16, HEAPU16, HEAP32, HEAPU32, HEAPF32;

var GLsetupContext;
var audio_node;
var audioCtx;

// Heap access helpers.
function setHeap() {
	var buf = WASM_MEMORY.buffer;
	HEAP   = buf;
	HEAP8 = new Int8Array(buf);
	HEAP16 = new Int16Array(buf);
	HEAP32 = new Int32Array(buf);
	HEAPU8 = new Uint8Array(buf);
	HEAPU16 = new Uint16Array(buf);
	HEAPU32 = new Uint32Array(buf);
	HEAPF32 = new Float32Array(buf);
}

function checkHeap() {
	if (HEAP.byteLength == 0) {
		console.log("Heap has changed");
		setHeap();
	}
}


function Pointer_stringify(ptr, length) {
	if (length === 0 || !ptr) return '';
	for (var hasUtf = 0, t, i = 0;;)
	{
		t = HEAPU8[((ptr)+(i))>>0];
		hasUtf |= t;
		if (t == 0 && !length) break;
		i++;
		if (length && i == length) break;
	}
	if (!length) length = i;

	if (hasUtf & 128)
	{
		for(var r=HEAPU8,o=ptr,p=ptr+length,F=String.fromCharCode,e,f,i,n,C,t,a,g='';;)
		{
			if(o==p||(e=r[o++],!e)) return g;
			128&e?(f=63&r[o++],192!=(224&e)?(i=63&r[o++],224==(240&e)?e=(15&e)<<12|f<<6|i:(n=63&r[o++],240==(248&e)?e=(7&e)<<18|f<<12|i<<6|n:(C=63&r[o++],248==(252&e)?e=(3&e)<<24|f<<18|i<<12|n<<6|C:(t=63&r[o++],e=(1&e)<<30|f<<24|i<<18|n<<12|C<<6|t))),65536>e?g+=F(e):(a=e-65536,g+=F(55296|a>>10,56320|1023&a))):g+=F((31&e)<<6|f)):g+=F(e);
		}
	}

	// split up into chunks, because .apply on a huge string can overflow the stack
	for (var ret = '', curr; length > 0; ptr += 1024, length -= 1024)
		ret += String.fromCharCode.apply(String, HEAPU8.subarray(ptr, ptr + Math.min(length, 1024)));
	return ret;
}

function stringToUTF8Array(str, outU8Array, outIdx, maxBytesToWrite) {
	if(!(0<maxBytesToWrite))return 0;
	for(var e=str,r=outU8Array,f=outIdx,i=maxBytesToWrite,a=f,t=f+i-1,b=0;b<e.length;++b)
	{
		var k=e.charCodeAt(b);
		if(55296<=k&&k<=57343&&(k=65536+((1023&k)<<10)|1023&e.charCodeAt(++b)),k<=127){if(t<=f)break;r[f++]=k;}
		else if(k<=2047){if(t<=f+1)break;r[f++]=192|k>>6,r[f++]=128|63&k;}
		else if(k<=65535){if(t<=f+2)break;r[f++]=224|k>>12,r[f++]=128|k>>6&63,r[f++]=128|63&k;}
		else if(k<=2097151){if(t<=f+3)break;r[f++]=240|k>>18,r[f++]=128|k>>12&63,r[f++]=128|k>>6&63,r[f++]=128|63&k;}
		else if(k<=67108863){if(t<=f+4)break;r[f++]=248|k>>24,r[f++]=128|k>>18&63,r[f++]=128|k>>12&63,r[f++]=128|k>>6&63,r[f++]=128|63&k;}
		else{if(t<=f+5)break;r[f++]=252|k>>30,r[f++]=128|k>>24&63,r[f++]=128|k>>18&63,r[f++]=128|k>>12&63,r[f++]=128|k>>6&63,r[f++]=128|63&k;}
	}
	return r[f]=0,f-a;
}

function GL_WASM_IMPORTS(env) {
	var GLlastError;
	var GLctx;
	var GLcounter = 1;
	var GLbuffers = [];
	var GLprograms = [];
	var GLframebuffers = [];
	var GLtextures = [];
	var GLuniforms = [];
	var GLshaders = [];
	var GLprogramInfos = {};
	var GLpackAlignment = 4;
	var GLunpackAlignment = 4;
	var GLMINI_TEMP_BUFFER_SIZE = 256;
	var GLminiTempBuffer = null;
	var GLminiTempBufferViews = [0];
	GLminiTempBuffer = new Float32Array(GLMINI_TEMP_BUFFER_SIZE);
	for (var i = 0; i < GLMINI_TEMP_BUFFER_SIZE; i++) GLminiTempBufferViews[i] = GLminiTempBuffer.subarray(0, i+1);

	GLsetupContext = function(canvas)
	{
		var attr = { majorVersion: 1, minorVersion: 0, antialias: true, alpha: false };
		var errorInfo = '';
		try
		{
			let onContextCreationError = function(event) { errorInfo = event.statusMessage || errorInfo; };
			canvas.addEventListener('webglcontextcreationerror', onContextCreationError, false);
			try
			{
				//if (attr.majorVersion == 1 && attr.minorVersion == 0)
					GLctx = canvas.getContext('webgl', attr) || canvas.getContext('experimental-webgl', attr);
				//else if (attr.majorVersion == 2 && attr.minorVersion == 0)
				//	GLctx = canvas.getContext('webgl2', attr);
				//else
				//	throw 'Unsupported WebGL context version ' + majorVersion + '.' + minorVersion;
			}
			finally { canvas.removeEventListener('webglcontextcreationerror', onContextCreationError, false); }
			if (!GLctx) throw 'Could not create context';
		}
		catch (e)
		{
			abort('WEBGL', e + (errorInfo ? ' (' + errorInfo + ')' : ''));
		}

		/*
		// Detect the presence of a few extensions manually, this GL interop layer itself will need to know if they exist.
		if (attr.majorVersion < 2)
		{
			// Extension available from Firefox 26 and Google Chrome 30
			var instancedArraysExt = GLctx.getExtension('ANGLE_instanced_arrays');
			if (instancedArraysExt)
			{
				GLctx.vertexAttribDivisor = function(index, divisor) { instancedArraysExt.vertexAttribDivisorANGLE(index, divisor); };
				GLctx.drawArraysInstanced = function(mode, first, count, primcount) { instancedArraysExt.drawArraysInstancedANGLE(mode, first, count, primcount); };
				GLctx.drawElementsInstanced = function(mode, count, type, indices, primcount) { instancedArraysExt.drawElementsInstancedANGLE(mode, count, type, indices, primcount); };
			}
			// Extension available from Firefox 25 and WebKit
			var vaoExt = GLctx.getExtension('OES_vertex_array_object');
			if (vaoExt)
			{
				GLctx.createVertexArray = function() { return vaoExt.createVertexArrayOES(); };
				GLctx.deleteVertexArray = function(vao) { vaoExt.deleteVertexArrayOES(vao); };
				GLctx.bindVertexArray = function(vao) { vaoExt.bindVertexArrayOES(vao); };
				GLctx.isVertexArray = function(vao) { return vaoExt.isVertexArrayOES(vao); };
			}
			var drawBuffersExt = GLctx.getExtension('WEBGL_draw_buffers');
			if (drawBuffersExt)
			{
				GLctx.drawBuffers = function(n, bufs) { drawBuffersExt.drawBuffersWEBGL(n, bufs); };
			}
		}
		GLctx.disjointTimerQueryExt = GLctx.getExtension('EXT_disjoint_timer_query');
		*/

		var exts = GLctx.getSupportedExtensions();
		if (exts && exts.length > 0)
		{
			// These are the 'safe' feature-enabling extensions that don't add any performance impact related to e.g. debugging, and
			// should be enabled by default so that client GLES2/GL code will not need to go through extra hoops to get its stuff working.
			// As new extensions are ratified at http://www.khronos.org/registry/webgl/extensions/ , feel free to add your new extensions
			// here, as long as they don't produce a performance impact for users that might not be using those extensions.
			// E.g. debugging-related extensions should probably be off by default.
			var W = 'WEBGL_', O = 'OES_', E = 'EXT_', T = 'texture_', C = 'compressed_'+T;
			var automaticallyEnabledExtensions = [ // Khronos ratified WebGL extensions ordered by number (no debug extensions):
				O+T+'float', O+T+'half_float', O+'standard_derivatives',
				O+'vertex_array_object', W+C+'s3tc', W+'depth_texture',
				O+'element_index_uint', E+T+'filter_anisotropic', E+'frag_depth',
				W+'draw_buffers', 'ANGLE_instanced_arrays', O+T+'float_linear',
				O+T+'half_float_linear', E+'blend_minmax', E+'shader_texture_lod',
				// Community approved WebGL extensions ordered by number:
				W+C+'pvrtc', E+'color_buffer_half_float', W+'color_buffer_float',
				E+'sRGB', W+C+'etc1', E+'disjoint_timer_query',
				W+C+'etc', W+C+'astc', E+'color_buffer_float',
				W+C+'s3tc_srgb', E+'disjoint_timer_query_webgl2'];
			exts.forEach(function(ext)
			{
				if (automaticallyEnabledExtensions.indexOf(ext) != -1)
				{
					// Calling .getExtension enables that extension permanently, no need to store the return value to be enabled.
					GLctx.getExtension(ext);
				}
			});
		}
		return true;
	};
	function getNewId(table)
	{
		var ret = GLcounter++;
		for (var i = table.length; i < ret; i++) table[i] = null;
		return ret;
	}
	function getSource(shader, count, string, length)
	{
		var source = '';
		for (var i = 0; i < count; ++i)
		{
			var frag;
			if (length)
			{
				var len = HEAP32[(((length)+(i*4))>>2)];
				if (len < 0) frag = Pointer_stringify(HEAP32[(((string)+(i*4))>>2)]);
				else frag = Pointer_stringify(HEAP32[(((string)+(i*4))>>2)], len);
			}
			else frag = Pointer_stringify(HEAP32[(((string)+(i*4))>>2)]);
			source += frag;
		}
		return source;
	}
	function populateUniformTable(program)
	{
		var p = GLprograms[program];
		GLprogramInfos[program] =
		{
			uniforms: {},
			maxUniformLength: 0, // This is eagerly computed below, since we already enumerate all uniforms anyway.
			maxAttributeLength: -1, // This is lazily computed and cached, computed when/if first asked, '-1' meaning not computed yet.
			maxUniformBlockNameLength: -1 // Lazily computed as well
		};

		var ptable = GLprogramInfos[program];
		var utable = ptable.uniforms;

		// A program's uniform table maps the string name of an uniform to an integer location of that uniform.
		// The global GLuniforms map maps integer locations to WebGLUniformLocations.
		var numUniforms = GLctx.getProgramParameter(p, GLctx.ACTIVE_UNIFORMS);
		for (var i = 0; i < numUniforms; ++i)
		{
			var u = GLctx.getActiveUniform(p, i);

			var name = u.name;
			ptable.maxUniformLength = Math.max(ptable.maxUniformLength, name.length+1);

			// Strip off any trailing array specifier we might have got, e.g. '[0]'.
			if (name.indexOf(']', name.length-1) !== -1)
			{
				var ls = name.lastIndexOf('[');
				name = name.slice(0, ls);
			}

			// Optimize memory usage slightly: If we have an array of uniforms, e.g. 'vec3 colors[3];', then
			// only store the string 'colors' in utable, and 'colors[0]', 'colors[1]' and 'colors[2]' will be parsed as 'colors'+i.
			// Note that for the GLuniforms table, we still need to fetch the all WebGLUniformLocations for all the indices.
			var loc = GLctx.getUniformLocation(p, name);
			if (loc != null)
			{
				var id = getNewId(GLuniforms);
				utable[name] = [u.size, id];
				GLuniforms[id] = loc;

				for (var j = 1; j < u.size; ++j)
				{
					var n = name + '['+j+']';
					loc = GLctx.getUniformLocation(p, n);
					id = getNewId(GLuniforms);

					GLuniforms[id] = loc;
				}
			}
		}
	}
	function GLrecordError(err)
	{
		if (!GLlastError) GLlastError = err;
	}

	env.glActiveTexture = function(x0) {
		GLctx.activeTexture(x0);
	};

	env.glAttachShader = function(program, shader) {
		GLctx.attachShader(GLprograms[program], GLshaders[shader]);
	};

	env.glBindAttribLocation = function(program, index, name) {
		checkHeap();
		GLctx.bindAttribLocation(GLprograms[program], index, Pointer_stringify(name));
	};

	env.glBindBuffer = function(target, buffer) {
		GLctx.bindBuffer(target, buffer ? GLbuffers[buffer] : null); 
	};

	env.glBindFramebuffer = function(target, framebuffer) {
		GLctx.bindFramebuffer(target, framebuffer ? GLframebuffers[framebuffer] : null);
	};

	env.glBindTexture = function(target, texture) {
		GLctx.bindTexture(target, texture ? GLtextures[texture] : null);
	};

	env.glBlendFunc = function(x0, x1) {
		GLctx.blendFunc(x0, x1);
	};

	env.glBlendFuncSeparate = function(x0, x1, x2, x3) {
		GLctx.blendFuncSeparate(x0, x1, x2, x3);
	}

	env.glBlendColor = function(x0, x1, x2, x3) {
		GLctx.blendColor(x0, x1, x2, x3);
	}

	env.glBlendEquation = function(x0) {
		GLctx.blendEquation(x0);
	}

	env.glBlendEquationSeparate = function(x0, x1) {
		GLctx.blendEquationSeparate(x0, x1);
	}

	env.glBufferData = function(target, size, data, usage) {
		checkHeap();
		if (!data) GLctx.bufferData(target, size, usage);
		else GLctx.bufferData(target, HEAPU8.subarray(data, data+size), usage);
	};

	env.glBufferSubData = function(target, offset, size, data) {
		checkHeap();
		GLctx.bufferSubData(target, offset, HEAPU8.subarray(data, data+size));
	};

	env.glClear = function(x0) {
		GLctx.clear(x0);
	};

	env.glClearColor = function(x0, x1, x2, x3) {
		GLctx.clearColor(x0, x1, x2, x3);
	};

	env.glColorMask = function(red, green, blue, alpha) {
		GLctx.colorMask(!!red, !!green, !!blue, !!alpha);
	};

	env.glCompileShader = function(shader) {
		GLctx.compileShader(GLshaders[shader]);
	};

	env.glCreateProgram = function() {
		var id = getNewId(GLprograms);
		var program = GLctx.createProgram();
		program.name = id;
		GLprograms[id] = program;
		return id;
	};

	env.glCreateShader = function(shaderType) {
		var id = getNewId(GLshaders);
		GLshaders[id] = GLctx.createShader(shaderType);
		return id;
	};

	env.glDeleteBuffers = function(n, buffers) {
		checkHeap();
		for (var i = 0; i < n; i++)
		{
			var id = HEAP32[(((buffers)+(i*4))>>2)];
			var buffer = GLbuffers[id];

			// From spec: "glDeleteBuffers silently ignores 0's and names that do not correspond to existing buffer objects."
			if (!buffer) continue;

			GLctx.deleteBuffer(buffer);
			buffer.name = 0;
			GLbuffers[id] = null;
		}
	};

	env.glDeleteFramebuffers = function(n, framebuffers) {
		checkHeap();
		for (var i = 0; i < n; ++i)
		{
			var id = HEAP32[(((framebuffers)+(i*4))>>2)];
			var framebuffer = GLframebuffers[id];
			if (!framebuffer) continue; // GL spec: "glDeleteFramebuffers silently ignores 0s and names that do not correspond to existing framebuffer objects".
			GLctx.deleteFramebuffer(framebuffer);
			framebuffer.name = 0;
			GLframebuffers[id] = null;
		}
	};

	env.glDeleteProgram = function(id) {
		if (!id) return;
		var program = GLprograms[id];
		if (!program) 
		{
			// glDeleteProgram actually signals an error when deleting a nonexisting object, unlike some other GL delete functions.
			GLrecordError(0x0501); // GL_INVALID_VALUE
			return;
		}
		GLctx.deleteProgram(program);
		program.name = 0;
		GLprograms[id] = null;
		GLprogramInfos[id] = null;
	};

	env.glDeleteShader = function(id) {
		if (!id) return;
		var shader = GLshaders[id];
		if (!shader)
		{
			// glDeleteShader actually signals an error when deleting a nonexisting object, unlike some other GL delete functions.
			GLrecordError(0x0501); // GL_INVALID_VALUE
			return;
		}
		GLctx.deleteShader(shader);
		GLshaders[id] = null;
	};

	env.glDeleteTextures = function(n, textures) {
		checkHeap();
		for (var i = 0; i < n; i++) {
			var id = HEAP32[(((textures)+(i*4))>>2)];
			var texture = GLtextures[id];
			if (!texture) continue; // GL spec: "glDeleteTextures silently ignores 0s and names that do not correspond to existing textures".
			GLctx.deleteTexture(texture);
			texture.name = 0;
			GLtextures[id] = null;
		}
	};

	env.glDepthFunc = function(x0) {
		GLctx.depthFunc(x0);
	};

	env.glDepthMask = function(flag) {
		GLctx.depthMask(!!flag);
	};

	env.glDetachShader = function(program, shader) {
		GLctx.detachShader(GLprograms[program], GLshaders[shader]);
	};

	env.glDisable = function(x0) {
		GLctx.disable(x0);
	};

	env.glDisableVertexAttribArray = function(index) {
		GLctx.disableVertexAttribArray(index);
	};

	env.glDrawArrays = function(mode, first, count) {
		GLctx.drawArrays(mode, first, count);
	};

	env.glDrawElements = function(mode, count, type, indices) {
		GLctx.drawElements(mode, count, type, indices);
	};

	env.glEnable = function(x0) {
		GLctx.enable(x0);
	};

	env.glEnableVertexAttribArray = function(index) {
		GLctx.enableVertexAttribArray(index);
	};

	env.glFramebufferTexture2D = function(target, attachment, textarget, texture, level) {
		GLctx.framebufferTexture2D(target, attachment, textarget, GLtextures[texture], level);
	};

	env.glGenBuffers = function(n, buffers) {
		checkHeap();
		for (var i = 0; i < n; i++)
		{
			var buffer = GLctx.createBuffer();
			if (!buffer)
			{
				GLrecordError(0x0502); // GL_INVALID_OPERATION
				while(i < n) HEAP32[(((buffers)+(i++*4))>>2)]=0;
				return;
			}
			var id = getNewId(GLbuffers);
			buffer.name = id;
			GLbuffers[id] = buffer;
			HEAP32[(((buffers)+(i*4))>>2)]=id;
		}
	};

	env.glGenFramebuffers = function(n, ids) {
		checkHeap();
		for (var i = 0; i < n; ++i)
		{
			var framebuffer = GLctx.createFramebuffer();
			if (!framebuffer)
			{
				GLrecordError(0x0502); // GL_INVALID_OPERATION
				while(i < n) HEAP32[(((ids)+(i++*4))>>2)]=0;
				return;
			}
			var id = getNewId(GLframebuffers);
			framebuffer.name = id;
			GLframebuffers[id] = framebuffer;
			HEAP32[(((ids)+(i*4))>>2)] = id;
		}
	};

	env.glGenTextures = function(n, textures) {
		checkHeap();
		for (var i = 0; i < n; i++)
		{
			var texture = GLctx.createTexture();
			if (!texture)
			{
				// GLES + EGL specs don't specify what should happen here, so best to issue an error and create IDs with 0.
				GLrecordError(0x0502); // GL_INVALID_OPERATION
				while(i < n) HEAP32[(((textures)+(i++*4))>>2)]=0;
				return;
			}
			var id = getNewId(GLtextures);
			texture.name = id;
			GLtextures[id] = texture;
			HEAP32[(((textures)+(i*4))>>2)]=id;
		}
	};

	env.glGetActiveUniform = function(program, index, bufSize, length, size, type, name) {
		checkHeap();
		program = GLprograms[program];
		var info = GLctx.getActiveUniform(program, index);
		if (!info) return; // If an error occurs, nothing will be written to length, size, type and name.

		if (bufSize > 0 && name)
		{
			var numBytesWrittenExclNull = stringToUTF8Array(info.name, HEAP8, name, bufSize);
			if (length) HEAP32[((length)>>2)]=numBytesWrittenExclNull;
		} else {
			if (length) HEAP32[((length)>>2)]=0;
		}

		if (size) HEAP32[((size)>>2)]=info.size;
		if (type) HEAP32[((type)>>2)]=info.type;
	};
	
	env.glGetAttribLocation = function(program, name) {
		checkHeap();
		program = GLprograms[program];
		name = Pointer_stringify(name);
		return GLctx.getAttribLocation(program, name);
	};

	function webGLGet(name_, p, type) {
		checkHeap();
		// Guard against user passing a null pointer.
		// Note that GLES2 spec does not say anything about how passing a null pointer should be treated.
		// Testing on desktop core GL 3, the application crashes on glGetIntegerv to a null pointer, but
		// better to report an error instead of doing anything random.
		if (!p) { GLrecordError(0x0501); return; } // GL_INVALID_VALUE

		var ret = undefined;
		switch(name_)
		{
			// Handle a few trivial GLES values
			case 0x8DFA: ret = 1; break; // GL_SHADER_COMPILER
			case 0x8DF8: // GL_SHADER_BINARY_FORMATS
				if (type !== 'Integer' && type !== 'Integer64') GLrecordError(0x0500); // GL_INVALID_ENUM
				return; // Do not write anything to the out pointer, since no binary formats are supported.
			case 0x8DF9: ret = 0; break; // GL_NUM_SHADER_BINARY_FORMATS
			case 0x86A2: // GL_NUM_COMPRESSED_TEXTURE_FORMATS
				// WebGL doesn't have GL_NUM_COMPRESSED_TEXTURE_FORMATS (it's obsolete since GL_COMPRESSED_TEXTURE_FORMATS returns a JS array that can be queried for length),
				// so implement it ourselves to allow C++ GLES2 code get the length.
				var formats = GLctx.getParameter(0x86A3); // GL_COMPRESSED_TEXTURE_FORMATS
				ret = formats.length;
				break;
		}

		if (ret === undefined)
		{
			var result = GLctx.getParameter(name_);
			switch (typeof(result))
			{
				case 'number':
					ret = result;
					break;
				case 'boolean':
					ret = result ? 1 : 0;
					break;
				case 'string':
					GLrecordError(0x0500); // GL_INVALID_ENUM
					return;
				case 'object':
					if (result === null)
					{
						// null is a valid result for some (e.g., which buffer is bound - perhaps nothing is bound), but otherwise
						// can mean an invalid name_, which we need to report as an error
						switch(name_)
						{
							case 0x8894: // ARRAY_BUFFER_BINDING
							case 0x8B8D: // CURRENT_PROGRAM
							case 0x8895: // ELEMENT_ARRAY_BUFFER_BINDING
							case 0x8CA6: // FRAMEBUFFER_BINDING
							case 0x8CA7: // RENDERBUFFER_BINDING
							case 0x8069: // TEXTURE_BINDING_2D
							case 0x8514: // TEXTURE_BINDING_CUBE_MAP
								ret = 0;
								break;
							default:
								GLrecordError(0x0500); // GL_INVALID_ENUM
								return;
						}
					}
					else if (result instanceof Float32Array || result instanceof Uint32Array || result instanceof Int32Array || result instanceof Array)
					{
						for (var i = 0; i < result.length; ++i) {
							switch (type)
							{
								case 'Integer': HEAP32[(((p)+(i*4))>>2)]=result[i]; break;
								case 'Float':   HEAPF32[(((p)+(i*4))>>2)]=result[i]; break;
								case 'Boolean': HEAP8[(((p)+(i))>>0)]=result[i] ? 1 : 0; break;
								default: abort('WEBGL', 'internal glGet error, bad type: ' + type);
							}
						}
						return;
					}
					else if (result instanceof WebGLBuffer || result instanceof WebGLProgram || result instanceof WebGLFramebuffer || result instanceof WebGLRenderbuffer || result instanceof WebGLTexture)
					{
						ret = result.name | 0;
					}
					else
					{
						GLrecordError(0x0500); // GL_INVALID_ENUM
						return;
					}
					break;
				default:
					GLrecordError(0x0500); // GL_INVALID_ENUM
					return;
			}
		}

		switch (type)
		{
			case 'Integer64': (tempI64 = [ret>>>0,(tempDouble=ret,(+(Math.abs(tempDouble))) >= 1.0 ? (tempDouble > 0.0 ? ((Math.min((+(Math.floor((tempDouble)/4294967296.0))), 4294967295.0))|0)>>>0 : (~~((+(Math.ceil((tempDouble - +(((~~(tempDouble)))>>>0))/4294967296.0)))))>>>0) : 0)],HEAP32[((p)>>2)]=tempI64[0],HEAP32[(((p)+(4))>>2)]=tempI64[1]); break;
			case 'Integer': HEAP32[((p)>>2)] = ret; break;
			case 'Float':   HEAPF32[((p)>>2)] = ret; break;
			case 'Boolean': HEAP8[((p)>>0)] = ret ? 1 : 0; break;
			default: abort('WEBGL', 'internal glGet error, bad type: ' + type);
		}
	}

	env.glGetError = function() {
		if (GLlastError)
		{
			var e = GLlastError;
			GLlastError = 0;
			return e;
		}
		return GLctx.getError();
	};

	env.glGetIntegerv = function(name_, p) {
		webGLGet(name_, p, 'Integer');
	};

	env.glGetProgramInfoLog = function(program, maxLength, length, infoLog) {
		checkHeap();
		var log = GLctx.getProgramInfoLog(GLprograms[program]);
		if (log === null) log = '(unknown error)';
		if (maxLength > 0 && infoLog)
		{
			var numBytesWrittenExclNull = stringToUTF8Array(log, HEAP8, infoLog, maxLength);
			if (length) HEAP32[((length)>>2)]=numBytesWrittenExclNull;
		}
		else if (length) HEAP32[((length)>>2)]=0;
	};

	env.glGetProgramiv = function(program, pname, p) {
		checkHeap();
		if (!p)
		{
			// GLES2 specification does not specify how to behave if p is a null pointer. Since calling this function does not make sense
			// if p == null, issue a GL error to notify user about it.
			GLrecordError(0x0501); // GL_INVALID_VALUE
			return;
		}

		if (program >= GLcounter)
		{
			GLrecordError(0x0501); // GL_INVALID_VALUE
			return;
		}

		var ptable = GLprogramInfos[program];
		if (!ptable)
		{
			GLrecordError(0x0502); //GL_INVALID_OPERATION
			return;
		}

		if (pname == 0x8B84) // GL_INFO_LOG_LENGTH
		{
			var log = GLctx.getProgramInfoLog(GLprograms[program]);
			if (log === null) log = '(unknown error)';
			HEAP32[((p)>>2)] = log.length + 1;
		}
		else if (pname == 0x8B87) //GL_ACTIVE_UNIFORM_MAX_LENGTH
		{
			HEAP32[((p)>>2)] = ptable.maxUniformLength;
		}
		else if (pname == 0x8B8A) //GL_ACTIVE_ATTRIBUTE_MAX_LENGTH
		{
			if (ptable.maxAttributeLength == -1)
			{
				program = GLprograms[program];
				var numAttribs = GLctx.getProgramParameter(program, GLctx.ACTIVE_ATTRIBUTES);
				ptable.maxAttributeLength = 0; // Spec says if there are no active attribs, 0 must be returned.
				for (var i = 0; i < numAttribs; ++i)
				{
					var activeAttrib = GLctx.getActiveAttrib(program, i);
					ptable.maxAttributeLength = Math.max(ptable.maxAttributeLength, activeAttrib.name.length+1);
				}
			}
			HEAP32[((p)>>2)] = ptable.maxAttributeLength;
		}
		else if (pname == 0x8A35) //GL_ACTIVE_UNIFORM_BLOCK_MAX_NAME_LENGTH
		{
			if (ptable.maxUniformBlockNameLength == -1)
			{
				program = GLprograms[program];
				var numBlocks = GLctx.getProgramParameter(program, GLctx.ACTIVE_UNIFORM_BLOCKS);
				ptable.maxUniformBlockNameLength = 0;
				for (var i = 0; i < numBlocks; ++i)
				{
					var activeBlockName = GLctx.getActiveUniformBlockName(program, i);
					ptable.maxUniformBlockNameLength = Math.max(ptable.maxUniformBlockNameLength, activeBlockName.length+1);
				}
			}
			HEAP32[((p)>>2)] = ptable.maxUniformBlockNameLength;
		}
		else
		{
			HEAP32[((p)>>2)] = GLctx.getProgramParameter(GLprograms[program], pname);
		}
	};

	env.glGetShaderInfoLog = function(shader, maxLength, length, infoLog) {
		checkHeap();
		var log = GLctx.getShaderInfoLog(GLshaders[shader]);
		if (log === null) log = '(unknown error)';
		if (maxLength > 0 && infoLog)
		{
			var numBytesWrittenExclNull = stringToUTF8Array(log, HEAP8, infoLog, maxLength);
			if (length) HEAP32[((length)>>2)] = numBytesWrittenExclNull;
		}
		else if (length) HEAP32[((length)>>2)] = 0;
	};

	env.glGetShaderiv = function(shader, pname, p) {
		checkHeap();
		if (!p)
		{
			// GLES2 specification does not specify how to behave if p is a null pointer. Since calling this function does not make sense
			// if p == null, issue a GL error to notify user about it.
			GLrecordError(0x0501); // GL_INVALID_VALUE
			return;
		}
		if (pname == 0x8B84) // GL_INFO_LOG_LENGTH
		{
			var log = GLctx.getShaderInfoLog(GLshaders[shader]);
			if (log === null) log = '(unknown error)';
			HEAP32[((p)>>2)] = log.length + 1;
		}
		else if (pname == 0x8B88) // GL_SHADER_SOURCE_LENGTH
		{
			var source = GLctx.getShaderSource(GLshaders[shader]);
			var sourceLength = (source === null || source.length == 0) ? 0 : source.length + 1;
			HEAP32[((p)>>2)] = sourceLength;
		}
		else HEAP32[((p)>>2)] = GLctx.getShaderParameter(GLshaders[shader], pname);
	};

	env.glGetUniformLocation = function(program, name) {
		checkHeap();
		name = Pointer_stringify(name);

		var arrayOffset = 0;
		if (name.indexOf(']', name.length-1) !== -1)
		{
			// If user passed an array accessor "[index]", parse the array index off the accessor.
			var ls = name.lastIndexOf('[');
			var arrayIndex = name.slice(ls+1, -1);
			if (arrayIndex.length > 0)
			{
				arrayOffset = parseInt(arrayIndex);
				if (arrayOffset < 0) return -1;
			}
			name = name.slice(0, ls);
		}

		var ptable = GLprogramInfos[program];
		if (!ptable) return -1;
		var utable = ptable.uniforms;
		var uniformInfo = utable[name]; // returns pair [ dimension_of_uniform_array, uniform_location ]
		if (uniformInfo && arrayOffset < uniformInfo[0])
		{
			// Check if user asked for an out-of-bounds element, i.e. for 'vec4 colors[3];' user could ask for 'colors[10]' which should return -1.
			return uniformInfo[1] + arrayOffset;
		}
		return -1;
	};

	env.glLineWidth = function(x0) {
		GLctx.lineWidth(x0);
	};

	env.glLinkProgram = function(program) {
		GLctx.linkProgram(GLprograms[program]);
		GLprogramInfos[program] = null; // uniforms no longer keep the same names after linking
		populateUniformTable(program);;
	};

	env.glPixelStorei = function(pname, param) {
		if (pname == 0x0D05) GLpackAlignment = param; //GL_PACK_ALIGNMENT
		else if (pname == 0x0cf5) GLunpackAlignment = param; //GL_UNPACK_ALIGNMENT
		GLctx.pixelStorei(pname, param);
	};

	function webGLGetTexPixelData(type, format, width, height, pixels, internalFormat) {
		checkHeap();
		var sizePerPixel;
		var numChannels;
		switch(format)
		{
			case 0x1906: case 0x1909: case 0x1902: numChannels = 1; break; //GL_ALPHA, GL_LUMINANCE, GL_DEPTH_COMPONENT
			case 0x190A: numChannels = 2; break; //GL_LUMINANCE_ALPHA
			case 0x1907: case 0x8C40: numChannels = 3; break; //GL_RGB, GL_SRGB_EXT
			case 0x1908: case 0x8C42: numChannels = 4; break; //GL_RGBA, GL_SRGB_ALPHA_EXT
			default: throw new Error();//GLrecordError(0x0500); return null; //GL_INVALID_ENUM
		}
		switch (type)
		{
			case 0x1401: sizePerPixel = numChannels*1; break; //GL_UNSIGNED_BYTE
			case 0x1403: case 0x8D61: sizePerPixel = numChannels*2; break; //GL_UNSIGNED_SHORT, GL_HALF_FLOAT_OES
			case 0x1405: case 0x1406: sizePerPixel = numChannels*4; break; //GL_UNSIGNED_INT, GL_FLOAT
			case 0x84FA: sizePerPixel = 4; break; //GL_UNSIGNED_INT_24_8_WEBGL/GL_UNSIGNED_INT_24_8
			case 0x8363: case 0x8033: case 0x8034: sizePerPixel = 2; break; //GL_UNSIGNED_SHORT_5_6_5, GL_UNSIGNED_SHORT_4_4_4_4, GL_UNSIGNED_SHORT_5_5_5_1
			default: throw new Error();//GLrecordError(0x0500); return null; //GL_INVALID_ENUM
		}

		function roundedToNextMultipleOf(x, y) { return Math.floor((x + y - 1) / y) * y; }
		var plainRowSize = width * sizePerPixel;
		var alignedRowSize = roundedToNextMultipleOf(plainRowSize, GLunpackAlignment);
		var bytes = (height <= 0 ? 0 : ((height - 1) * alignedRowSize + plainRowSize));

		switch(type)
		{
			case 0x1401: return HEAPU8.subarray((pixels),(pixels+bytes)); //GL_UNSIGNED_BYTE
			case 0x1406: return HEAPF32.subarray((pixels)>>2,(pixels+bytes)>>2); //GL_FLOAT
			case 0x1405: case 0x84FA: return HEAPU32.subarray((pixels)>>2,(pixels+bytes)>>2); //GL_UNSIGNED_INT, GL_UNSIGNED_INT_24_8_WEBGL/GL_UNSIGNED_INT_24_8
			case 0x1403: case 0x8363: case 0x8033: case 0x8034: case 0x8D61: return HEAPU16.subarray((pixels)>>1,(pixels+bytes)>>1); //GL_UNSIGNED_SHORT, GL_UNSIGNED_SHORT_5_6_5, GL_UNSIGNED_SHORT_4_4_4_4, GL_UNSIGNED_SHORT_5_5_5_1, GL_HALF_FLOAT_OES
			default: throw new Error();//GLrecordError(0x0500); return null; //GL_INVALID_ENUM
		}
	}

	env.glReadPixels = function(x, y, width, height, format, type, pixels) {
		var pixelData = webGLGetTexPixelData(type, format, width, height, pixels, format);
		if (!pixelData) return GLrecordError(0x0500); // GL_INVALID_ENUM
		GLctx.readPixels(x, y, width, height, format, type, pixelData);
	};

	env.glScissor = function(x0, x1, x2, x3) {
		GLctx.scissor(x0, x1, x2, x3)
	};

	env.glShaderSource = function(shader, count, string, length) {
		checkHeap();
		var source = getSource(shader, count, string, length);
		GLctx.shaderSource(GLshaders[shader], source);
	};

	env.glTexImage2D = function(target, level, internalFormat, width, height, border, format, type, pixels) {
		checkHeap();
		var pixelData = null;
		if (pixels) pixelData = webGLGetTexPixelData(type, format, width, height, pixels, internalFormat);
		GLctx.texImage2D(target, level, internalFormat, width, height, border, format, type, pixelData);
	};

	env.glTexParameteri = function(x0, x1, x2) {
		GLctx.texParameteri(x0, x1, x2);
	};

	env.glTexSubImage2D = function(target, level, xoffset, yoffset, width, height, format, type, pixels) {
		checkHeap();
		var pixelData = null;
		if (pixels) pixelData = webGLGetTexPixelData(type, format, width, height, pixels, 0);
		GLctx.texSubImage2D(target, level, xoffset, yoffset, width, height, format, type, pixelData);
	};

	env.glUniform1f = function(loc, v0) {
		GLctx.uniform1f(GLuniforms[loc], v0);
	};

	env.glUniform1i = function(loc, v0) {
		GLctx.uniform1i(GLuniforms[loc], v0);
	};

	env.glUniform2i = function(loc, v0, v1) {
		GLctx.uniform2i(GLuniforms[loc], v0, v1);
	};


	env.glUniform2f = function(loc, v0, v1) {
		GLctx.uniform2f(GLuniforms[loc], v0, v1);
	};

	env.glUniform3f = function(loc, v0, v1, v2) {
		GLctx.uniform3f(GLuniforms[loc], v0, v1, v2);
	};

	env.glUniform3fv = function(loc, count, value) {
		checkHeap();
		var view;
		if (3*count <= GLMINI_TEMP_BUFFER_SIZE)
		{
			// avoid allocation when uploading few enough uniforms
			view = GLminiTempBufferViews[3*count-1];
			for (var ptr = value>>2, i = 0; i != 3*count; i++)
			{
				view[i] = HEAPF32[ptr+i];
			}
		}
		else view = HEAPF32.subarray((value)>>2,(value+count*12)>>2);
		GLctx.uniform3fv(GLuniforms[loc], view);
	};

	env.glUniform4f = function(loc, v0, v1, v2, v3) {
		GLctx.uniform4f(GLuniforms[loc], v0, v1, v2, v3);
	};

	env.glUniformMatrix4fv = function(loc, count, transpose, value) {
		checkHeap();
		count<<=4;
		var view;
		if (count <= GLMINI_TEMP_BUFFER_SIZE)
		{
			// avoid allocation when uploading few enough uniforms
			view = GLminiTempBufferViews[count-1];
			for (var ptr = value>>2, i = 0; i != count; i += 4)
			{
				view[i  ] = HEAPF32[ptr+i  ];
				view[i+1] = HEAPF32[ptr+i+1];
				view[i+2] = HEAPF32[ptr+i+2];
				view[i+3] = HEAPF32[ptr+i+3];
			}
		}
		else view = HEAPF32.subarray((value)>>2,(value+count*4)>>2);
		GLctx.uniformMatrix4fv(GLuniforms[loc], !!transpose, view);
	};

	env.glUseProgram = function(program) {
		GLctx.useProgram(program ? GLprograms[program] : null);
	};

	env.glVertexAttrib4f = function(x0, x1, x2, x3, x4) {
		GLctx.vertexAttrib4f(x0, x1, x2, x3, x4);
	};

	env.glVertexAttrib4fv = function(index, v) {
		checkHeap();
		GLctx.vertexAttrib4f(index, HEAPF32[v>>2], HEAPF32[v+4>>2], HEAPF32[v+8>>2], HEAPF32[v+12>>2]);
	};

	env.glVertexAttribPointer = function(index, size, type, normalized, stride, ptr) {
		GLctx.vertexAttribPointer(index, size, type, !!normalized, stride, ptr);
	};

	env.glViewport = function(x0, x1, x2, x3) {
		GLctx.viewport(x0, x1, x2, x3);
	};
}




async function Load_game(bin_path) {
	BINARY_PATH = bin_path;

    function readChar(ptr) {
        return String.fromCharCode(heap_uint8[ptr]);
    }

    function readStr(ptr, len = -1) {
        let str = '';
        var end = heap_size_bytes;
        if (len != -1)
            end = ptr + len;
        for (var i = ptr; i < end && heap_uint8[i] != 0; ++i)
            str += readChar(i);
        return str;
    }

    function writeBuffer(offset, buf) {
        buf.copy(heap_uint8, offset);
    }

    function writeStr(offset, str) {
        var start = offset;
        for (var i = 0; i < str.length; i++) {
            write8(offset, str.charCodeAt(i));
            offset++;
        }
        write8(offset, 0);
        offset++;
        return offset - start;
    }

    function write8(offset, value) {
        HEAP8[offset] = value;
    }

    function write16(offset, value) {
        HEAP16[offset >> 1] = value;
    }

    function write32(offset, value) {
        HEAP32[offset >> 2] = value;
    }

    function write64(offset, valueFirst, valueLast) {
        HEAP32[(offset + 0) >> 2] = valueFirst;
        HEAP32[(offset + 4) >> 2] = valueLast;
    }

    function read8(offset) {
        return heap_uint8[offset];
    }

    function read16(offset) {
        return heap_uint16[offset >> 1];
    }

    function read32(offset) {
        return heap_uint32[offset >> 2];
    }

    let DEBUG = false;


    // WASI implemenation
    // See: https://github.com/WebAssembly/WASI/blob/master/design/WASI-core.md
    var wasi_interface = (function() {
        const STDIN = 0;
        const STDOUT = 1;
        const STDERR = 2;
        const MAXFD = 2;

        const WASI_ESUCCESS = 0;
        const WASI_EBADF = 8;
        const WASI_ENOTSUP = 58;
        const WASI_EPERM = 63;

        const WASI_PREOPENTYPE_DIR = 0;

        const WASI_LOOKUP_SYMLINK_FOLLOW = 0x1;

        const WASI_FDFLAG_APPEND = 0x0001;
        const WASI_FDFLAG_DSYNC = 0x0002;
        const WASI_FDFLAG_NONBLOCK = 0x0004;
        const WASI_FDFLAG_RSYNC = 0x0008;
        const WASI_FDFLAG_SYNC = 0x0010;

        const WASI_RIGHT_FD_DATASYNC = 0x00000001;
        const WASI_RIGHT_FD_READ = 0x00000002;
        const WASI_RIGHT_FD_SEEK = 0x00000004;
        const WASI_RIGHT_PATH_OPEN = 0x00002000;
        const WASI_RIGHT_PATH_FILESTAT_GET = 0x00040000;
        const WASI_RIGHT_FD_READDIR = 0x00004000;
        const WASI_RIGHT_FD_FILESTAT_GET = 0x00200000;
        const WASI_RIGHT_ALL = 0xffffffff;

        const WASI_FILETYPE_UNKNOWN = 0;
        const WASI_FILETYPE_BLOCK_DEVICE = 1;
        const WASI_FILETYPE_CHARACTER_DEVICE = 2;
        const WASI_FILETYPE_DIRECTORY = 3;
        const WASI_FILETYPE_REGULAR_FILE = 4;
        const WASI_FILETYPE_SOCKET_DGRAM = 5;
        const WASI_FILETYPE_SOCKET_STREAM = 6;
        const WASI_FILETYPE_SYMBOLIC_LINK = 7;

        const WASI_WHENCE_CUR = 0;
        const WASI_WHENCE_END = 1;
        const WASI_WHENCE_SET = 2;


        let argv = [];

        let stdin = (function() {
            return {
                flush: function() {}
            };
        })();

        let stdout = (function() {
            let buf = '';
            return {
                type: WASI_FILETYPE_CHARACTER_DEVICE,
                flags: WASI_FDFLAG_APPEND,
                write: function(str) {
                    buf += str;
                    if (buf[-1] == '\n') {
                        buf = buf.slice(0, -1);
                        print(buf);
                        buf = '';
                    }
                },
                flush: function() {
                    if (buf[-1] == '\n')
                        buf = buf.slice(0, -1);
                    print(buf);
                    buf = '';
                }
            }
        })();

        let stderr = (function() {
            let buf = '';
            return {
                type: WASI_FILETYPE_CHARACTER_DEVICE,
                flags: WASI_FDFLAG_APPEND,
                write: function(str) {
                    buf += str;
                    if (buf[-1] == '\n') {
                        buf = buf.slice(0, -1);
                        print(buf);
                        buf = '';
                    }
                },
                flush: function() {
                    if (buf[-1] == '\n')
                        buf = buf.slice(0, -1);
                    print(buf);
                    buf = '';
                }
            }
        })();

        let rootdir = (function() {
            return {
                type: WASI_FILETYPE_DIRECTORY,
                flags: 0,
                flush: function() {},
                name: "/",
                rootdir: "/",
                preopen: true,
                rights_base: WASI_RIGHT_ALL,
                rights_inheriting: WASI_RIGHT_ALL,
            };
        })();

        let openFile = function(filename) {
            dbg('openFile: ' + filename);
            let data = read(filename);
            let position = 0;
            let end = data.length;
            return {
                read: function(len) {
                    let start = position;
                    let end = Math.min(position + len, data.length);
                    position = end;
                    return data.slice(start, end)
                },
                seek: function(offset, whence) {
                    if (whence == WASI_WHENCE_CUR) {
                        position += offset;
                    } else if (whence == WASI_WHENCE_END) {
                        position += end + offset;
                    } else if (whence == WASI_WHENCE_SET) {
                        position = offset;
                    }
                    if (position > end) {
                        position = end;
                    } else if (position < 0) {
                        position = 0;
                    }
                    return position;
                },
                flush: function() {}
            };
        };

        let openFiles = [
            stdin,
            stdout,
            stderr,
            rootdir,
        ];

        let nextFD = openFiles.length;

        function isValidFD(fd) {
            return openFiles.hasOwnProperty(fd)
        }

        function trace(syscall_name, syscall_args) {
            if (DEBUG)
                dbg('wasi_snapshot_preview1.' + syscall_name + '(' + Array.from(syscall_args) + ')');
        }

        let module_api = {
            proc_exit: function(code) {
				if (code != 0) {
					throw new Error('proc_exit(' + code + ')');
				}
            },
            environ_sizes_get: function(environ_count_out_ptr, environ_buf_size_out_ptr) {
                trace('environ_sizes_get', arguments);
                checkHeap();
                const names = Object.getOwnPropertyNames(env);
                let total_space = 0;
                for (const i in names) {
                    let name = names[i];
                    let value = env[name];
                    // Format of each env entry is name=value with null terminator.
                    total_space += name.length + value.length + 2;
                }
                write64(environ_count_out_ptr, names.length);
                write64(environ_buf_size_out_ptr, total_space)
                return WASI_ESUCCESS;
            },
            environ_get: function(environ_pointers_out, environ_out) {
                trace('environ_get', arguments);
                let names = Object.getOwnPropertyNames(env);
                for (const i in names) {
                    write32(environ_pointers_out, environ_out);
                    environ_pointers_out += 4;
                    let name = names[i];
                    let value = env[name];
                    let full_string = name + "=" + value;
                    environ_out += writeStr(environ_out, full_string);
                }
                write32(environ_pointers_out, 0);
                return WASI_ESUCCESS;
            },
            args_sizes_get: function(args_count_out_ptr, args_buf_size_out_ptr) {
                return WASI_ESUCCESS;
            },
            args_get: function(args_pointers_out, args_out) {
                return WASI_ESUCCESS;
            },
            fd_pread: function(fd, iovs, iovs_len, offset, nread) {
                trace('fd_pread', arguments);
                checkHeap();
                if (!isValidFD(fd))
                    return WASI_EBADF;
                var file = openFiles[fd];
                if (fd.read == undefined)
                    return WASI_EBADF;
                throw new Error('Not implemented');
            },
            fd_prestat_get: function(fd, prestat_ptr) {
                trace('fd_prestat_get', arguments);
                checkHeap();
                if (!isValidFD(fd))
                    return WASI_EBADF;
                var file = openFiles[fd];
                if (!file.preopen)
                    return WASI_EBADF;
                write8(prestat_ptr, WASI_PREOPENTYPE_DIR);
                write64(prestat_ptr + 4, file.name.length);
                return 0;
            },
            fd_prestat_dir_name: function(fd, path_ptr, path_len) {
                trace('fd_prestat_dir_name', arguments);
                if (!isValidFD(fd))
                    return WASI_EBADF;
                var file = openFiles[fd];
                if (!file.preopen)
                    return WASI_EBADF;
                write64(path_len, file.name.length);
                writeStr(path_ptr, file.name);
                return 0;
            },
            fd_fdstat_get: function(fd, fdstat_ptr) {
                trace('fd_fdstat_get', arguments);
                if (!isValidFD(fd))
                    return WASI_EBADF;
                var file = openFiles[fd];
                write8(fdstat_ptr, file.type);
                write16(fdstat_ptr + 2, file.flags);
                write64(fdstat_ptr + 8, file.rights_base);
                write64(fdstat_ptr + 16, file.rights_inheriting);
                return WASI_ESUCCESS;
            },
            fd_fdstat_set_flags: function(fd, fdflags) {
                trace('fd_fdstat_set_flags', arguments);
                if (!isValidFD(fd))
                    return WASI_EBADF;
                return WASI_ESUCCESS;
            },
            fd_read: function(fd, iovs_ptr, iovs_len, nread) {
                if (!isValidFD(fd))
                    return WASI_EBADF;
                var file = openFiles[fd];
                if (!file.hasOwnProperty('read'))
                    return WASI_EBADF;
                checkHeap();
                let total = 0;
                for (let i = 0; i < iovs_len; i++) {
                    let buf = read32(iovs_ptr);
                    iovs_ptr += 4;
                    let len = read32(iovs_ptr);
                    iovs_ptr += 4;
                    let data = file.read(len);
                    if (data.length == 0) {
                        break;
                    }
                    writeBuffer(buf, data);
                    total += data.length;
                }
                write32(nread, total);
                return WASI_ESUCCESS;
            },
            fd_write: function(fd, iov, iovcnt, pOutResult) { //only used to write to stdout
				checkHeap();
				var ret = 0, str = '';
				if (iovcnt == 0) return 0;
				for (var i = 0; i < iovcnt; i++)
				{
					var ptr = HEAP32[(((iov)+(i*8))>>2)];
					var len = HEAP32[(((iov)+(i*8 + 4))>>2)];
					if (len < 0) return -1;
					ret += len;
					str += Pointer_stringify(ptr, len);
					//console.log('__wasi_fd_write - fd: ' + fd + ' - ['+i+'][len:'+len+']: ' + Pointer_stringify(ptr, len));
				}
				if      (fd == STDOUT) {console.log(str);}
				else if (fd == STDERR) {console.error(str);}
				else {console.log("[" + fd + "]: " + str);}
				HEAPU32[pOutResult>>2] = ret;
				return 0;
            },
            fd_close: function(fd) {
                trace('fd_close', arguments);
                if (!isValidFD(fd)) {
                    return WASI_EBADF;
                }
                openFiles[fd].flush();
                delete openFiles[fd];
                if (fd < nextFD) {
                    nextFD = fd;
                }
                return WASI_ESUCCESS;
            },
            fd_seek: function(fd, offset, whence, newoffset_ptr) {
                trace('fd_seek', arguments);
                if (!isValidFD(fd)) {
                    return WASI_EBADF;
                }
                let file = openFiles[fd];
                checkHeap();
                let intOffset = parseInt(offset.toString());
                let newPos = file.seek(intOffset, whence);
                write64(newoffset_ptr, newPos);
                dbg("done seek: " + newPos);
                return WASI_ESUCCESS;
            },
            path_filestat_get: function(dirfd, lookupflags, path, path_len, buf) {
                trace('path_filestat_get', arguments);
                if (!isValidFD(dirfd)) {
                    return WASI_EBADF;
                }
                let file = openFiles[dirfd];
                if (file != rootdir) {
                    return WASI_EBADF;
                }
                let filename = readStr(path, path_len);
                let stat = nodeFS.statSync(filename);
                if (stat.isFile()) {
                    write32(buf + 16, WASI_FILETYPE_REGULAR_FILE);
                } else if (stat.isSymbolicLink()) {
                    write32(buf + 16, WASI_FILETYPE_SYMBOLIC_LINK);
                } else if (stat.isDirectory()) {
                    write32(buf + 16, WASI_FILETYPE_DIRECTORY);
                } else if (stat.isCharDevice()) {
                    write32(buf + 16, WASI_FILETYPE_CHARACTER_DEVICE);
                } else if (stat.isBlockDevice()) {
                    write32(buf + 16, WASI_FILETYPE_BLOCK_DEVICE);
                } else {
                    write32(buf + 16, WASI_FILETYPE_UNKNOWN);
                }
                return WASI_ESUCCESS;
            },
            path_open: function(dirfd, dirflags, path, path_len, oflags, fs_rights_base, fs_rights_inheriting, fs_flags, fd_out) {
                throw new Error('Not implemented');
            },
            path_unlink_file: function(dirfd, path, path_len) {
                throw new Error('Not implemented');
            },
            path_remove_directory: function(dirfd, path, path_len) {
                throw new Error('Not implemented');
            },
            random_get: function(buf, buf_len) {
                trace('random_get', arguments);
                return WASI_ESUCCESS;
            },
            clock_res_get: function(a, b) {
                throw new Error('Not implemented');
            },
            clock_time_get: function(a, b, c) {
                throw new Error('Not implemented');
            },
            fd_advise: function(a, b, c, d) {
                throw new Error('Not implemented');
            },
            fd_allocate: function(a, b, c) {
                throw new Error('Not implemented');
            },
            fd_datasync: function(a) {
                throw new Error('Not implemented');
            },
            fd_filestat_get: function(a, b) {
                throw new Error('Not implemented');
            },
            fd_filestat_set_size: function(a, b) {
                throw new Error('Not implemented');
            },
            fd_filestat_set_times: function(a, b, c, d) {
                throw new Error('Not implemented');
            },
            fd_pwrite: function(a, b, c, d, e) {
                throw new Error('Not implemented');
            },
            fd_readdir: function(a, b, c, d, e) {
                throw new Error('Not implemented');
            },
            fd_renumber: function(a, b) {
                throw new Error('Not implemented');
            },
            fd_sync: function(a) {
                throw new Error('Not implemented');
            },
            fd_tell: function(a, b) {
                throw new Error('Not implemented');
            },
            path_create_directory: function(a, b, c) {
                throw new Error('Not implemented');
            },
            path_filestat_set_times: function(a, b, c, e, f, g, h) {
                throw new Error('Not implemented');
            },
            path_link: function(a, b, c, e, f, g, h) {
                throw new Error('Not implemented');
            },
            path_readlink: function(a, b, c, e, f, g) {
                throw new Error('Not implemented');
            },
            path_rename: function(a, b, c, e, f, g) {
                throw new Error('Not implemented');
            },
            path_symlink: function(a, b, c, e, f) {
                throw new Error('Not implemented');
            },
            poll_oneoff: function(a, b, c, e) {
                throw new Error('Not implemented');
            },
            proc_raise: function(a) {
                throw new Error('Not implemented');
            },
            sched_yield: function() {
                throw new Error('Not implemented');
            },
            sock_recv: function(a, b, c, d, e, f) {
                throw new Error('Not implemented');
            },
            sock_send: function(a, b, c, d, e) {
                throw new Error('Not implemented');
            },
            sock_shutdown: function(a, b) {
                throw new Error('Not implemented');
            },
        }

        return {
            onExit: function() {
                for (let k in openFiles) {
                    if (openFiles.hasOwnProperty(k)) {
                        openFiles[k].flush();
                    }
                }
            },
            api: module_api
        };
    })();
	
    let ffi = (function() {
        let env = {
            // Any non-wasi dependencies end up under 'env'.
            // TODO(sbc): Implement on the wasm side or add to WASI?
            _Unwind_RaiseException: function() {
                throw new Error('Not implemented');
            },
			JS_setup_GL: function() {
				return GLsetupContext(document.getElementById('canvas'));
			},
			JS_set_window_title: function(title_ptr) {
				window.document.title = Pointer_stringify(title_ptr);
			},
			JS_window_has_focus: function() {
				return document.hasFocus();
			},
			JS_get_time: function() {
				return Date.now();
			},
			
			// Function that starts the audio output
			JS_start_audio: function(sample_rate, n_channels, n_frames, buffer_ptr) {
				checkHeap();

				function findAlias(el, a, b, c) { return el[a+c] || el['moz'+b+c] || el['webkit'+b+c] || el['ms'+b+c]; }
				try { audioCtx = new (findAlias(window,'','','AudioContext'))({
					sampleRate: sample_rate,
					latencyHint: 'interactive',
				}); } catch (e) { }
				if (!audioCtx) { console,log('Warning: WebAudio not supported'); return; }

        		audio_node = audioCtx.createScriptProcessor(n_frames, 0, n_channels);
        		audio_node.onaudioprocess = function (event) {
					checkHeap();
        		    var num_frames = event.outputBuffer.length;
        		    var num_channels = event.outputBuffer.numberOfChannels;
					if (!WASM.exports.WAFN_audio_cb(num_frames, num_channels, buffer_ptr)) {
						audio_node.disconnect();
						return;
					}
					for (var chn = 0; chn < num_channels; chn++) {
        		        var chan = event.outputBuffer.getChannelData(chn);
        		        for (var i = 0; i < num_frames; i++) {
        		            chan[i] = HEAPF32[(buffer_ptr>>2) + ((num_channels*i)+chn)];
        		        }
        		    }
        		    
        		};
        		audio_node.connect(audioCtx.destination);

				// in some browsers, WebAudio needs to be activated on a user action
				var resume_webaudio = function() {
					if (audioCtx) {
						if (audioCtx.state === 'suspended') {
							audioCtx.resume();
						}
					}
				};
				document.addEventListener('click', resume_webaudio, {once:true});
				document.addEventListener('touchstart', resume_webaudio, {once:true});
				document.addEventListener('keydown', resume_webaudio, {once:true});
			},

			JS_terminate_audio: function() {
				if (audio_node) {
					audio_node.disconnect();
				}
			},

			JS_set_fullscreen: function(enable) {
				if(enable) {
					document.documentElement.requestFullscreen().catch(console.log);
				}else {
					document.exitFullscreen();
				}
			},

			JS_show_mouse: function(show) {
				if (show) {
					canvas.style.cursor = 'auto';
				}
				else {
					canvas.style.cursor = 'none';
			
				}
			}
        };
		GL_WASM_IMPORTS(env);
        return {
            env: env,
            wasi_snapshot_preview1: wasi_interface.api
        };
    })();


    const response = await fetch(BINARY_PATH);
    const bytes    = await response.arrayBuffer();
    const {
        instance
    } = await WebAssembly.instantiate(bytes, ffi);
    if (instance.exports.memory) {
		WASM_MEMORY = instance.exports.memory;
        setHeap();
    } else {
		throw new Error("Wasm program dont exports memory.")
	}
	WASM = instance;
    WASM.exports._start();
	

	// Sets the handler to report to the application the windows resizing.
	function Report_resize() {
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
		instance.exports.WAFN_report_resize(window.innerWidth, window.innerHeight);
	}
	Report_resize(); // We call directly to set the correct size on the first frame
	window.onresize = Report_resize;
	
	// Key down
	window.addEventListener('keydown', function(e) {
		// Prevent default browser's shortcuts
		e.preventDefault();
		WASM.exports.WAFN_report_keydown(e.keyCode);
	}, true);
	
	// Key up
	function Report_keyup(e) { WASM.exports.WAFN_report_keyup(e.keyCode); }
	window.addEventListener('keyup', Report_keyup, true);

	// Mouse position
	window.addEventListener('mousemove', function(evt) {
		var rect = canvas.getBoundingClientRect();
		WASM.exports.WAFN_set_mouse_xy(evt.clientX - rect.left, evt.clientY - rect.top);
	}, false);

	window.addEventListener('mousedown', function(e) {
		WASM.exports.WAFN_set_mouse_buttons_down(e.buttons);
	});

	window.addEventListener('mouseup', function(e) {
		WASM.exports.WAFN_set_mouse_button_up(e.button);
	});

	// Setups the frame handler
	function Application_frame() {
		var result = instance.exports.WAFN_application_frame();
		if (result == 0) {
			window.requestAnimationFrame(Application_frame);
		}
	}
	window.requestAnimationFrame(Application_frame);


}

