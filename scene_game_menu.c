
#include "menu.h"

void
Scene_in_game_menu() {
	f32 min_alpha = 0.5f;
	f32 max_alpha = 1.2f;
	f32 starfield_alpha = Lerp(min_alpha, max_alpha, (sinf(GLOBALS.current_time_f32*1.5f)+1.0f) * 0.5f);

	Draw_sprite_ex(0.0f, 0.0f, GAMEPLAY_WIDTH, GAMEPLAY_HEIGHT, 0.0f, 0.0f, 0.0f, &GLOBALS.starfield_bg, HMM_Vec4(1.0f, 1.0f, 1.0f, starfield_alpha));
	f32 title_height = (f32)GAMEPLAY_HEIGHT * 0.1f;
	f32 title_width = Get_text_width_with_height(&GLOBALS.font, "GAME PAUSED!", title_height);
	f32 title_x = ((f32)GAMEPLAY_WIDTH - title_width) * 0.5f;
	f32 title_y = (f32)GAMEPLAY_HEIGHT * 0.80;
	Draw_text_with_height(&GLOBALS.font, "GAME PAUSED!", title_height, title_x, title_y, 1, 1, 1, 1);

	if (Show_menu(
			GLOBALS.pause_menu_options,
			PAUSE_MENU_OPTION_COUNT,
			GLOBALS.pause_menu_selected_option,
			&GLOBALS.pause_menu_selected_option)) {

		GLOBALS.mouse_pointing = false;

		if (GLOBALS.pause_menu_selected_option == PAUSE_MENU_OPTION_CONTINUE) {
			GLOBALS.current_scene = SCENE_GAME;
		}
        else if (GLOBALS.pause_menu_selected_option == PAUSE_MENU_OPTION_CONTROLS) {
            Set_controls_return_scene(SCENE_IN_GAME_MENU);
            GLOBALS.current_scene = SCENE_CONTROLS;
        }
		else if (GLOBALS.pause_menu_selected_option == PAUSE_MENU_OPTION_QUIT) {
			Mixer_clear_tracks();
			Mixer_play_sound(GLOBALS.launch_option_sound, PLAY_SOUND_FLAG_NONE, MAX_VOLUME);
			Mixer_play_sound(GLOBALS.intro_music, PLAY_SOUND_FLAG_LOOP, MAX_VOLUME);
			GLOBALS.current_scene = SCENE_MENU;
		}
	}
}
