#ifndef MENU_H
#define MENU_H

typedef struct {
	const char *text;
	f32 height, x0, x1, y0, y1;
} MenuOption;

typedef struct {
	Texture *texture;
	f32 x0, x1, y0, y1, width, height;
    hmm_vec3 m_over_color;
    hmm_vec3 hold_color;
} ImageOption;


typedef enum {
	OPTION_STATE_UNSELECTED,
	OPTION_STATE_SELECTED,
	OPTION_STATE_HOLDED,
} OptionState;


MenuOption
Make_menu_option(const char *text, f32 y_percent);

int
Show_menu(MenuOption *options, u32 options_count, i32 selected_option, i32 *selected_option_out);

bool
Mouse_on_image_option(ImageOption *option);

ImageOption
Make_menu_image_option(Texture *texture, f32 x, f32 y, f32 width, f32 height);

void
Show_image_option(ImageOption *option, OptionState state);

#endif // MENU_H

