#ifndef UTILS_H
#define UTILS_H

#include "engine/base.h"
#include "game.h"

void
Calculate_viewport(u32 w_width, u32 w_height, f32 *vpx, f32 *vpy, f32 *vpw, f32 *vph);

void
Get_mouse_on_game_screen_coords(f32 *x_out, f32 *y_out);

void
Transform_triangle(Vec2 v0, Vec2 v1, Vec2 v2, f32 scale_x, f32 scale_y, f32 x, f32 y, f32 rotation, Vec2 *v0_out, Vec2 *v1_out, Vec2 *v2_out);

void
Draw_bounding_mesh(const BoundingMesh *bm, f32 scale_x, f32 scale_y, f32 x,f32 y, f32 rotation, Vec4 color);

bool
Triangles_overlap(Vec2 *t0, Vec2 *t1);

void
Draw_dummy_asteroid(Asteroid *asteroid);

#endif // UTILS_H
