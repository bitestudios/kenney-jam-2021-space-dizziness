# Kenney jam 2021 - space dizziness

## Compile for windows

We only crosscompiled from linux, so we don't know if this will compile correctly form windows.

To crosscompile easily from linux you can use zig.

Debug

```
zig cc space-dizziness.c engine/third_party/third_party.c icon.o -target x86_64-windows-gnu -g -lkernel32 -luser32 -lgdi32 -lole32 -o space-dizziness.exe
```

Release

```
zig cc space-dizziness.c engine/third_party/third_party.c .gen/bundled_tree.c icon.o -target x86_64-windows-gnu -DBUNDLED_TREE -O -lkernel32 -luser32 -lgdi32 -lole32 -o space-dizziness.exe
```

## Compile for linux

This was tested with clang, gcc and 'zig cc'. You can replace cc by your compiler program.

Debug build 

```
cc space-dizziness.c engine/third_party/third_party.c -g -Wall -lpthread -lasound -lEGL -lGLESv2 -lm -lX11 -lXi -lXcursor -o space-dizziness 
```

Release build 

```
cc space-dizziness.c engine/third_party/third_party.c .gen/bundled_tree.c -DBUNDLED_TREE -Os -Wall -lpthread -lasound -lEGL -lGLESv2 -lm -lX11 -lXi -lXcursor -o space-dizziness 
```

## Compile web version

You can compile using 'zig cc' or clang if you setup the wasi libc manually.

Debug build 

```
zig cc space-dizziness.c engine/third_party/third_party.c .gen/bundled_tree.c -target wasm32-wasi -DWASM -g -Wall -o space-dizziness.wasm
```

Release build 

```
zig cc space-dizziness.c engine/third_party/third_party.c .gen/bundled_tree.c -target wasm32-wasi -DBUNDLED_TREE -DWASM -Os -Wall -o space-dizziness.wasm
```

You have to put index.html, space-dizziness.wasm and engine/loader.js in the same folder and serve it with a http server.



