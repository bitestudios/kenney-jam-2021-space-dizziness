#ifndef GAME_H
#define GAME_H

#include "engine/base.h"
#include "engine/platform.h"
#include "engine/basic_gl.h"
#include "engine/font.h"
#include "engine/mixer.h"
#define HANDMADE_MATH_IMPLEMENTATION
#include "engine/third_party/HandmadeMath.h"
#include "engine/madmath.h"
#include "engine/sprite.h"

#include "menu.h"
#include "scene_main_menu.h"
#include "scene_game_menu.h"
#include "scene_credits.h"
#include "scene_congratulations.h"
#include "scene_controls.h"

#define GAME_TITLE  "Space dizziness!"

#define SAMPLE_RATE 32000
#define SOUND_CHANNELS 1

#define GAMEPLAY_WIDTH  320
#define GAMEPLAY_HEIGHT 240
// Desired aspect 
#define ASPECT_RATIO_W 4
#define ASPECT_RATIO_H 3

// Impulse effect parameters
#define IMPULSE_EFFECT_SHOW_TIME 0.3
#define IMPULSE_EFFECT_LENGTH 10
#define IMPULSE_EFFECT_WIDTH 20

#define HUD_HEIGHT (20.0f)
#define GAME_AREA_BORDER (0.0f)
#define GAME_AREA_X  (GAME_AREA_BORDER)
#define GAME_AREA_Y  (GAME_AREA_BORDER)
#define GAME_AREA_W  (GAMEPLAY_WIDTH - (GAME_AREA_BORDER * 2.0f))
#define GAME_AREA_H  (GAMEPLAY_HEIGHT - GAME_AREA_BORDER - HUD_HEIGHT)

#define MAX_VOLUME 0.5f

#define FONT_FILE "assets/kenvector_future.ttf"

#define GAMEPLAY_DELTA_TIME (1.0f/300.0f)

#define VSYNC_MSG_TIME 2.5f

#define PLAYER_SCALE 15.0f
#define PLAYER_CENTER_X 0.3f
#define PLAYER_CENTER_Y 0.5f
#define PLAYER_ANGULAR_MAX_VEL   (60.0f*8.0f)
#define PLAYER_ANGULAR_FRICTION  (60.0f*60.0f*0.25f)
#define PLAYER_ANGULAR_ACCEL     (60.0f*60.0f*0.5f)
#define PLAYER_MAX_VEL  (60.0f*5.0f)
#define PLAYER_MIN_VEL  (60.0f*0.25f)
#define PLAYER_ACCEL    (60.0f*60.0f*5.0f)
#define PLAYER_FRICTION (60.0f*60.0f*0.05f)
#define PLAYER_DIED_MAX_TIME (2.5f)
#define PLAYER_ARRIVED_MAX_TIME (2.5f)
#define PLAYER_COLLSION_RADIUS (PLAYER_SCALE+5.0f)
#define PLAYER_ENERGY_CONSUMPTION 20


#define CHECKPOINT_SCALE 20.0f
#define TOTAL_LEVELS 27
#define MAX_ASTEROIDS 20

#define EXPLOSION_TOTAL_PARTICLES 20
#define EXPLOSION_MAX_TIME 1.5f
#define EXPLOSION_PARTICLE_MAX_VEL  20.0f
#define EXPLOSION_PARTICLE_MIN_VEL  5.0f
#define EXPLOSION_PARTICLE_MAX_SIZE 3.0f
#define EXPLOSION_PARTICLE_MIN_SIZE 0.5f

enum {
	SCENE_MENU,
	SCENE_IN_GAME_MENU,
	SCENE_CREDITS,
	SCENE_GAME,
	SCENE_CONGRATULATIONS,
    SCENE_CONTROLS
};

typedef struct {
	u32 type;
	f32 x, y;
	f32 vx, vy;
	f32 dir, angular_vel;
	f32 scale_x;
	f32 scale_y;
} Asteroid;

typedef struct {
	struct {
		f32 x;
		f32 y;
		f32 dir;
		u32 impulses;
		f32 energy;
	} player;

	struct {
		f32 x;
		f32 y;
	} checkpoint;

	u32 total_asteroids;

	struct {
		f32 x;
		f32 y;
		f32 scale_x;
		f32 scale_y;
        f32 vx;
        f32 vy;
        f32 angular_speed;
        f32 rotation; //Initial angle of rotation
        u32 type;
	} asteroids[MAX_ASTEROIDS];

} Level;

typedef struct {
	f32 viewport_x, viewport_y, viewport_w, viewport_h;
	i64 current_time_i64;
	i64 delta_time_i64;
	f32 current_time_f32;
	f32 delta_time_f32;
	int mouse_x;
	int mouse_y;

	int current_scene;
	bool quit;
	bool fullscreen;
	bool show_fps;
	bool show_bounding_meshes;
	bool vsync_off;

	f32 vsync_msg_time;

	bool mouse_moved;
	bool mouse_pointing;

	i32 main_menu_selected_option;
	MenuOption main_menu_options[MAIN_MENU_OPTION_COUNT];
	i32 pause_menu_selected_option;
	MenuOption pause_menu_options[PAUSE_MENU_OPTION_COUNT];
    i32 credits_menu_selected_option;
    MenuOption credits_menu_options[CREDITS_MENU_OPTION_COUNT];
    i32 congratulations_menu_selected_option;
    MenuOption congratulations_menu_options[CONGRATULATIONS_MENU_OPTION_COUNT];
    i32 controls_menu_selected_option;
    MenuOption controls_menu_options[CONTROLS_MENU_OPTION_COUNT];

	Texture starfield_bg;
	Texture kenney_jam_logo;
	TextFont font;

	// Sounds
	u32 select_option_sound;
	u32 launch_option_sound;
	u32 arrive_sound;
	u32 explosion_sound;
	u32 impulse_sound;

	u32 intro_music;
	u32 main_theme_music;

	struct {
		Texture asteroid_texture;
		Level levels[TOTAL_LEVELS];
		u32 current_level;
		f32 partial_iters;
		struct {
			f32 x, y;
			f32 time;
			u64 seed;
		} explosion;
		struct {
			Texture texture;
			f32 x, y;
		} checkpoint;
		struct {
			Texture texture;
			f32 x, y;
			f32 vx, vy;
			f32 accel;
			f32 dir;
			f32 angular_accel;
			f32 angular_vel;
			f32 died_time;
			f32 arrived_time;
			bool died;
			bool arrived;
            u32 impulses_left;
            f32 energy_left;
		} player;
		u32 asteroids_count;
		Asteroid asteroids[MAX_ASTEROIDS];
	} game;

} Globals;


extern Globals GLOBALS;

typedef struct {
	u32 total_verts;
	Vec2 vertices[];
} BoundingMesh;

extern const BoundingMesh PLAYER_BOUNDIG_MESH;

extern const BoundingMesh *ASTEROIDS_BOUNDING_MESHES[];

extern const Level LEVEL0;
extern const Level LEVEL1;

enum {
	ASTEROID_TYPE_0
};

int
Load_level(const char* filename, Level *level);


#endif // GAME_H

