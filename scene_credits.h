#ifndef SCENE_CREDITS_H
#define SCENE_CREDITS_H

typedef enum {
	CREDITS_MENU_BACK,
	CREDITS_MENU_OPTION_COUNT,
} PauseMenuOptions;

void
Scene_credits();

#endif // SCENE_CREDITS_H

