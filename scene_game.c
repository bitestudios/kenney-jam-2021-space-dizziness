
#include "game.h"
#include "scene_game.h"
#include "engine/madmath.h"
#include "hud.h"

i32
Scene_game_init() {
    if (Setup_texture("assets/img/playerShip.png", true, &GLOBALS.game.player.texture) != 0) {
		fprintf(stderr, "Cannot load player texture.\n");
        return -1;
	}
    if (Setup_texture("assets/img/laserBlue16.png", true, &impulse.texture) != 0) {
        fprintf(stderr, "Cannot load laser texture.\n");
        return -1;
    }
    return 0;
}




void
Scene_game_prepare_level() {
	Level *level = &GLOBALS.game.levels[GLOBALS.game.current_level];
    GLOBALS.game.player.x = level->player.x;
	GLOBALS.game.player.y = level->player.y;
	GLOBALS.game.player.dir = level->player.dir;
    GLOBALS.game.player.impulses_left = level->player.impulses;
    GLOBALS.game.player.energy_left = level->player.energy;
	GLOBALS.game.player.vx = 0.0f;
	GLOBALS.game.player.vy = 0.0f;
	GLOBALS.game.player.angular_accel = 0.0f;
	GLOBALS.game.player.angular_vel = 0.0f;
	GLOBALS.game.player.arrived = false;
	GLOBALS.game.player.arrived_time = 0.0f;
	GLOBALS.game.player.died = false;
	GLOBALS.game.player.died_time = 0.0f;
	GLOBALS.game.checkpoint.x = level->checkpoint.x;
	GLOBALS.game.checkpoint.y = level->checkpoint.y;
	impulse.time_left = 0.0f;

	u32 asteroids_count = level->total_asteroids;
	GLOBALS.game.asteroids_count = asteroids_count;
	for (u32 i = 0; i < asteroids_count; ++i) {
		GLOBALS.game.asteroids[i].x = level->asteroids[i].x;
		GLOBALS.game.asteroids[i].y = level->asteroids[i].y;
		GLOBALS.game.asteroids[i].scale_x = level->asteroids[i].scale_x;
		GLOBALS.game.asteroids[i].scale_y = level->asteroids[i].scale_y;
		GLOBALS.game.asteroids[i].angular_vel = level->asteroids[i].angular_speed;
		GLOBALS.game.asteroids[i].type = ASTEROID_TYPE_0;
	}
}


void
Scene_game() {
    if (app_data.keyboard[PLATFORM_KEYCODE_P]         == KEY_STATUS_DOWN || 
		app_data.keyboard[PLATFORM_KEYCODE_ESCAPE]    == KEY_STATUS_DOWN || 
		app_data.keyboard[PLATFORM_KEYCODE_BACKSPACE] == KEY_STATUS_DOWN) {
        GLOBALS.current_scene = SCENE_IN_GAME_MENU;
    }


	if (GLOBALS.game.player.died) {
		GLOBALS.game.player.died_time += GLOBALS.delta_time_f32;
		if (GLOBALS.game.player.died_time > PLAYER_DIED_MAX_TIME) {
			Scene_game_prepare_level();
		}
	}

	if (GLOBALS.game.player.arrived) {
		GLOBALS.game.player.arrived_time += GLOBALS.delta_time_f32;
		Vec2 player_pos     = V2(GLOBALS.game.player.x, GLOBALS.game.player.y);
		Vec2 checkpoint_pos = V2(GLOBALS.game.checkpoint.x, GLOBALS.game.checkpoint.y);
		f32 lerp_factor = Clamp(GLOBALS.game.player.arrived_time / PLAYER_ARRIVED_MAX_TIME, 0.0f, 1.0f);
		player_pos = V2_lerp(player_pos, checkpoint_pos, lerp_factor);
		GLOBALS.game.player.x = player_pos.x;
		GLOBALS.game.player.y = player_pos.y;
		if (GLOBALS.game.player.arrived_time >= PLAYER_ARRIVED_MAX_TIME) {
			++GLOBALS.game.current_level;
			if (GLOBALS.game.current_level == TOTAL_LEVELS) {
				GLOBALS.current_scene = SCENE_CONGRATULATIONS;
			}
			else {
				Scene_game_prepare_level();
			}
		}
	}

	static const Vec4 POSSIBLE_COLORS[5] = {
		V4( 15.0f/255.0f,  15.0f/255.0f,  15.0f/255.0f, 255.0f/255.0f),
		V4(  9.0f/255.0f,   4.0f/255.0f,  21.0f/255.0f, 255.0f/255.0f),
		V4( 21.0f/255.0f,   9.0f/255.0f,   6.0f/255.0f, 255.0f/255.0f),
		V4( 31.0f/255.0f,  13.0f/255.0f,  41.0f/255.0f, 255.0f/255.0f),
		V4(  9.0f/255.0f,  28.0f/255.0f,  28.0f/255.0f, 255.0f/255.0f),
	};
	u32 color_i = GLOBALS.game.current_level % 5;
	Draw_rect(
		HMM_Vec3(GAME_AREA_X, GAME_AREA_Y, 0.0f),
		GAME_AREA_W,
		GAME_AREA_H,
		HMM_Vec4(POSSIBLE_COLORS[color_i].r, POSSIBLE_COLORS[color_i].g, POSSIBLE_COLORS[color_i].b, POSSIBLE_COLORS[color_i].a)
	);
	Flush_triangles();
    Draw_sprite_ex(0.0f, 0.0f, GAMEPLAY_WIDTH, GAMEPLAY_HEIGHT, 0.0f, 0.0f, 0.0f, &GLOBALS.starfield_bg, RGB_DEFAULT);
	// Draw_rect(
	// 	HMM_Vec3(GAME_AREA_X, GAME_AREA_Y, 0.0f),
	// 	GAME_AREA_W, GAME_AREA_H,
	// 	HMM_Vec4(0.0f, 0.0f, 0.0f, 1.0f));
	// Flush_triangles();
	Draw_asteroids();
	Draw_checkpoint();
	if (!GLOBALS.game.player.died) {
		Draw_player();
        Simulate_impulse();
        Draw_impulse();
	} else {
		Draw_explosion();
		Flush_triangles();
	}
	Draw_hud();

	f32 simulation_itersf = GLOBALS.delta_time_f32 / GAMEPLAY_DELTA_TIME + GLOBALS.game.partial_iters;
	u32 simulation_iters = (u32)simulation_itersf;
	GLOBALS.game.partial_iters = simulation_itersf - (f32)simulation_iters;

	for (u32 i = 0; i < simulation_iters; ++i) {
		if (!GLOBALS.game.player.died && !GLOBALS.game.player.arrived) {
			int collisions_result = Check_player_collisions();
			if (collisions_result == PLAYER_COLLSION_COLLIDED) {
				Mixer_play_sound(GLOBALS.explosion_sound, PLAY_SOUND_FLAG_NONE, MAX_VOLUME);
				GLOBALS.game.player.died = true;
				GLOBALS.game.explosion.time = 0.0f;
				GLOBALS.game.explosion.seed = (u64)rand();
				GLOBALS.game.explosion.x    = GLOBALS.game.player.x;
				GLOBALS.game.explosion.y    = GLOBALS.game.player.y;
			}
			else if (collisions_result == PLAYER_COLLSION_ARRIVED) {
				Mixer_play_sound(GLOBALS.arrive_sound, PLAY_SOUND_FLAG_NONE, MAX_VOLUME);
				GLOBALS.game.player.arrived = true;
			}
			Scene_game_input();
			Simulate_player();
		}
		Simulate_asteroids();
	}
}

void
Scene_game_input() {
	if (app_data.keyboard[PLATFORM_KEYCODE_SPACE] == KEY_STATUS_DOWN) {
        if (impulse.time_left <= 0 && GLOBALS.game.player.impulses_left > 0) {
            GLOBALS.game.player.accel = PLAYER_ACCEL;
            impulse.time_left = IMPULSE_EFFECT_SHOW_TIME;
            GLOBALS.game.player.impulses_left--;
			Mixer_play_sound(GLOBALS.impulse_sound, PLAY_SOUND_FLAG_NONE, MAX_VOLUME);
        }
	}

    if (GLOBALS.game.player.energy_left > 0) {
        GLOBALS.game.player.angular_accel = 0.0f;
        if (app_data.keyboard[PLATFORM_KEYCODE_LEFT] == KEY_STATUS_DOWN ||
            app_data.keyboard[PLATFORM_KEYCODE_LEFT] == KEY_STATUS_HOLD) {
            GLOBALS.game.player.energy_left -= PLAYER_ENERGY_CONSUMPTION * GAMEPLAY_DELTA_TIME;
            GLOBALS.game.player.angular_accel = PLAYER_ANGULAR_ACCEL;
        } 
        if (app_data.keyboard[PLATFORM_KEYCODE_RIGHT] == KEY_STATUS_DOWN ||
            app_data.keyboard[PLATFORM_KEYCODE_RIGHT] == KEY_STATUS_HOLD) {
            GLOBALS.game.player.energy_left -= PLAYER_ENERGY_CONSUMPTION * GAMEPLAY_DELTA_TIME;
            GLOBALS.game.player.angular_accel = -PLAYER_ANGULAR_ACCEL;
        }
    }
}




int
Check_player_collisions() {
	Assert((PLAYER_BOUNDIG_MESH.total_verts%3) == 0, "");
	u32 n_player_triangles = PLAYER_BOUNDIG_MESH.total_verts/3;
	for (u32 player_triangle_i = 0; player_triangle_i < n_player_triangles; ++player_triangle_i) {
		u32 p_v0_i = player_triangle_i * 3 + 0;
		u32 p_v1_i = player_triangle_i * 3 + 1;
		u32 p_v2_i = player_triangle_i * 3 + 2;
		Vec2 player_triangle[3];
		Transform_triangle(
			PLAYER_BOUNDIG_MESH.vertices[p_v0_i], PLAYER_BOUNDIG_MESH.vertices[p_v1_i], PLAYER_BOUNDIG_MESH.vertices[p_v2_i],
			PLAYER_SCALE, PLAYER_SCALE, GLOBALS.game.player.x, GLOBALS.game.player.y, GLOBALS.game.player.dir,
			&player_triangle[0], &player_triangle[1], &player_triangle[2]);

		// Check collision with borders
		for (int i = 0; i < 3; ++i) {
			if (player_triangle[i].x < 0.0f ||
				player_triangle[i].x > GAME_AREA_W ||
				player_triangle[i].y < 0.0f ||
				player_triangle[i].y > GAME_AREA_H) {

				return PLAYER_COLLSION_COLLIDED;
			}
		}

		// Check collision with checkpoint

		f32 px = GLOBALS.game.checkpoint.x - CHECKPOINT_SCALE * 0.5f;
		f32 py = GLOBALS.game.checkpoint.y - CHECKPOINT_SCALE * 0.5f;
		Vec2 checkpoint_triangle_bottom_left[3] = {
			V2(px, py),
			V2(px + CHECKPOINT_SCALE, py),
			V2(px + CHECKPOINT_SCALE, py + CHECKPOINT_SCALE)
		};
		if (Triangles_overlap(player_triangle, checkpoint_triangle_bottom_left)) {
			return PLAYER_COLLSION_ARRIVED;
		}

		Vec2 checkpoint_triangle_top_right[3] = {
			V2(px, py),
			V2(px + CHECKPOINT_SCALE, py + CHECKPOINT_SCALE),
			V2(px, py + CHECKPOINT_SCALE)
		};
		if (Triangles_overlap(player_triangle, checkpoint_triangle_top_right)) {
			return PLAYER_COLLSION_ARRIVED;
		}

		u32 asteroids_count = GLOBALS.game.asteroids_count;
		for (u32 i = 0; i < asteroids_count; ++i) {
			Asteroid *asteroid = &GLOBALS.game.asteroids[i];
			f32 x = asteroid->x;
			f32 y = asteroid->y;
			f32 w = asteroid->scale_x;
			f32 h = asteroid->scale_y;
			f32 r = asteroid->dir;

			f32 asteroid_radius = sqrtf(w * w + h * h);

			f32 min_dist = asteroid_radius * asteroid_radius + PLAYER_COLLSION_RADIUS * PLAYER_COLLSION_RADIUS;

			f32 dx = GLOBALS.game.player.x - x;
			f32 dy = GLOBALS.game.player.y - y;
			if ((dx * dx + dy * dy) > min_dist) continue;

			const BoundingMesh *asteroid_bm = ASTEROIDS_BOUNDING_MESHES[asteroid->type];
			u32 n_asteroid_triangles = asteroid_bm->total_verts/3;
			for (u32 triangle_i = 0; triangle_i < n_asteroid_triangles; ++triangle_i) {
				u32 v0_i = triangle_i * 3 + 0;
				u32 v1_i = triangle_i * 3 + 1;
				u32 v2_i = triangle_i * 3 + 2;
				Vec2 asteroid_triangle[3];
				Transform_triangle(
					asteroid_bm->vertices[v0_i], asteroid_bm->vertices[v1_i], asteroid_bm->vertices[v2_i],
					w, h, x, y, r,
					&asteroid_triangle[0], &asteroid_triangle[1], &asteroid_triangle[2]);

				if (Triangles_overlap(player_triangle, asteroid_triangle)) {
					return PLAYER_COLLSION_COLLIDED;
				}
			}
		}
	}
	
	return PLAYER_COLLSION_NONE;
}

void Simulate_impulse() {
    const f32 dt = GLOBALS.delta_time_f32;

    if (impulse.time_left > 0) {
        impulse.time_left -= dt;
    }
}

void
Simulate_asteroids() {
	u32 asteroids_count = GLOBALS.game.asteroids_count;
	for (u32 i = 0; i < asteroids_count; ++i) {
		Asteroid *asteroid = &GLOBALS.game.asteroids[i];
		f32 dir_to_add = asteroid->angular_vel * GAMEPLAY_DELTA_TIME;
		while(dir_to_add < 0.0f) {
			dir_to_add += 360.0f;
		}
		asteroid->dir = fmodf(asteroid->dir + dir_to_add, 360.0f);
	}

}


void
Simulate_player() {
	const f32 dt = GAMEPLAY_DELTA_TIME;

	// Apply angular friction
	if (GLOBALS.game.player.angular_vel < 0.0f) { // Rotating clockwise
		GLOBALS.game.player.angular_vel += PLAYER_ANGULAR_FRICTION * dt;
		GLOBALS.game.player.angular_vel = Min(GLOBALS.game.player.angular_vel, 0.0f);
	}
	else if (GLOBALS.game.player.angular_vel > 0.0f) { // Rotating counterclockwise
		GLOBALS.game.player.angular_vel -= PLAYER_ANGULAR_FRICTION * dt;
		GLOBALS.game.player.angular_vel = Max(GLOBALS.game.player.angular_vel, 0.0f);
	}

	// Update angular things
	f32 angular_vel = GLOBALS.game.player.angular_vel;
	angular_vel += GLOBALS.game.player.angular_accel * dt;
	angular_vel = Clamp(angular_vel, -PLAYER_ANGULAR_MAX_VEL, PLAYER_ANGULAR_MAX_VEL); // Clamp between the maximums
	GLOBALS.game.player.angular_vel = angular_vel;
	GLOBALS.game.player.dir = fmodf(GLOBALS.game.player.dir + angular_vel * dt, 360.0f);


	// Velocity things
	f32 vx = GLOBALS.game.player.vx;
	f32 vy = GLOBALS.game.player.vy;
	f32 vel_total = sqrtf(vx * vx + vy * vy);	

	// Apply friction
	if (vel_total > 0.0f) {
		f32 vel_total_new = vel_total - (PLAYER_FRICTION * dt);
		vel_total_new     = Max(vel_total_new, PLAYER_MIN_VEL);
		vx = vx * (vel_total_new / vel_total);
		vy = vy * (vel_total_new / vel_total);
	}

	// Calculate the decomposed accel
	if (GLOBALS.game.player.accel != 0) {
		f32 ax = cosf(Radians(GLOBALS.game.player.dir)) * GLOBALS.game.player.accel;
		f32 ay = sinf(Radians(GLOBALS.game.player.dir)) * GLOBALS.game.player.accel;
		vx = (ax * dt);
		vy = (ay * dt);

		GLOBALS.game.player.vx = vx;
		GLOBALS.game.player.vy = vy;
	}

	GLOBALS.game.player.x += (vx * dt);
	GLOBALS.game.player.y += (vy * dt);
	GLOBALS.game.player.accel = 0.0; // Reset the acceleration
}




void
Draw_impulse() {
    if (impulse.time_left > 0) {
		
		f32 impulse_alpha = Min(1.0f, Lerp(0.0f, 2.0f, impulse.time_left/IMPULSE_EFFECT_SHOW_TIME));
        f32 px = GLOBALS.game.player.x + GAME_AREA_X;
        f32 py = GLOBALS.game.player.y + GAME_AREA_Y;
        f32 dir = GLOBALS.game.player.dir;
        
        Draw_sprite_ex(px, py, IMPULSE_EFFECT_WIDTH, IMPULSE_EFFECT_LENGTH, dir, 1.1, 0.5, &impulse.texture, HMM_Vec4(1,1,1,impulse_alpha));
    }
}


void
Draw_checkpoint() {
	f32 cx = GAME_AREA_X + GLOBALS.game.checkpoint.x;
	f32 cy = GAME_AREA_Y + GLOBALS.game.checkpoint.y;
    Draw_sprite_ex(cx, cy, CHECKPOINT_SCALE, CHECKPOINT_SCALE, 0.0f, 0.5f, 0.5f, &GLOBALS.game.checkpoint.texture, RGB_DEFAULT);
}

void
Draw_player() {
	Texture *tex = &GLOBALS.game.player.texture;
	u32 w = tex->width;
	u32 h = tex->height;
	f32 pw = PLAYER_SCALE;
	f32 ph = pw * (f32)h/(f32)w;

	f32 px = GLOBALS.game.player.x + GAME_AREA_X;
	f32 py = GLOBALS.game.player.y + GAME_AREA_Y;
	f32 pr = GLOBALS.game.player.dir;
	Draw_sprite_ex(px, py, pw, ph, pr, PLAYER_CENTER_X, PLAYER_CENTER_Y, tex, RGB_DEFAULT);
	if (GLOBALS.show_bounding_meshes) {
		Draw_bounding_mesh(&PLAYER_BOUNDIG_MESH, PLAYER_SCALE, PLAYER_SCALE, px, py, pr, V4(1.0f, 0.0f, 0.0f, 0.7f));
		Flush_triangles();
	}
}


void
Draw_asteroids() {
	u32 asteroids_count = GLOBALS.game.asteroids_count;
	for (u32 i = 0; i < asteroids_count; ++i) {
		Asteroid *asteroid = &GLOBALS.game.asteroids[i];
		f32 x = asteroid->x + GAME_AREA_X;
		f32 y = asteroid->y + GAME_AREA_Y;
		f32 w = asteroid->scale_x;
		f32 h = asteroid->scale_y;
		f32 r = asteroid->dir;
		Draw_sprite_ex(x, y, w, h, r, 0.5f, 0.5f, &GLOBALS.game.asteroid_texture, RGB_DEFAULT);
		if (GLOBALS.show_bounding_meshes) {
			Draw_bounding_mesh(ASTEROIDS_BOUNDING_MESHES[asteroid->type], w, h, x, y, r, V4(1.0f, 0.0f, 0.0f, 0.7f));
		}
	}
	if (GLOBALS.show_bounding_meshes) {
		Flush_triangles();
	}
}


bool
Draw_explosion() {
	if (GLOBALS.game.explosion.time > EXPLOSION_MAX_TIME) {
		return false;
	}
	f32 t = GLOBALS.game.explosion.time;
	u64 seed = GLOBALS.game.explosion.seed;
	f32 explosion_x = GLOBALS.game.explosion.x;
	f32 explosion_y = GLOBALS.game.explosion.y;
	f32 alpha = 1.0f;
	f32 full_alpha_t = 0.8f *  EXPLOSION_MAX_TIME;
	if (t > full_alpha_t) {
		alpha = Lerp(1.0f, 0.0f, (t-full_alpha_t)/(EXPLOSION_MAX_TIME-full_alpha_t));
	}
	for (int i = 0; i < EXPLOSION_TOTAL_PARTICLES; ++i) {
		f32 vel = Lerp(EXPLOSION_PARTICLE_MIN_VEL,  EXPLOSION_PARTICLE_MAX_VEL, wy2u01(wyrand(&seed)));
		f32 s   = Lerp(EXPLOSION_PARTICLE_MIN_SIZE, EXPLOSION_PARTICLE_MAX_SIZE, wy2u01(wyrand(&seed)));
		f32 dir = (f32)i/(f32)(EXPLOSION_TOTAL_PARTICLES) * PI32 * 2;
		f32 vx = cosf(dir) * vel;
		f32 vy = sinf(dir) * vel;
		f32 x = (explosion_x + vx * t) + GAME_AREA_X;
		f32 y = (explosion_y + vy * t) + GAME_AREA_Y;
		Vec3 color = V3_lerp(V3(1.0f, 1.0f, 0.0f), V3(1.0f, 0.0f, 0.0f), wy2u01(wyrand(&seed)));
		Draw_quad(
			HMM_Vec3(x-1.0f*s, y-1.0f*s, 0.0f),
			HMM_Vec3(x+1.0f*s, y-1.0f*s, 0.0f),
			HMM_Vec3(x+1.0f*s, y+1.0f*s, 0.0f),
			HMM_Vec3(x-1.0f*s, y+1.0f*s, 0.0f),
			HMM_Vec4(color.r, color.g, color.b, alpha));
	}
	GLOBALS.game.explosion.time += GLOBALS.delta_time_f32;
	return true;
}


