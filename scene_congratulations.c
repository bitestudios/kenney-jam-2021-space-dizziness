
void
Scene_congratulations() {
	{
		f32 min_alpha = 0.5f;
		f32 max_alpha = 1.2f;
		f32 starfield_alpha = Lerp(min_alpha, max_alpha, (sinf(GLOBALS.current_time_f32*1.5f)+1.0f) * 0.5f);
		Draw_sprite_ex(
			0.0f, 0.0f,
			GAMEPLAY_WIDTH, GAMEPLAY_HEIGHT, 0.0f, 0.0f, 0.0f,
			&GLOBALS.starfield_bg, HMM_Vec4(1.0f, 1.0f, 1.0f, starfield_alpha));
	}
    f32 title_height = (f32)GAMEPLAY_HEIGHT * 0.08f;
	f32 title_width = Get_text_width_with_height(&GLOBALS.font, "CONGRATULATIONS!", title_height);
	f32 title_x = ((f32)GAMEPLAY_WIDTH - title_width) * 0.5f;
	f32 title_y = (f32)GAMEPLAY_HEIGHT * 0.75;
	Draw_text_with_height(&GLOBALS.font, "CONGRATULATIONS!", title_height, title_x, title_y, 1, 1, 1, 1);


    title_height = (f32)GAMEPLAY_HEIGHT * 0.05f;
	title_width = Get_text_width_with_height(&GLOBALS.font, "YOU COMPLETED THE GAME!", title_height);
	title_x = ((f32)GAMEPLAY_WIDTH - title_width) * 0.5f;
	title_y = (f32)GAMEPLAY_HEIGHT * 0.65;
	Draw_text_with_height(&GLOBALS.font, "YOU COMPLETED THE GAME!", title_height, title_x, title_y, 1, 1, 1, 1);

	{
		Texture *tex = &GLOBALS.game.player.texture;
		u32 w = tex->width;
		u32 h = tex->height;
		f32 pw = PLAYER_SCALE * 2;
		f32 ph = pw * (f32)h/(f32)w;

		f32 px = GAMEPLAY_WIDTH * 0.5f;
		f32 py = GAMEPLAY_HEIGHT * 0.4f;
		f32 pr = 90.0f;
		Draw_sprite_ex(px, py, pw, ph, pr, PLAYER_CENTER_X, PLAYER_CENTER_Y, tex, RGB_DEFAULT);
	}

	if (Show_menu(
			GLOBALS.congratulations_menu_options,
			CREDITS_MENU_OPTION_COUNT,
			GLOBALS.congratulations_menu_selected_option,
			&GLOBALS.congratulations_menu_selected_option)) {

		GLOBALS.mouse_pointing = false;

		if (GLOBALS.congratulations_menu_selected_option == CONGRATULATIONS_MENU_BACK_TO_MENU) {
			Mixer_clear_tracks();
			Mixer_play_sound(GLOBALS.launch_option_sound, PLAY_SOUND_FLAG_NONE, MAX_VOLUME);
			Mixer_play_sound(GLOBALS.intro_music, PLAY_SOUND_FLAG_LOOP, MAX_VOLUME);
			GLOBALS.current_scene = SCENE_MENU;
		}
	}
}

