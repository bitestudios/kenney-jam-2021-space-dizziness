#ifndef SCENE_GAME_H
#define SCENE_GAME_H

#include "game.h"

static struct {
    f32 time_left;
    Texture texture;
} impulse = {0};

enum {
	PLAYER_COLLSION_NONE,
	PLAYER_COLLSION_COLLIDED,
	PLAYER_COLLSION_ARRIVED,
};

Texture backgrounds[4];

void
Draw_player();

void
Draw_impulse();

void
Draw_asteroids();

void
Draw_checkpoint();

void
Draw_hud();

void
Scene_game_input();

void 
Simulate_impulse();

void
Simulate_asteroids();

void
Simulate_player();

void
Scene_game_prepare_level();

i32
Scene_game_init();

void
Scene_game();

int
Check_player_collisions();

bool
Draw_explosion();

#endif // SCENE_GAME_H
