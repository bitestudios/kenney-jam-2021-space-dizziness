#include "hud.h"
#include "engine/sprite.h"

Texture impulse_texture;
Texture bolt_texture;
Texture pause_texture;
Texture restart_texture;
ImageOption pause_option;
ImageOption restart_option;

void
Init_hud() {
    Setup_texture("assets/img/laserBlue08.png", true, &impulse_texture);
    Setup_texture("assets/img/bolt.png", true, &bolt_texture); //TODO only do once
    Setup_texture("assets/img/pause.png", true, &pause_texture);
    Setup_texture("assets/img/return.png", true, &restart_texture);

    f32 px = GAMEPLAY_WIDTH - HUD_MARGIN_LEFT - HUD_BUTTON_SCALE;
    f32 py = GAMEPLAY_HEIGHT-HUD_HEIGHT+(HUD_HEIGHT-HUD_BUTTON_SCALE)*0.5f;
    f32 width = HUD_BUTTON_SCALE;
    f32 height = pause_texture.height * HUD_BUTTON_SCALE / pause_texture.width;
    pause_option = Make_menu_image_option(&pause_texture, px, py, width, height);

    px = GAMEPLAY_WIDTH - HUD_MARGIN_LEFT - 2*HUD_BUTTON_SCALE - HUD_SPACING;
    py = 2+GAMEPLAY_HEIGHT-HUD_HEIGHT+(HUD_HEIGHT-HUD_BUTTON_SCALE)*0.5f;
    width = HUD_BUTTON_SCALE;
    height = restart_texture.height * HUD_BUTTON_SCALE / restart_texture.width;
    restart_option = Make_menu_image_option(&restart_texture, px, py, width, height);
}

void
Draw_hud() {
	// Draw_text_with_height(&GLOBALS.font, "TODO", 10.0f, 0, GAMEPLAY_HEIGHT-HUD_HEIGHT+(HUD_HEIGHT-10.0f)*0.5f, 1.0f, 1.0f, 1.0f, 1.0f);
    // Dibujar impulsos restantes
    f32 init_x = HUD_MARGIN_LEFT;
    f32 init_y = GAMEPLAY_HEIGHT-HUD_HEIGHT+(HUD_HEIGHT-HUD_IMPULSE_SIZE)*0.5f;
    // for (u32 i = 0; i < GLOBALS.game.player.impulses_left; i++) {
    //     Draw_sprite(init_x, init_y, HUD_IMPULSE_SIZE, HUD_IMPULSE_SIZE, 0, &impulse_texture, impulse_texture.width, impulse_texture.height, 0, RGB_DEFAULT);
    //     init_x += HUD_IMPULSE_SIZE + HUD_SPACING;
    // }
	Draw_rect(
		HMM_Vec3(0.0f, GAMEPLAY_HEIGHT-HUD_HEIGHT, 0.0f),
		GAMEPLAY_WIDTH,
		HUD_TEXT_HEIGHT,
		HMM_Vec4(0.0f, 0.0f, 0.0f, 1.0f)
	);
	Flush_triangles();
    Draw_sprite(init_x, init_y, HUD_IMPULSE_SIZE, HUD_IMPULSE_SIZE, 0, &impulse_texture,
			impulse_texture.width, impulse_texture.height, 0, RGB_DEFAULT);

    init_x += HUD_IMPULSE_SIZE + HUD_SPACING;
    init_y = -1+GAMEPLAY_HEIGHT-HUD_HEIGHT+(HUD_HEIGHT-HUD_TEXT_HEIGHT/2)*0.5f;
    char text[] = "p";
    text[0] = (char)GLOBALS.game.player.impulses_left + '0';
    Draw_text_with_height(&GLOBALS.font, text, HUD_TEXT_HEIGHT, init_x, init_y, 255, 255, 255, 1);

    init_x += HUD_TEXT_HEIGHT;
    f32 wx = HUD_BOLT_SCALE;
    f32 wy = bolt_texture.height * HUD_BOLT_SCALE / bolt_texture.width;
    init_y = GAMEPLAY_HEIGHT-HUD_HEIGHT+(HUD_HEIGHT-wy)*0.5f;

    

    if (GLOBALS.game.player.energy_left >= 0) {
        Draw_sprite(init_x, init_y, wx, wy, 0, &bolt_texture, bolt_texture.width, bolt_texture.height, 0, RGB_DEFAULT);
	}
    else {
        Draw_sprite(init_x, init_y, wx, wy, 0, &bolt_texture, bolt_texture.width, bolt_texture.height, 0, HMM_Vec4(1.0f, 0.0f, 0.0f, 1.0f));
    }

    init_x += wx + HUD_SPACING;
    f32 bar_h = wy * GLOBALS.game.player.energy_left / 100;
    Draw_rect(HMM_Vec3(init_x, init_y+bar_h, 0), HUD_ENERGY_WIDTH, wy-bar_h, HMM_Vec4(0.6, 0.6, 0.6, 1));
    Draw_rect(HMM_Vec3(init_x, init_y, 0), HUD_ENERGY_WIDTH, bar_h, HMM_Vec4(1.0f, 1.0f, 1.0f, 1));
	Flush_triangles();

	{
		f32 level_txt_w = Get_text_width_with_height(&GLOBALS.font, "LEVEL", 10.0f);
		f32 level_txt_x = (GAMEPLAY_WIDTH - level_txt_w) * 0.5f;
		f32 level_txt_y = (GAMEPLAY_HEIGHT - 7.0f);
		Draw_text_with_height(&GLOBALS.font, "LEVEL", 10.0f, level_txt_x, level_txt_y, 255, 255, 255, 1.0f);
	}

    init_y = GAMEPLAY_HEIGHT-HUD_HEIGHT + 2.0f;
    char level_value[20];
    snprintf(level_value, 20, "%i/%i", GLOBALS.game.current_level+1, TOTAL_LEVELS);
    f32 level_value_w = Get_text_width_with_height(&GLOBALS.font, level_value, HUD_LEVEL_COUNTER_HEIGHT);
	init_x = (GAMEPLAY_WIDTH - level_value_w) * 0.5f;
    Draw_text_with_height(&GLOBALS.font, level_value, HUD_LEVEL_COUNTER_HEIGHT, init_x, init_y, 255, 255, 255, 1.0f);
    

    //Image buttons

    if (Check_image_option(&pause_option)) {
        GLOBALS.current_scene = SCENE_IN_GAME_MENU;
    }
    // if (Check_image_option(&restart_option)) {
    //     GLOBALS.game.player.died = true;
    // }
}
