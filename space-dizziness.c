

#include "game.h"
#include "utils.h"
#include "scene_main_menu.h"
#include "scene_game_menu.h"
#include "scene_game.h"
#include "scene_credits.h"
#include "scene_congratulations.h"
#include "hud.h"
#include "scene_controls.h"


void
Game_cleanup();

void
Show_vsync_msg() {
	static const f32 VSYNC_HEIGHT = 15.0f;
	static const f32 VSYNC_PAD    = 2.0f;
	const char *msg = GLOBALS.vsync_off ? "vsync: off" : "vsync: on";
	f32 vsync_msg_w = Get_text_width_with_height(&GLOBALS.font, msg, VSYNC_HEIGHT);
	f32 msg_x = (GAMEPLAY_WIDTH - vsync_msg_w)   * 0.5f;
	f32 msg_y = (GAMEPLAY_HEIGHT - VSYNC_HEIGHT) * 0.5f;
	f32 rect_width  = vsync_msg_w  + VSYNC_PAD * 2.0f;
	f32 rect_height = VSYNC_HEIGHT + VSYNC_PAD * 2.0f;
	f32 rect_x      = (GAMEPLAY_WIDTH - rect_width)   * 0.5f;
	f32 rect_y      = (GAMEPLAY_HEIGHT - rect_height) * 0.5f;
	Draw_rect(
		HMM_Vec3(rect_x, rect_y, 0.0f),
		rect_width, rect_height,
		HMM_Vec4(0.0f, 0.0f, 0.0f, 0.7f));

	Flush_triangles();

	Draw_text_with_height(
		&GLOBALS.font,
		msg,
		VSYNC_HEIGHT,
		msg_x, msg_y,
		1.0f, 1.0f, 1.0f, 1.0f);
}



int
Game_frame() {
	if (app_data.quit_requested || GLOBALS.quit) {
		Game_cleanup();
		return 1;
	}
	if (app_data.window_resized) {
		u32 w_width  = app_data.w_width;
		u32 w_height = app_data.w_height;

		Calculate_viewport(
			w_width, w_height,
			&GLOBALS.viewport_x, &GLOBALS.viewport_y,
			&GLOBALS.viewport_w, &GLOBALS.viewport_h);

		glViewport(GLOBALS.viewport_x, GLOBALS.viewport_y, GLOBALS.viewport_w, GLOBALS.viewport_h);
		glScissor(GLOBALS.viewport_x, GLOBALS.viewport_y, GLOBALS.viewport_w, GLOBALS.viewport_h);
	}

	i64 current_time = App_time();
	GLOBALS.delta_time_i64     = current_time - GLOBALS.current_time_i64;
	GLOBALS.current_time_i64   = current_time;
	GLOBALS.delta_time_f32   = To_sec_f32(GLOBALS.delta_time_i64);
	GLOBALS.current_time_f32 = To_sec_f32(current_time);
	
	GLOBALS.mouse_moved = false;
	if (GLOBALS.mouse_x != app_data.mouse.x) {
		GLOBALS.mouse_x = app_data.mouse.x;
		GLOBALS.mouse_moved = true;
	}
	if (GLOBALS.mouse_y != app_data.mouse.y) {
		GLOBALS.mouse_y = app_data.mouse.y;
		GLOBALS.mouse_moved = true;
	}

	glDisable(GL_SCISSOR_TEST);
	glClearColor(0.0, 0.0, 0.0, 1.0);// Reset all to black
	glClear(GL_COLOR_BUFFER_BIT);
	glEnable(GL_SCISSOR_TEST);

	glClearColor(0.1f, 0.1f, 0.1f, 1.0f);// background color	
	glClear(GL_COLOR_BUFFER_BIT);

	{
		Renderer_set_max_world_coordinates(GAMEPLAY_WIDTH, GAMEPLAY_HEIGHT);

		hmm_mat4 f_ortho = HMM_Orthographic(0.0f, (f32)GAMEPLAY_WIDTH, 0.0f, (f32)GAMEPLAY_HEIGHT, 0.0f, 1.0f);
		Font_renderer_set_matrix(f_ortho);
	}

	if (GLOBALS.current_scene == SCENE_MENU) {
		Scene_main_menu();
	}
	else if (GLOBALS.current_scene == SCENE_CREDITS) {
		Scene_credits();
	}
	else if (GLOBALS.current_scene == SCENE_GAME) {
		Scene_game();
	}
	else if (GLOBALS.current_scene == SCENE_IN_GAME_MENU) {
		Scene_in_game_menu();
	}
	else if (GLOBALS.current_scene == SCENE_CONGRATULATIONS) {
		Scene_congratulations();
	}
    else if (GLOBALS.current_scene == SCENE_CONTROLS) {
        Scene_controls();
    }


	if (app_data.keyboard[PLATFORM_KEYCODE_F1] == KEY_STATUS_DOWN) {
		GLOBALS.vsync_msg_time = VSYNC_MSG_TIME;
		GLOBALS.vsync_off = !GLOBALS.vsync_off;
		if (GLOBALS.vsync_off) {
			Set_swap_interval(0);
		}
		else {
			Set_swap_interval(1);
		}
	}

	if (app_data.keyboard[PLATFORM_KEYCODE_F11] == KEY_STATUS_DOWN ||
		app_data.keyboard[PLATFORM_KEYCODE_F] == KEY_STATUS_DOWN) {
		GLOBALS.fullscreen = !GLOBALS.fullscreen;
		Set_fullscreen(GLOBALS.fullscreen);
	}

	if (app_data.keyboard[PLATFORM_KEYCODE_F4] == KEY_STATUS_DOWN) {
		GLOBALS.show_bounding_meshes = !GLOBALS.show_bounding_meshes;
	}

	if (app_data.keyboard[PLATFORM_KEYCODE_F3] == KEY_STATUS_DOWN) {
		GLOBALS.show_fps = !GLOBALS.show_fps;
	}

	// Calculate fps
	static char fps_message[32] = "FPS: 00.00";
	static u32 total_fps = 0;
	static f32 fps = 0.0f;
	static f32 fps_time_acum = 0.0f;
	fps_time_acum += GLOBALS.delta_time_f32;
	++total_fps;

	if (fps_time_acum >= 1.0f) {
		fps = (f32)(total_fps) / fps_time_acum;
		fps_time_acum = 0;
		total_fps = 0;
		snprintf(fps_message, sizeof(fps_message), "FPS: %.2f", fps);
	}

	if (GLOBALS.show_fps) {
		const f32 FPS_HEIGHT = 5.0f;
		const f32 FPS_PAD    = 5.0f;
		f32 fps_message_w = Get_text_width_with_height(&GLOBALS.font, fps_message, FPS_HEIGHT);
		Draw_rect(
			HMM_Vec3(GAMEPLAY_WIDTH-fps_message_w-FPS_PAD*2.0f, GAMEPLAY_HEIGHT-FPS_HEIGHT-FPS_PAD*2.0f, 0.0f),
			fps_message_w+FPS_PAD*2, FPS_HEIGHT+FPS_PAD*2.0f,
			HMM_Vec4(0.0f, 0.0f, 0.0f, 0.7f));
		Flush_triangles();

		Draw_text_with_height(
			&GLOBALS.font,
			fps_message,
			FPS_HEIGHT,
			((f32)GAMEPLAY_WIDTH-fps_message_w-FPS_PAD), (f32)GAMEPLAY_HEIGHT-FPS_HEIGHT-FPS_PAD,
			1.0f, 1.0f, 1.0f, 1.0f);
	}

	if (GLOBALS.vsync_msg_time > 0.0f) {
		Show_vsync_msg();
		GLOBALS.vsync_msg_time -= GLOBALS.delta_time_f32;
	}

    // Draw_sprite(0, 0, objects[1].texture->width, objects[1].texture->height, 0, 
    //         objects[1].texture, objects[1].texture->width, objects[1].texture->height, 0);

	return 0;
}




// the sample callback, running in audio thread
static int
stream_cb(void *user_data, float* buffer, int n_frames, int n_channels) {
	Mixer_play_frames(buffer, n_frames * n_channels);
	return 0;
}


void
Game_cleanup() {
	Renderer_cleanup();

	Free_font(&GLOBALS.font);
	Font_renderer_cleanup();

	Terminate_sound_player();
	Mixer_cleanup();

	Cleanup_GL();
	Destroy_window();
}

int
Game_setup() {
	WindowDesc window_desc = {
		.title  = GAME_TITLE,
		.width  = GAMEPLAY_WIDTH,
		.height = GAMEPLAY_HEIGHT,
	};

	if (Create_window(&window_desc) != 0) {
		fprintf(stderr, "Cannot create window.\n");
		return -1;
	}

	if (Setup_GL() != 0) {
		fprintf(stderr, "Cannot setup GL.\n");
	    return -1;
	}

	Set_swap_interval(1); // enable vsync

	Init_app_time();

	if (Renderer_setup((f32)GAMEPLAY_WIDTH, (f32)GAMEPLAY_HEIGHT) != 0) {
		fprintf(stderr, "Failed to setup renderer\n");
	    return -1;
	}

	if (Font_renderer_setup() != 0) {
		fprintf(stderr, "Failed to setup font renderer\n");
	    return -1;
	}
	if (Make_font(&GLOBALS.font, FONT_FILE, 64.0f) != 0) {
		fprintf(stderr, "Cannot make font '%s'.\n", FONT_FILE);
		return -1;
	}
    //Load and compile shaders
	if (Init_sprites() != 0) {
		fprintf(stderr, "Failet do set up sprite system\n");
	    return -1;
	}

	SoundPlayerDesc splayer_desc = {
		.sample_rate = SAMPLE_RATE,
		.num_channels = SOUND_CHANNELS,
		.buffer_frames = 1024,
    	.user_data = NULL,
		.stream_cb = stream_cb
    };

    if (Launch_sound_player(&splayer_desc) != 0) {
		fprintf(stderr, "Error launching the sound player.\n");
	}

	Mixer_setup(sound_player_info.sample_rate, sound_player_info.num_channels);

	// Setup all the sounds
	GLOBALS.select_option_sound = Mixer_load_sound("assets/sound/select_option.ogg");
	GLOBALS.launch_option_sound = GLOBALS.select_option_sound;
	GLOBALS.explosion_sound     = Mixer_load_sound("assets/sound/explosion.ogg");
	GLOBALS.arrive_sound        = Mixer_load_sound("assets/sound/arrive.ogg");
	GLOBALS.impulse_sound       = Mixer_load_sound("assets/sound/impulse.ogg");
    
	GLOBALS.intro_music         = Mixer_load_sound("assets/sound/intro.ogg");
	GLOBALS.main_theme_music    = Mixer_load_sound("assets/sound/main-theme.ogg");

	Mixer_play_sound(GLOBALS.intro_music, PLAY_SOUND_FLAG_LOOP, MAX_VOLUME);
	


	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable( GL_BLEND );
	glEnable( GL_SCISSOR_TEST );
	glViewport(0, 0, GAMEPLAY_WIDTH, GAMEPLAY_HEIGHT);

	GLOBALS.main_menu_selected_option = MAIN_MENU_OPTION_PLAY;
	GLOBALS.main_menu_options[MAIN_MENU_OPTION_PLAY]    = Make_menu_option("PLAY", 0.60f);
    GLOBALS.main_menu_options[MAIN_MENU_OPTION_CONTROLS] = Make_menu_option("CONTROLS", 0.45);
	GLOBALS.main_menu_options[MAIN_MENU_OPTION_CREDITS] = Make_menu_option("CREDITS", 0.30f);
	GLOBALS.main_menu_options[MAIN_MENU_OPTION_EXIT]    = Make_menu_option("EXIT", 0.15f);

    GLOBALS.pause_menu_selected_option = PAUSE_MENU_OPTION_CONTINUE;
    GLOBALS.pause_menu_options[PAUSE_MENU_OPTION_CONTINUE] = Make_menu_option("CONTINUE", 0.55f);
    GLOBALS.pause_menu_options[PAUSE_MENU_OPTION_CONTROLS] = Make_menu_option("CONTROLS", 0.40f);
    GLOBALS.pause_menu_options[PAUSE_MENU_OPTION_QUIT] = Make_menu_option("MAIN MENU", 0.25f);

    GLOBALS.credits_menu_selected_option = CREDITS_MENU_BACK;
    GLOBALS.credits_menu_options[CREDITS_MENU_BACK] = Make_menu_option("BACK", 0.10f);

	GLOBALS.congratulations_menu_selected_option = CONGRATULATIONS_MENU_BACK_TO_MENU;
    GLOBALS.congratulations_menu_options[CONGRATULATIONS_MENU_BACK_TO_MENU] = Make_menu_option("BACK TO MENU", 0.10f);

    GLOBALS.controls_menu_selected_option = CONTROLS_MENU_OPTION_BACK;
    GLOBALS.controls_menu_options[CONTROLS_MENU_OPTION_BACK] = Make_menu_option("BACK", 0.10f);

	GLOBALS.current_scene = SCENE_MENU;

    if (Setup_texture("assets/img/Meteors/spaceMeteors_001.png", true, &GLOBALS.game.asteroid_texture) != 0) {
		fprintf(stderr, "Cannot load asteroid texture.\n");
        return -1;
	}

	if (Setup_texture("assets/img/checkpoint.png", true, &GLOBALS.game.checkpoint.texture) != 0) {
		fprintf(stderr, "Cannot load checkpoint texture.\n");
        return -1;
	}

	if (Setup_texture("assets/img/starfield_bg.png", true, &GLOBALS.starfield_bg) != 0) {
		fprintf(stderr, "Cannot load starfield texture.\n");
        return -1;
	}

	if (Setup_texture("assets/img/kenney_jam_logo.png", true, &GLOBALS.kenney_jam_logo) != 0) {
		fprintf(stderr, "Cannot load logo texture.\n");
        return -1;
	}

    if (Scene_game_init() != 0) {
        fprintf(stderr, "Couldn't initialize game scene\n");
        return -1;
    }

    Init_hud();
    Init_scene_controls();


    //Load levels
    Load_level("assets/maps/mapa1.json", &GLOBALS.game.levels[0]);
    Load_level("assets/maps/mapa2.json", &GLOBALS.game.levels[1]);
    Load_level("assets/maps/mapa3.json", &GLOBALS.game.levels[2]);
    Load_level("assets/maps/mapa4.json", &GLOBALS.game.levels[3]);
    Load_level("assets/maps/mapa5.json", &GLOBALS.game.levels[4]);
    Load_level("assets/maps/mapa6.json", &GLOBALS.game.levels[5]);
    Load_level("assets/maps/mapa7.json", &GLOBALS.game.levels[6]);
    Load_level("assets/maps/mapa8.json", &GLOBALS.game.levels[7]);
    Load_level("assets/maps/mapa9.json", &GLOBALS.game.levels[8]);
    Load_level("assets/maps/mapa10.json", &GLOBALS.game.levels[9]);
    Load_level("assets/maps/mapa11.json", &GLOBALS.game.levels[10]);
    Load_level("assets/maps/mapa12.json", &GLOBALS.game.levels[11]);
    Load_level("assets/maps/mapa13.json", &GLOBALS.game.levels[12]);
    Load_level("assets/maps/mapa14.json", &GLOBALS.game.levels[13]);
    Load_level("assets/maps/mapa15.json", &GLOBALS.game.levels[14]);
    Load_level("assets/maps/mapa16.json", &GLOBALS.game.levels[15]);
    Load_level("assets/maps/mapa17.json", &GLOBALS.game.levels[16]);
    Load_level("assets/maps/mapa18.json", &GLOBALS.game.levels[17]);
    Load_level("assets/maps/mapa19.json", &GLOBALS.game.levels[18]);
    Load_level("assets/maps/mapa20.json", &GLOBALS.game.levels[19]);
    Load_level("assets/maps/mapa21.json", &GLOBALS.game.levels[20]);
    Load_level("assets/maps/mapa22.json", &GLOBALS.game.levels[21]);
    Load_level("assets/maps/mapa23.json", &GLOBALS.game.levels[22]);
    Load_level("assets/maps/mapa24.json", &GLOBALS.game.levels[23]);
    Load_level("assets/maps/mapa25.json", &GLOBALS.game.levels[24]);
    Load_level("assets/maps/mapa26.json", &GLOBALS.game.levels[25]);
    Load_level("assets/maps/mapa27.json", &GLOBALS.game.levels[26]);

	return 0;
}


int
main() {
	if (Game_setup() != 0) {
		return -1;
	}
    return Run_application_loop(Game_frame);
}




#include "utils.c"
#include "game.c"
#include "menu.c"
#include "scene_main_menu.c"
#include "scene_game_menu.c"
#include "scene_game.c"
#include "scene_credits.c"
#include "scene_congratulations.c"
#include "hud.c"
#include "scene_controls.c"


